module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),       
        sass: {
            dist: {
              files: [{

                expand: true,
                cwd: 'src/stylesheets/sass/',
                src: ['*.scss'],
                dest: 'src/stylesheets/css/',
                ext: '.css'
              }]
            }
        },
        concat: {
          basic_and_extras: {
            files: {
                'build-dev/js/<%= pkg.name %>.js': [                
                "src/language/en.js",
                
              "src/PMDynaform.js",
                "src/core/Selector.js",
                "src/core/Utils.js",
                "src/core/Mask.js",
                "src/core/Validators.js",
                "src/core/Project.js",
                "src/core/Formula.js",
                "src/core/PMRest.js", 
                "src/core/TransformJSON.js",
                "src/core/FileStream.js",
                "src/core/FullScreen.js",
                "src/core/Script.js",
                "src/util/ArrayList.js",
                "src/extends/core/ProjectMobile.js",
                "src/extends/core/ProcessFlow.js",
                "src/extends/core/ProcessFlowIOS.js",
                "src/extends/core/FormsHandler.js",
                "src/extends/core/DataLocalManager.js",                
                "src/extends/views/MediaElement.js",   
                "src/proxy/RestProxy.js",
                "src/views/Validator.js",
                "src/views/Panel.js",
                "src/views/FormPanel.js",
                "src/views/Field.js",
                "src/views/Grid.js",
                "src/views/Button.js",
                "src/views/DropDown.js",
                "src/views/Radio.js",
                "src/views/Submit.js",
                "src/views/TextArea.js",
                "src/views/Text.js",
                "src/views/File.js",
                "src/views/CheckBox.js",
                "src/views/Suggest.js",
                "src/views/Link.js",
                "src/views/Label.js",
                "src/views/Title.js",
                "src/views/Empty.js",
                "src/views/Hidden.js",
                "src/views/Image.js",
                "src/views/SubForm.js",
                "src/views/GeoMap.js",
                "src/views/Annotation.js",
                "src/views/Datetime.js",
                "src/extends/views/ScannerCode.js",
                "src/extends/views/Signature.js",
                "src/extends/views/Geo.js",
                "src/extends/views/FileMobile.js",


                "src/models/Validator.js",
                "src/models/Panel.js",
                "src/models/FormPanel.js",
                "src/models/Field.js",
                "src/models/Grid.js",
                "src/models/Button.js",
                "src/models/DropDown.js",           
                "src/models/Radio.js",
                "src/models/Submit.js",
                "src/models/TextArea.js",
                "src/models/Text.js",
                "src/models/File.js",
                "src/models/CheckBox.js",
                "src/models/Datetime.js",
                "src/models/Suggest.js",
                "src/models/Link.js",
                "src/models/Label.js",
                "src/models/Title.js",
                "src/models/Empty.js",
                "src/models/Hidden.js",
                "src/models/Image.js",
                "src/models/SubForm.js",
                "src/models/GeoMap.js",
                "src/models/Annotation.js",
                "src/extends/models/ScannerCode.js",                     
                "src/extends/models/Signature.js",
                "src/extends/models/Geo.js",   
                "src/extends/models/FileMobile.js"                                                     
                
                /*"src/views/Validator.js",
                "src/views/Form.js",
                "src/views/Panel.js",
                "src/views/Field.js",
                "src/views/Grid.js",
                "src/views/Button.js",
                "src/views/DropDown.js",
                "src/views/Radio.js",
                "src/views/Submit.js",
                "src/views/TextArea.js",
                "src/views/Text.js",
                "src/views/File.js",
                "src/views/CheckBox.js",
                "src/views/Datetime.js",
                "src/views/Suggest.js",
                "src/views/Link.js",
                "src/views/Label.js",
                "src/views/Title.js",
                "src/views/Empty.js",
                "src/views/Hidden.js",
                "src/views/Image.js",
                "src/views/SubForm.js",
                "src/views/GeoMap.js"*/                                                         
                                             
               ],               
                'build-dev/css/<%= pkg.name %>.css': [
                    
                    "src/stylesheets/css/bootstrapCss.css",                    
                    "src/stylesheets/css/extendsMobile.css",
                    "src/stylesheets/css/pm-media.css",                      
                    "src/stylesheets/css/ValidationField.css",
                    "src/stylesheets/css/ContainerPMDynaform.css",
                    "src/stylesheets/css/Label.css",
                    "src/stylesheets/css/DateTimePicker.css",
                    "src/stylesheets/css/GridTable.css",
                    "src/stylesheets/css/ImageField.css",
                    "src/stylesheets/css/Suggest.css",
                    "src/stylesheets/css/File.css",
                    "src/stylesheets/css/GeoMap.css"

                    /*"src/stylesheets/css/bootstrapCss.css",
                    "src/stylesheets/css/ValidationField.css",
                    "src/stylesheets/css/ContainerPMDynaform.css",
                    "src/stylesheets/css/bootstrap-datetimepicker.css",
                    "src/stylesheets/css/Label.css",
                    "src/stylesheets/css/File.css",
                    "src/stylesheets/css/FileField.css",
                    "src/stylesheets/css/ImageField.css",
                    "src/stylesheets/css/extMobile.css",
                    "src/stylesheets/css/pm-media.css" */                     
                ],
                'build-dev/index.html': 
                [   'app/form-dev.html',
                    "src/templates/template.html",
                    "src/templates/controls.html",
                    "src/templates/GridTemplate.html",
                    "src/templates/GeoMapTemplate.html",
                    "src/templates/annotation.html",

                    "src/extends/templates/Qrcode_mobile.html",                                        
                    "src/extends/templates/Signature_mobile.html",                    
                    "src/extends/templates/GeoMobile.html",                    
                    "src/extends/templates/FileMobile.html",                                        
                    'app/form-dev2.html'],
                
                'build-prod/index.html': 
                [   'app/form-prod.html',
                     "src/templates/template.html",
                    "src/templates/controls.html",
                    "src/templates/GridTemplate.html",
                    "src/templates/GeoMapTemplate.html",
                    "src/templates/annotation.html",
                    "src/extends/templates/Qrcode_mobile.html",                                        
                    "src/extends/templates/Signature_mobile.html",
                    "src/extends/templates/GeoMobile.html",
                    "src/extends/templates/FileMobile.html",                                          
                    'app/form-prod2.html'
                ]                                                                
            }
          }
        },
        uglify: {
          options: {
            banner: '/*! <%= pkg.name %> - v<%= pkg.version %>*/',
            mangle: false,
            compress: {
              drop_console: true
            }
          },
            my_target: {
              files: {
                'build-prod/js/pmDynaform.min.js': [
                    'build-dev/libs/jquery/jquery-1.11.js',                   
                    'build-dev/libs/bootstrap-3.1.1/js/bootstrap.min.js',
                    'build-dev/libs/underscore/underscore-1.6.js',
                    'build-dev/libs/backbone/backbone-min.js',
                    'build-dev/libs/restclient/restclient.js',
                    'build-dev/libs/media/mediaelement-and-player.js',
                    'build-dev/libs/datepicker/bootstrap-datetimepicker.js',                                            
                    'build-dev/js/pmDynaform.js'                   
                   ] 
                }
            }
        },
        copy: {
            main:{
                files: [                 
                    {expand: true,flatten: true, src: ['app/appBuild.js'], dest: 'build-dev/'},
                    //{expand: true,flatten: true, src: ['index.json'], dest: 'build-dev/'},                    
                    {expand: true,flatten: true, src: ['app/appBuild.js'], dest: 'build-prod/'},                    
                    {expand: true,flatten: true, src: ['data/*'], dest: 'build-dev/data/'},
                    {expand: true,flatten: true, src: ['data/*'], dest: 'build-prod/data/'},

                    {expand: true, src: ['fonts/**'], dest: 'build-dev/css/'},
                    {expand: true, src: ['fonts/**'], dest: 'build-prod/css/'},                                                            
                    {expand: true, src: ['libs/**/*'], dest: 'build-dev/'},
                    {expand: true,flatten: true,src: ['libs/**/*'], dest: 'build-poc/'},
                    {expand: true,flatten: true, src: ['libs/bootstrap-3.1.1/fonts/*.*'], dest: 'build-prod/fonts/'},
                    {expand: true,flatten: true, src: ['img/loading.gif'], dest: 'build-prod/'},
                    {expand: true,flatten: true, src: ['img/loading.gif'], dest: 'build-dev/'}                                           
                ]
            }       
        },
        cssmin: {
          combine: {
            files: {
              'build-prod/css/pmDynaform.min.css': [
                   "build-dev/libs/bootstrap-3.1.1/css/bootstrap.min.css",
                   "build-dev/libs/detepicker/bootstrap-datetimepicker.css",
                   "build-dev/css/pmDynaform.css"                                                                                                                                            
                ]  
            }
          }
        },
        minified : {
          files: {
            src: [
                'build-dev/libs/jquery/jquery-1.11.js',
                
                'build-dev/libs/bootstrap-3.1.1/js/bootstrap.min.js',
                "build-dev/libs/datepicker/bootstrap-datetimepicker.js",    
                'build-dev/libs/underscore/underscore-1.6.js',
                'build-dev/libs/backbone/backbone-min.js',
                'build-dev/libs/restclient/restclient.js',
                'build-dev/js/pmDynaform.js'                    
                ] ,
            dest: 'build-prod/js/'
          },
          options : {
            sourcemap: false,
            allinone: true,
            dest_filename: "pmDynaform.min.js"
          }
        },
        exec: {
            list_files:{
              cmd: 'node config/etag.js'
            },
            copy_files:{
              cmd: 'cp index.json build-dev/'
            },
            copy_json_prod:{
              cmd: 'cp index.json build-prod/'
            },
            //update_pmdynaform:{
            //    cmd: 'cp -R ../pmdynaform/src/*.* ./src/'   
            //}            
        }        
    });

    // Load the plugin that provides the "uglify" task.
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    // Load the plugin that provides the "concat" task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');    
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-minified');
    grunt.loadNpmTasks('grunt-exec');
    
    
    grunt.loadNpmTasks('grunt-jsduck');
    // Default task(s).
    grunt.registerTask('default', ['copy','sass','concat','cssmin','minified']);    
    grunt.registerTask('etag', ['exec']);    
    
    //grunt.registerTask('default', ['sass','concat','cssmin','exec','copy','minified']);
    

};