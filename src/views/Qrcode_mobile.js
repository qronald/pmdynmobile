(function(){
    var Qrcode_mobile = PMDynaform.view.Field.extend({
        validator: null,
        template: _.template($("#tpl-Qrcode_mobile").html()),
        initialize: function (){
            this.model.on("change", this.render, this);
        },
        render: function() {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.Qrcode_mobile", Qrcode_mobile);
}());
