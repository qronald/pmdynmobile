(function(){
    var Signature_mobile = PMDynaform.view.Field.extend({
        validator: null,
        template: _.template($("#tpl-Signature_mobile").html()),
        initialize: function (){
            this.model.on("change", this.render, this);
        },
        render: function() {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.Signature_mobile", Signature_mobile);
}());
