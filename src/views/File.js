(function(){
	var File = PMDynaform.view.Field.extend({
		item: null,
		firstLoad : true,
		isIE : false,
		template: _.template( $("#tpl-file").html()),
		events: {
			"click .pmdynaform-file-container .form-control": "onClickButton",
			"click div[name='button-all'] .pmdynaform-file-buttonup": "onUploadAll",
			"click div[name='button-all'] .pmdynaform-file-buttoncancel": "onCancelAll",
			"click div[name='button-all'] .pmdynaform-file-buttonremove": "onRemoveAll"
		},
		initialize: function () {
			//this.setOnChangeFiles();
			this.model.on("change", this.render, this);
		},
		onClickButton: function (event) {
			this.$el.find("input").trigger( "click" );
			event.preventDefault();
			event.stopPropagation();
			
			return this;
		},
		onUploadAll: function (event) {
			var i,
			length = this.model.get("items").length;

			this.$el.find("div[name='button-all'] .pmdynaform-file-buttonup").hide();
			this.$el.find("div[name='button-all'] .pmdynaform-file-buttonremove").hide();
			this.$el.find("div[name='button-all'] .pmdynaform-file-buttoncancel").show();
			for (i=0; i<length; i+=1) {
				this.model.uploadFile(i);	
			}

			event.preventDefault();
			event.stopPropagation();

			return this;
		},
		onCancelAll: function (event) {
			var i,
			length = this.model.get("items").length;

			this.$el.find("div[name='button-all'] .pmdynaform-file-buttonup").show();
			this.$el.find("div[name='button-all'] .pmdynaform-file-buttonremove").show();
			this.$el.find("div[name='button-all'] .pmdynaform-file-buttoncancel").hide();
			for (i=0; i<length; i+=1) {
				this.model.stopUploadFile(i);
			}
			event.preventDefault();
			event.stopPropagation();

			return this;
		},
		onRemoveAll: function (event) {

			this.model.set("items", []);
			this.render();
			event.preventDefault();
			event.stopPropagation();

			return this;
		},
		removeItem: function (event) {
			var items = this.model.get("items"),
			index = $(event.target).data("index");

			items.splice(index, 1);
			this.model.set("items", items);
			this.render();
			return this;
		},
		addNewItem: function (e, file) {
			if (this.validate(e, file)) {
				var items = this.model.get("items");
				items.push({
					event: e,
					file: file
				});
				this.model.set("items", items);
				/*
				 * The call to render method will not be necessary if the change event from initialize
				 * method would be working
				 **/
				this.render();
				if (items.length > 0) {
					this.$el.find(".pmdynaform-file-container div[name='button-all']").show();
				} else {
					this.$el.find(".pmdynaform-file-container div[name='button-all']").hide();
				}
			}
						
			return this;
		},
		onUploadItem: function (event) {
			var index = $(event.target).data("index");

			this.model.uploadFile(index);
			return this;
		},
		onCancelUploadItem: function () {
			var index = $(event.target).data("index");

			this.model.stopUploadFile(index);
			return this;
		},
		onToggleButtonUpload: function (event, action) {
			var i,
			j,
			buttons = $(event.target).parent().parent(),
			opt = {
				up: {
					show: [
						".pmdynaform-file-buttonstop",
						".pmdynaform-file-buttoncancel"
					],
					hide: [
						".pmdynaform-file-buttonup",
						".pmdynaform-file-buttonremove"
					]
				},
				cancel: {
					show: [
						".pmdynaform-file-buttonup",
						".pmdynaform-file-buttonremove"
					],
					hide: [
						".pmdynaform-file-buttonstop",
						".pmdynaform-file-buttoncancel"
					]
				}
			};

			for ( i=0; i<opt[action].hide.length; i+=1 ) {
				buttons.find(opt[action].hide[i]).hide();
			} 

			for ( j=0; j<opt[action].show.length; j+=1 ) {
				buttons.find(opt[action].show[j]).show();
			} 
			
			return this;
		},
		onClosePreview: function () {

			return this;
		},
		onPreviewItem: function (event) {
			var file,
			that = this,
			reader,
			previewFile,
			resource,
			previewFileName,
			heightContainer,
			shadow = document.createElement("div"),
			background = document.createElement("div"),
			preview = document.createElement("img"),
			index = $(event.target).data("index"),
			closeItem = document.createElement("span");
			closeItem.className = "glyphicon glyphicon-remove";

			closeItem.title = "Close";
			closeItem.setAttribute("data-placement", "bottom");
			
			$(closeItem).tooltip().click(function(e) {
				$(this).tooltip('toggle');
				event.preventDefault();
				event.stopPropagation();
			});

			heightContainer = $(document.documentElement).height();

			shadow.className = "pmdynaform-file-shadow";
			shadow.style.height = heightContainer + "px";
			background.className = "pmdynaform-file-preview-background"
			background.style.height = heightContainer + "px";
			background.setAttribute("contenteditable", "true");
			
			background.appendChild(closeItem);
			
			file = this.model.get("items")[index].file;
			
			if (file.type.match(/image.*/)) {
				reader  = new FileReader();
				reader.onloadend = function () {
					preview.src = reader.result;
				}
				reader.readAsDataURL(file);
				//preview.style.top = document.body.scrollTop + "px";	
			} else if(file.type.match(/audio.*/)) {
				resource = _.template( $("#tpl-audio").html());
				preview = resource({
					path: URL.createObjectURL(file),
					top: document.body.scrollTop + "px"
				});

			} else if(file.type.match(/video.*/)) {
				resource = _.template( $("#tpl-video").html());
				preview = resource({
					path: URL.createObjectURL(file),
					top: document.body.scrollTop + "px"
				});
			}/* else {

			}*/

			previewFile = document.createElement("div");
			previewFileName = document.createElement("p");
			previewFileName.name = "desc";
			previewFileName.appendChild(document.createTextNode(file.extra.nameNoExtension));
			previewFile.className = "pmdynaform-file-preview-image";
			
			$(previewFile).append(preview);
			$(previewFile).append(previewFileName);
			$(closeItem).on('click', function (event){
				document.body.removeChild(shadow);
				document.body.removeChild(background);
				that.$el.parents(".pmdynaform-container").css("position","");
				event.preventDefault();
				event.stopPropagation();
			});
			$(background).keyup(function(event){
				if (event.which === 27) {
					document.body.removeChild(shadow);
					document.body.removeChild(background);
					that.$el.parents(".pmdynaform-container").css("position","");
					event.preventDefault();
					event.stopPropagation();	
				}
			});
			
			$(background).append(previewFile);
			document.body.appendChild(shadow);
			document.body.appendChild(background);

			//Puts the parent node in Fixed mode
			this.$el.parents(".pmdynaform-container").css("position","fixed");

			return this;
		},
		renderFiles: function () {
			var i,
			that = this,
			items = this.model.get("items");

			for (i=0; i< items.length; i+=1) {
				if (that.model.get("preview")) {
					that.createBox(i, items[i].event,items[i].file);
				} else {
					that.createListBox(i, items[i].event, items[i].file);
				}
			}
			
			return this;
		},
		createButtonsHTML: function (index, opts) {
			var that = this,
			buttonGroups = document.createElement("div"),
			buttonGroupUp = document.createElement("div"),
			buttonGroupCancel = document.createElement("div"),
			buttonGroupRemove = document.createElement("div"),
			buttonGroupView = document.createElement("div"),
			buttonUp = document.createElement("button"),
			buttonCancel = document.createElement("button"),
			buttonRemove = document.createElement("button"),
			buttonView = document.createElement("button");

			/*
			 * Adding Options buttons to file
			 **/
			buttonGroups.className = "btn-group btn-group-justified";

			buttonGroupUp.className = "pmdynaform-file-buttonup btn-group";
			buttonGroupCancel.className = "pmdynaform-file-buttoncancel btn-group";
			buttonGroupCancel.style.display = "none";
			buttonGroupRemove.className = "pmdynaform-file-buttonremove btn-group";
			buttonGroupView.className = "pmdynaform-file-buttonview btn-group";

			buttonUp.className = "glyphicon glyphicon-upload btn btn-success btn-sm";
			buttonCancel.className = "glyphicon glyphicon-remove btn btn-danger btn-sm";
			buttonRemove.className = "glyphicon glyphicon-trash btn btn-danger btn-sm";
			buttonView.className = "glyphicon glyphicon-zoom-in btn btn-primary btn-sm";

			$(buttonUp).data("index", index);
			$(buttonCancel).data("index", index);
			$(buttonRemove).data("index", index);
			$(buttonView).data("index", index);
			
			$(buttonUp).on("click", function(event) {
				that.onToggleButtonUpload(event, "up");
				that.onUploadItem(event);
				event.stopPropagation();
				event.preventDefault();
			});
			$(buttonCancel).on("click", function(event) {
				that.onToggleButtonUpload(event, "cancel");
				that.onCancelUploadItem(event);
				event.stopPropagation();
				event.preventDefault();
			});
			$(buttonRemove).on("click", function(event) {
				that.removeItem(event);
				event.stopPropagation();
				event.preventDefault();
			});			
			$(buttonView).on("click", function(event) {
				that.onPreviewItem(event);
				event.stopPropagation();
				event.preventDefault();
			});

			buttonGroupUp.appendChild(buttonUp);
			buttonGroupCancel.appendChild(buttonCancel);
			buttonGroupRemove.appendChild(buttonRemove);
			buttonGroupView.appendChild(buttonView);
			if (opts.upload) {
				buttonGroups.appendChild(buttonGroupUp);
			}
			if (opts.cancel) {
				buttonGroups.appendChild(buttonGroupCancel);
			}
			if (opts.preview) {
				buttonGroups.appendChild(buttonGroupView);
			}
			if (opts.remove) {
				buttonGroups.appendChild(buttonGroupRemove);	
			}
			
			return buttonGroups;
		},
		createBox: function (index, e, file) {
			var buttonGroups,
			resource,
			enabledPreview = true,
			rand = Math.floor((Math.random()*100000)+3),
			imgName = file.name,
			src = e.target.result,
			template = document.createElement("div"),
			resizeImage = document.createElement("div"),
			preview = document.createElement("span"),
			progress = document.createElement("div"),
			imgPreview = document.createElement("img"),
			spanOverlay = document.createElement("span"),
			spanUpDone = document.createElement("span"),
			typeClasses = {
				video: {
					className: "pmdynaform-file-boxpreview-video",
					icon: "glyphicon glyphicon-facetime-video"
				},
				audio: {
					className: "pmdynaform-file-boxpreview-audio",
					icon: "glyphicon glyphicon-music"
				},
				file: {
					className: "pmdynaform-file-boxpreview-file",
					icon: "glyphicon glyphicon-book"
				}
			},
			fileName = file.extra.extension.toUpperCase();

			template.id = rand;
			template.className = "pmdynaform-file-containerimage";

			resizeImage.className = "pmdynaform-file-resizeimage";
			if (file.type.match(/image.*/)) {
				imgPreview.src = src;
				resizeImage.appendChild(imgPreview);
			} else if(file.type.match(/audio.*/)) {
				resizeImage.innerHTML = '<div class="'+ typeClasses['audio'].className +' thumbnail ' + typeClasses['audio'].icon+'"><div>'+ fileName +'</div></div>'; 
			} else if(file.type.match(/video.*/)) {
				resizeImage.innerHTML = '<div class="'+ typeClasses['video'].className +' thumbnail ' + typeClasses['video'].icon+'"><div>'+ fileName +'</div></div>'; 
			} else {
				enabledPreview = false,
				resizeImage.innerHTML = '<div class="'+ typeClasses['file'].className +' thumbnail ' + typeClasses['file'].icon+'"><div>'+ fileName +'</div></div>'; 
			}
			spanOverlay.className = "pmdynaform-file-overlay";
			spanUpDone.className = "pmdynaform-file-updone";
			spanOverlay.appendChild(spanUpDone);
			resizeImage.appendChild(spanOverlay);

			preview.id = rand;
			preview.className = "pmdynaform-file-preview";
			preview.appendChild(resizeImage);	    	

			progress.id = rand;
			progress.className = "pmdynaform-file-progress";
			progress.innerHTML = "<span></span>";

			template.appendChild(preview);
			buttonGroups = this.createButtonsHTML(index, {
				upload: true,
				preview: enabledPreview,
				cancel: true,
				remove: true
			});
			template.appendChild(buttonGroups);
			template.appendChild(progress);
			this.$el.find(".pmdynaform-file-droparea").append(template);
			
			return this;
			
		},
		createListBox: function (index, e, file) {
			var buttonGroups,
			enabledPreview = true,
			iconFile,
			rand = Math.floor((Math.random()*100000)+3),
			listItem = document.createElement("div"),
			label = document.createElement("div");

			listItem.className = "pmdynaform-file-listitem";
			if (file.type.match(/image.*/)) {
				//
			} else if(file.type.match(/audio.*/)) {
				//
			} else if(file.type.match(/video.*/)) {
				//
			} else {
				enabledPreview = false;				
			}
			buttonGroups = this.createButtonsHTML(index, {
				upload: true,
				preview: enabledPreview,
				cancel: true,
				remove: true
			});
			buttonGroups.style.width = "50%";
			label.className = "pmdynaform-label-nowrap";
			label.innerHTML = file.name;
			listItem.appendChild(label)
			listItem.appendChild(buttonGroups);
			

			this.$el.find(".pmdynaform-file-list").append(listItem);

			return this;
		},
		toggleButtonsAll: function () {
			//Select the name="button-all" for show the buttons

			return this;
		},
		render: function () {
			var that = this,
			fileContainer,
			fileControl,
			oprand,
			hidden,
			name,
			fileButton,
			fileControl;
			
			this.$el.html( this.template(this.model.toJSON()));
			
			fileControl = this.$el.find("input[type='file']");
			fileButton = that.$el.find("button[type='button']");
			hidden = this.$el.find("input[type='hidden']");
			fileContainer = this.$el.find(".pmdynaform-file-control")[0];
			
			if (this.model.get("hint")) {
				this.enableTooltip();
			}

			if ((navigator.userAgent.indexOf("MSIE") != -1) || (navigator.userAgent.indexOf("Trident") != -1)) {
				fileControl.css({visibility:"inherit", width : "100%"});
				fileButton.css({display:"none"});
				this.isIE = true;
			}
			
			/*this.renderFiles();
			this.toggleButtonsAll();
			oprand = {
				dragClass : "pmdynaform-file-active",
				dnd: this.model.get("dnd"),
				multiple: this.model.get("multiple"),
				on: {
					load: function (e, file) {
						that.addNewItem(e, file);
					}
				}
			};*/
			
			if (this.model.get("group") === "grid") {
				name = this.model.get("name");
				name = name.substring(0,name.length-1).concat("_label]");
				hidden[0].name = hidden[0].id = "form" + name;
			}else{
				hidden[0].name = hidden[0].id = "form[" + this.model.get("name")+"_label]";
			}
			
            hidden.val(JSON.stringify(this.model.get("data")["label"]));
            			
			fileControl.change(function(e, ui){
				var file = e.target, nameFileLoad;
				if (file.value  && that.isValid(file)){
					if (file.files){
						nameFileLoad = file.files[0].name;
					} else {
						nameFileLoad = file.value.split("\\")[2]
					}
					that.$el.find("button[type='button']").text(nameFileLoad);
					if ( that.model.get("data")["label"].length) {
						if ( that.model.get("data")["label"].indexOf(nameFileLoad) === -1  ) {
							if (that.firstLoad){
								that.model.get("data")["label"].push(nameFileLoad);
							}else{
								that.model.get("data")["label"].splice(that.model.get("data")["label"].length-1);
								that.model.get("data")["label"].push(nameFileLoad);
							}
						}
					} else {
						that.model.get("data")["label"].push(nameFileLoad);
					}
					hidden.val(JSON.stringify(that.model.get("data")["label"]));
					that.firstLoad = false;
				}
			});
			//PMDynaform.core.FileStream.setupInput(fileControl, oprand); 
			return this;
		},
		isValid : function (file) {
			var validated = false, extensions, maxSize, type, fileTarget;
			extensions = this.model.get("extensions");
			if (file.files){
				if (file.files[0]){
					type = file.files[0].name.substring(file.files[0].name.lastIndexOf(".")+1);
					fileTarget = file.files[0].name;
				}
			} else {
				if (file.value.trim().length){
					type = file.value.split("\\")[2].substring(file.value.split("\\")[2].lastIndexOf(".")+1);
					fileTarget = file.value;
				}
			}

			if (this.model.get("sizeUnity").toLowerCase() !== "kb" ){
				maxSize = parseInt(this.model.get("size"),10)*1024;
			} else {
				maxSize = parseInt(this.model.get("size"),10);
			}
			if (extensions === "*"){
				validated = true;
			} else {
				if (this.model.get("extensions").indexOf(type) > -1) {
					validated = true;
				}else{
					alert("The extension of the file is not supported for the field...");
					validated = false;
				}
			}
			if (validated && file.files){
				if ( file.files[0] && (file.files[0].size/1024 <= maxSize) ){
					validated = true
				}else{
					alert("File \""+file.name+"\" is too big. \n Max allowed size is "+ maxSize +" Kb.");
					validated = false;
				}				
			}
			if (validated){
				this.model.attributes.value = fileTarget || "";
				if (this.validator){
					this.validator.$el.remove();
					this.$el.removeClass('has-error has-feedback');					
				}
			}
			return validated;
		},

		validate: function(e) {
			var tagFile = this.$el.find("input[type='file']")[0], validated = true;
			if(this.validator){
				this.validator.$el.remove();
				this.$el.removeClass('has-error has-feedback');
			}
			if (!this.model.isValid()){
				this.validator = new PMDynaform.view.Validator({
					model: this.model.get("validator")
				});
				$(tagFile).parent().append(this.validator.el);
				if (!this.isIE){
					this.validator.el.style.top = "-23px";
				}
				this.applyStyleError();
			}else{
				validated = true;
			}
			return validated;
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.File",File);
}());
