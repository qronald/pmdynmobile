(function(){
	var ButtonView = Backbone.View.extend({
		template: _.template( $("#tpl-button").html()),
		events: {
	        "keydown": "preventEvents"
	    },
		initialize: function () {
			this.model.on("change", this.render, this);
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
        on: function (e, fn) {
        	var that = this;
        	$(this.$el.find("button")[0]).on(e, function(event){
        		fn(event, that);
        		
        		event.stopPropagation();
        	});
        	return this;
        },
		render: function() {
			this.$el.html( this.template(this.model.toJSON()) );
			return this;
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.Button",ButtonView);
	
}());
