(function(){

    var SuggestView = PMDynaform.view.Field.extend({
        template: _.template($("#tpl-text").html()),
        templateList: _.template($("#tpl-suggest-list").html()),
        //templateElement: _.template($("#tpl-suggest-element").html()),        
        validator: null,
        elements: [],
        input: null,
        containerList: null,
        makeFlag: false,
        keyPressed: false,
        pointerItem: 0,
        orientation: "under",
        stackItems: [],
        stackRow: 0,
        clicked: false,
        firstLoad : true,
        dirty : false,
        jsonData : {}, 
        previousValue : "",
        dependentFields : [],
        dependentFieldsData : [],
        events: {
                "click li": "continueDependentFields",
                "keyup input": "validate",
                "keydown input": "refreshBinding"
        },
        onChangeCallback: function (){},
        setOnChange : function (fn) {
            if (typeof fn === "function") {
                this.onChangeCallback = fn;
            }
            return this;
        },
        initialize: function () {
            var that = this;
            //this.model.on("change:value", this.onChange, this);
            this.model.on("change", this.checkBinding, this);
            this.containerList = $(this.templateList());
            this.enableKeyUpEvent();
        },
        refreshBinding: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            this.keyPressed = true;
            return this;
        },
        checkBinding: function (event) {
            //If the key is not pressed, executes the render method
            var form = this.model.get("form");
            if ( typeof this.onChangeCallback === 'function' ) {
                this.onChangeCallback(this.getValue(), this.previousValue);
            }

            if ( form && form.onChangeCallback ) {
                form.onChangeCallback(this.model.get("name"), this.model.get("value"), this.previousValue);
            }
            if ((this.keyPressed === false) &&
                (this.clicked === false)) {
                this.render();
                this.updateValueInput();   
            }
            if (this.clicked) {
                this.onChange(event);
            }
        },
        updateValueInput : function () {
            var input, value;
            input = this.$el.find("input[type='suggest']");
            if (this.model.get("data")) {
                input.val(this.model.get("data")["label"]); 
            }
            return this;
        },
        setValueDefault: function(){
            this.model.set("value",$(this.el).find(":input").val()); 
        },
        hideSuggest : function (){
            this.containerList.hide();
            this.stackRow = 0;
        },
        showSuggest : function (){
            this.containerList.show();
        },
        _attachSuggestGlobalEvents: function() {
          if (this.containerList) {
             $(document).on("click."+this.$el, $.proxy(this.hideSuggest, this)); 
          }
        },
        _detachSuggestGlobalEvents: function() {
          if (!this.containerList) {
             $(document).off("click."+this.$el); 
          }
        },
        onChange: function (event)  {
            var i, 
            j, 
            item, 
            dependents, 
            viewItems, 
            valueSelected,
            hidden,
            nameField,
            fieldDependentName, sql;
            this.previousValue = this.model.get("value");
            if ( !this.firstLoad ) {
                hidden = this.$el.find("input[type='hidden']");
                if (hidden.length && this.model.get("value")){
                    valueSelected = this.model.get("value");
                    hidden.val(valueSelected||"");
                }
                viewItems = this.parent.items.asArray();
                if (this.model.get("group") === "grid") {
                    nameField = this.model.get("columnName");
                    for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
                        if (nameField !== viewItems[i].model.get("columnName")) {
                            sql = viewItems[i].model.get("sql");
                            if (sql && (
									sql.indexOf("@#"+nameField) > -1 ||
									sql.indexOf("@@"+nameField) > -1 ||
									sql.indexOf("@="+nameField) > -1 ||
									sql.indexOf("@%"+nameField) > -1 ||
									sql.indexOf("@$"+nameField) > -1 ||
									sql.indexOf("@?"+nameField) > -1 
									)
								) {
                                if (event) {
                                    if (viewItems[i].onDependentHandler){
                                        //this.jsonData[nameField] = this.model.get("value");
                                        viewItems[i].onDependentHandler();
                                    }
                                }
                            }
                        }                   
                    }
                } else {
                    if (this.model.get("variable") && this.model.get("variable").trim().length){
                        nameField = this.model.get("variable"); 
                    }else{
                        nameField = this.model.get("id");
                    }
                    for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
                        if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
                            fieldDependentName = viewItems[i].model.get("id");
                        }
                        if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
                            fieldDependentName = viewItems[i].model.get("name");
                        }
                        if (fieldDependentName !== nameField) {
                            sql = viewItems[i].model.get("sql");
                            if (sql && (
									sql.indexOf("@#"+nameField) > -1 ||
									sql.indexOf("@%"+nameField) > -1 ||
									sql.indexOf("@@"+nameField) > -1 ||
									sql.indexOf("@?"+nameField) > -1 ||
									sql.indexOf("@$"+nameField) > -1 ||
									sql.indexOf("@="+nameField) > -1 )
								) {
                                if (event) {
                                    if (viewItems[i].onDependentHandler){
                                        viewItems[i].onDependentHandler();
                                    }
                                }
                            }
                        }                   
                    }
                }
                
                /*dependents = this.model.get("dependentFields") ? 
                this.model.get("dependentFields"): [];
                viewItems = this.parent.items.asArray();           
                if (dependents.length > 0) {
                    for (i = 0; i < viewItems.length; i+=1) {
                        for (j = 0; j < dependents.length; j+=1) {
                            item = viewItems[i].model.get("name");  
                            if(dependents[j] === item) {
                                if (event) {
                                    if (viewItems[i].onDependentHandler) {
                                        viewItems[i].onDependentHandler();
                                        viewItems[i].render();
                                        if(viewItems[i].setValueDefault){
                                            viewItems[i].setValueDefault();
                                        }                                          
                                    }
                                }
                            }
                        }
                    }
                }*/
                this.clicked = false;
            }
            return this;
        },
        createDependencies : function () {
            var i, 
            j, 
            item, 
            dependents, 
            viewItems;
            dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
            viewItems = this.parent.items.asArray();            
            if (dependents.length > 0) {
                for (i = 0; i < viewItems.length; i+=1) {
                    for (j = 0; j < dependents.length; j+=1) {
                        item = viewItems[i].model.get("name");
                        if (dependents[j] === item) {
                            if (viewItems[i].model.setDependencies) {
                                viewItems[i].model.setDependencies(this);
                            }
                        }
                    }
                }
            }
            return this;
        },
        makeElements: function (maxItems, event){
            var that = this,
            elementTpl,
            itemLabel,
            val,
            founded = false,
            count = 0;

            this.input = this.$el.find("input[type='suggest']");
            val = this.input.val();
            this.elements = [];
            this.elements = this.model.get("options");
            this._detachSuggestGlobalEvents();
            this.showSuggest();
            this.stackItems = [];

            if (this.containerList !== null) {
                this.containerList.empty();
            }
            
            if (val !== "") {
                $.grep(that.elements, function(data, index){
                    itemLabel = data.label;
                    if ( (itemLabel.toLowerCase().indexOf(val.toLowerCase()) !== -1) && count < maxItems) {

                        that.updatingItemsList(data);
                        founded = true;

                        $(that.stackItems[that.stackRow]).addClass("pmdynaform-suggest-list-keyboard");
                        count += 1;
                    }
                });
                if (!founded) {
                    that.hideSuggest();
                }
            } else {
                this.hideSuggest();
            }
        },
        updatingItemsList: function (data) {
            var li = document.createElement("li"),
            span = document.createElement("span");

            span.innerHTML = data.label;
            span.setAttribute("data-value", data.value);
            span.setAttribute("selected", false);
            li.appendChild(span);
            li.className = "list-group-item";
            /*var elementTpl = this.templateElement({
                value: data.value, 
                label:data.label,
                selected: false
            });*/

            this.stackItems.push(li);

            this.containerList.append(li); 

            this.input.after(this.containerList);
            this.containerList.css("position", "absolute");
            this.containerList.css("zIndex", 3);
            this.containerList.css("border-radius", "5px");
            
            if (this.stackItems.length > 4) {
                this.containerList.css("height","200px");
            } else {
                this.containerList.css("height","auto");
            }
            
            this._attachSuggestGlobalEvents();
            //this.onChange(event);
            return this;
        },
        continueDependentFields: function (e) {
            var newValue,
            content;
            this.model.set("clickedControl",true);
            this.clicked = true;
            this.keyPressed = false;
            content = $(e.currentTarget).text();
            //newValue = $(this.el).find(":input").val();          
            $(this.el).find(":input[type='suggest']").val(content);
            this.model.set("value", $(e.currentTarget).find("span").data().value);
            this.containerList.remove();
            this.stackRow = 0;
            this.clicked = false; 
            return this;
        },
        validate: function(event){
            //this.clicked = event.type === "submit" ? false : true;
            if (event && (event.which === 9) && (event.which !==0)) { //tab key
                this.keyPressed = true;
            }
            if (!this.model.get("disabled")) {
                if (event && event.type === "submit"){
                    this.keyPressed = true;
                    this.model.set({value: this.$el.find("input[type='suggest']").val()},{ validate: true});
                } else {
                    this.model.attributes.value = this.$el.find("input[type='suggest']").val();
                    this.model.attributes.validator.set("valid",true);
                    this.model.validate(this.model.toJSON());
                }

                if (this.validator) {
                    this.validator.$el.remove();
                    this.$el.removeClass('has-error has-feedback');
                }
                if(!this.model.isValid()){
                    this.validator = new PMDynaform.view.Validator({
                        model: this.model.get("validator")
                    });
                    this.$el.find("input[type='suggest']").parent().append(this.validator.el);
                    this.applyStyleError();
                }
            }
            this.keyPressed = false;
            if (event && event.type !== "submit") {
                this.makeElements(10);
            }
            return this;
        },
        getData: function() {
            return this.model.getData();
        },
        render: function() {
            var data, 
            dependenciesField,
            that = this,
            hidden,
            name;
            this.createDependencies();
            /*create HTML*/
            //this.verifyData();

            this.$el.html(this.template(this.model.toJSON()));

            if (this.model.get("hint") !== "") {
                this.enableTooltip();
            }
            data = this.model.get("data");
            if (this.firstLoad){
                if ( this.model.get("value") && data) {
                        this.$el.find("input[type='suggest']").val(data["label"]? data["label"] :"");
                } else {
                    this.model.emptyValue();
                }
            }else{
                this.$el.find("input[type='suggest']").val(this.model.get("value")||"");
            }
            if (this.model.get("group")=== "form") {
                this.getDependeciesField();
            }else{
                this.getDependeciesFieldGrid();
            }
            this.setNameHiddenControl();
            dependenciesField = this.model.get("dependenciesField");
            this.$el.find("input[type='suggest']").focusin(function(event){
                var data = {}, remoteOptions, options, option, i, val, value;
                that.clicked = true;
                that.dependentFields = [];
                that.dependentFieldsData = [];
                if (that.model.get("group") === "grid"){
                    that.getDependeciesFieldGrid();
                }else{
                    that.getDependeciesField();
                }
                if (that.dependentFields.length){
                    if (that.firstLoad){
                        if (!that.dirty){
                            $(this).empty();
                            if (that.model.get("group") === "grid"){
                                that.jsonData = that.generateDataDependenField2();
                            }else{
                                that.jsonData = that.generateDataDependenField();
                            }
                            remoteOptions = that.executeQuery(data);
                            that.mergeOptions(remoteOptions);
                            that.dirty = true;
                        }
                        that.firstLoad = false;
                    }
                } else {
                    that.firstLoad = false;
                }
            });
            /*if (!that.firstLoad && this.dirty){
                this.$el.find("input[type='suggest']").val(data["label"]? data["label"] :"");
            }*/
            this.$el.find("input[type='suggest']").focus();
            this.setValueToDomain();

            if (this.model.get("name").trim().length === 0){
                this.$el.find("input[type='suggest']").attr("name","");
                this.$el.find("input[type='hidden']").attr("name","");
            }
            return this;
        },
        onDependentHandler: function (target , datavalue) {
            var i, localOpt, remoteOptions;
            //data = this.generateDataDependenField();
            this.jsonData = this.generateDataDependenField2();
            remoteOptions = this.executeQuery();
            this.mergeOptions(remoteOptions);
            this.$el.find("input[type='suggest']").val(""); 
            this.$el.find("input[type='text']").val("");
            this.$el.find("input[type='hidden']").val("");
            this.firstLoad = false;
            return this;
        },
        executeQuery : function (){
            var restClient, resp, prj, endpoint, url, data = this.jsonData;
            if (this.model.get("group") === "grid"){
                data["field_id"] = this.model.get("columnName");
            }else{
                data["field_id"] = this.model.get("id");
            }
            if ( this.model.get("form") ) {
                if (this.model.get("form").model.get("form")){
                    data["dyn_uid"] = this.model.get("form").model.get("form").model.get("id");             
                }else{
                    data["dyn_uid"] = this.model.get("form").model.get("id");
                } 
            }
            prj = this.model.get("project");
            endpoint = this.model.getEndpointVariable({
                            type: "executeQuery",
                            keys: {
                                "{var_name}": this.model.get("var_name") || ""
                            }
                        });
            url = prj.getFullURL(endpoint);         
            resp = [];
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: "POST",
                data: data,
                keys: prj.token,
                successCallback: function (xhr, response) {
                    resp = response;
                }
            });
            return resp;
        },
        mergeOptions : function (remoteOptions){
            var k, remoteOpt = [], localOpt = [], options;
            for ( k = 0; k < remoteOptions.length; k+=1) {
                remoteOpt.push({
                    value : remoteOptions[k].value,
                    label : remoteOptions[k].text
                });
            }
            localOpt = this.model.get("localOptions");
            this.model.set("remoteOptions", remoteOpt);
            options = localOpt.concat(remoteOpt);
            this.model.set("options", options);
            return options;
        },
        generateDataDependenField : function () {
            var data, dependenciesField, name, value, i;
            dependenciesField = this.model.get("dependenciesField");
            data = {};
            for ( i = 0 ; i  < dependenciesField.length ; i+=1 ) {
                name = dependenciesField[i].model.get("name");
                if ( dependenciesField[i].model.get("type") === "text"){
                    value = dependenciesField[i].model.get("keyValue");
                }else{
                    value = dependenciesField[i].model.get("value");
                }
                data[name] = value;
            }
            return data;
        },
        toggleItemSelected: function () {
            $(this.stackItems).removeClass("pmdynaform-suggest-list-keyboard");
            $(this.stackItems[this.stackRow]).addClass("pmdynaform-suggest-list-keyboard");

            return this;
        },
        enableKeyUpEvent: function () {
            var that = this, 
            code,
            containerScroll;
            
            this.$el.keyup(function (event) {
                if (that.stackItems.length >0) {
                    code = event.which;
                    if (code === 38) { // UP
                        if (that.stackRow > 0) {
                            that.stackRow-=1;
                            that.toggleItemSelected();
                        }
                        that.containerList.scrollTop(-10*parseInt(that.stackRow+1));
                    } 
                    if (code === 40) { // DOWN
                        if (that.stackRow < that.stackItems.length-1) {
                            that.stackRow+=1;
                            that.toggleItemSelected();
                        }
                        that.containerList.scrollTop(+10*parseInt(that.stackRow+1));
                    }
                    if ((code === 13)) { //ENTER
                        that.continueDependentFields({
                            currentTarget: $(event.currentTarget).find(".pmdynaform-suggest-list-keyboard")[0]
                        });
                    }
                }
            });
            
        },
        updateValueControl: function () {
            var inputVal = this.$el.find("input[type='suggest']").val();

            this.model.set("value", inputVal);

            return this;    
        },
        getHTMLControl: function () {
            return this.$el.find("input[type='suggest']");
        },
        afterRender : function () {
            //this.continueDependentFields();
            return this;
        },
        getDependeciesField : function () {
            var i, items, nameField, j, sql;
            nameField = this.model.get("name");
            if (this.parent && this.parent.model.get("items").length) {
                items  = this.parent.model.get("items");
                for ( i = 0 ; i < items.length ; i+=1 ) {
                    if (items[i]){
                        for ( j = 0 ; j < items[i].length ; j+=1 ){
                            if (items[i][j].name && nameField !== items[i][j].name) {
                                sql = this.model.get("sql");
                                if (sql && 
									(
										sql.indexOf("@#"+items[i][j].name) > -1 ||
										sql.indexOf("@@"+items[i][j].name) > -1 ||
										sql.indexOf("@%"+items[i][j].name) > -1 ||
										sql.indexOf("@="+items[i][j].name) > -1 ||
										sql.indexOf("@?"+items[i][j].name) > -1 ||
										sql.indexOf("@$"+items[i][j].name) > -1 
									)
								) {
                                    if(this.dependentFields.indexOf(items[i][j].name) === -1) {
                                        this.dependentFields.push(items[i][j].name);
                                        this.dependentFieldsData.push(items[i][j]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        getDependeciesFieldGrid : function () {
            var i, items, nameField, j, sql;
            nameField = this.model.get("columnName");
            if (this.parent && this.parent.items.asArray().length) {
                items  = this.parent.items.asArray()
                for ( i = 0 ; i < items.length ; i+=1 ) {
                    if (items[i]){
                        if (items[i].model.get("name") && nameField !== items[i].model.get("name")) {
                            sql = this.model.get("sql");
                            if (sql && (
									sql.indexOf("@#" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@@" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@%" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@=" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@?" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@$" + items[i].model.get("columnName")) > -1 
								)
							) {
                                if(this.dependentFields.indexOf(items[i].model.get("columnName")) === -1) {
                                    this.dependentFields.push(items[i].model.get("columnName"));
                                    this.dependentFieldsData.push(items[i]);
                                }
                            }
                        }
                    }
                }
            }
        },
        generateDataDependenField2 : function () {
            var i, 
            j, 
            item, 
            dependents, 
            viewItems,
            label,
            control,
            hidden,
            sql, nameColumm, nameField, fieldDependentName, data = {};

            viewItems = this.parent.items.asArray();
            //find dependent fields
            if (this.model.get("group") === "grid") {
                nameField = this.model.get("columnName");
                for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
                    if (this.dependentFields.indexOf( viewItems[i].model.get("columnName")) !== -1 ) {
                        if ( viewItems[i].model.get("columnName") !== nameField){
                            if (viewItems[i].model.get("type") === "text"){
                                data[viewItems[i].model.get("columnName")] =  viewItems[i].fiendValueDependenField(viewItems[i].model.get("value"));
                            } else {
                                data[viewItems[i].model.get("columnName")] =  viewItems[i].model.get("value");
                            }
                        }
                    }
                }
            } else {
                if (this.model.get("variable") && this.model.get("variable").trim().length){
                    nameField = this.model.get("variable"); 
                }else{
                    nameField = this.model.get("id");
                }
                for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
                    if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
                        fieldDependentName = viewItems[i].model.get("id");
                    }
                    if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
                        fieldDependentName = viewItems[i].model.get("name");
                    }
                    if (fieldDependentName !== nameField) {
                        if (this.dependentFields.indexOf(fieldDependentName) !== -1 ) {
                            if (viewItems[i].model.get("type") === "text") {
                                data[fieldDependentName] = viewItems[i].fiendValueDependenField(viewItems[i].model.get("value"));
                            }else{
                                data[fieldDependentName] = viewItems[i].model.get("value");
                            }
                        }
                    }                   
                }
            }
            return data;
        }
    });

    PMDynaform.extendNamespace("PMDynaform.view.Suggest",SuggestView);
}());