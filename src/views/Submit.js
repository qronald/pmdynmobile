(function(){
	var SubmitView = Backbone.View.extend({
		template: _.template( $("#tpl-submit").html()),
		events: {
	        "keydown": "preventEvents"
	    },
		initialize: function (options){
			this.model.on("change", this.render, this);	
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
        on: function (e, fn) {
        	var that = this, 
        	control = this.$el.find("button");

        	if (control) {
        		control.on(e, function(event){
	        		fn(event, that);

	        		event.stopPropagation();
	        	});
        	}
        	
        	return this;
        },
		render: function() {
			this.$el.html( this.template(this.model.toJSON()) );
			return this;
		}
	});
	
	PMDynaform.extendNamespace("PMDynaform.view.Submit",SubmitView);
	
}());