(function(){
    
    var Label = PMDynaform.view.Field.extend({
        template: _.template($("#tpl-label").html()),
        validator: null,
        initialize: function (){
            this.model.on("change", this.render, this);
        },
		render: function() {
			var hidden, name;
			this.$el.html( this.template(this.model.toJSON()));
			if (this.model.get("originalType") === "checkbox") {
				this.$el.find("input[type='hidden']")[0].value = this.model.get("data")["value"];
				this.$el.find("input[type='hidden']")[1].value = JSON.stringify(JSON.parse(this.model.get("data")["label"]));
			}
			if (this.model.get("group") === "grid") {
				hidden = this.$el.find("input[type = 'hidden']")[0];
				name = this.model.get("name");
				name = name.substring(0,name.length-1).concat("]");	
				hidden.name = hidden.id = "form" + name;
				
				hidden.name = hidden.id = "form" + name;
				hidden = this.$el.find("input[type = 'hidden']")[1];
				name = this.model.get("name");
				name = name.substring(0,name.length-1).concat("_label]");
				hidden.name = hidden.id = "form" + name;
			}
			return this;
		}
    });

    PMDynaform.extendNamespace("PMDynaform.view.Label", Label);
}());
