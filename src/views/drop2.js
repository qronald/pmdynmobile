(function(){
	var DropDownView = PMDynaform.view.Field.extend({
		dirty : false,
		events: {
			"change select": "continueDependentFields",
            "blur select": "validate",
            "keydown select": "preventEvents",
            //"mousedown .pmdynaform-control-dropdown" : "onclickField"
		},
		clicked: false,
		firstTime : true,
		template: _.template( $("#tpl-dropdown").html()),
		initialize: function () {
			this.model.on("change", this.checkBinding, this);
			this.model.on("mousedown", this.functiontest, this);
		},
		functiontest : function (a){
			console.log(a);
		},
		checkBinding: function (event) {
			if (!this.clicked) {
				this.render();
				this.validate();
			}
			this.onChange(event);
			return this;
		},
		preventEvents: function (event) {
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
		setValueDefault: function(){
			var val = $(this.el).find(":selected").val();
			if(val!= undefined) {
				this.model.set("value",val);
			}
			else {
				this.model.set("value","");	
			}
		},
		onChange: function (event)  {
			var i, j, item, dependents, viewItems, valueSelected;
			if (!this.firstTime){			
				dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
				viewItems = this.parent.items.asArray();			
				if(this.dirty){	
					for ( i = 0 ; i < viewItems.length ; i+=1 ) {
						if (dependents.indexOf(viewItems[i].model.get("name")) !== -1){
							console.log(viewItems[i].model.get("name"));
							if (event) {
								if (viewItems[i].onDependentHandler) {
									valueSelected = this.model.get("value");
									viewItems[i].onDependentHandler(this.model.get("name"),valueSelected,viewItems[i]);
									viewItems[i].render();
									viewItems[i].setValueDefault();																
								}
							}
						}
					}
				}
			}
			this.clicked = false;

			return this;
		},
		createDependencies : function () {
			var i, j, item, dependents, viewItems;
			dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
			viewItems = this.parent.items.asArray();			
			if (dependents.length > 0) {
				for (i = 0; i < viewItems.length; i+=1) {
					for (j = 0; j < dependents.length; j+=1) {
						item = viewItems[i].model.get("name");
						if(dependents[j] === item) {
							if (viewItems[i].model.setDependencies) {
								viewItems[i].model.setDependencies(this);
							}
						}
					}
				}
			}

			return this;
		},		
		continueDependentFields: function () {
			var newValue, 
			auxValue;

			this.clicked = true;
			auxValue = $(this.el).find(":selected").val();
			newValue = (auxValue === undefined)? "" : auxValue;			
			this.model.set("value", newValue);
			this.changeValuesFieldsRelated();
			
			return this;
		},
		onDependentHandler: function (name, value, field) {
			var i,j, itemField, endpoint, data = {},localOpt,
			prj = this.model.get("project"),restClient,fullendpoint,url,
			dependenciesField = field.model.get("dependenciesField"),
			fields;
			this.dirty = false;
			for ( i = 0 ; i  < dependenciesField.length ; i+=1 ) {
				data[dependenciesField[i].model.get("name")] = dependenciesField[i].model.get("value"); 
			}
			itemField = field;
			endpoint =  itemField.model.getEndpointVariable({
			                type: "executeQuery",
			                keys: {
			                    "{var_name}": itemField.model.get("var_name") || ""
			                }
						});
			url = prj.getFullURL(endpoint);
			restClient = new PMDynaform.core.Proxy ({
			    url: url,
			    method: 'POST',
			    data: data,
			    keys: prj.token,
			    successCallback: function (xhr, response) {
			        var k, remoteOpt = [];
			        for (k =0; k< response.length; k+=1){
			            remoteOpt.push({
			                value: response[k].value,
			                label: response[k].text
			            });
			        }
			        localOpt = itemField.model.get("localOptions");
			        itemField.model.set("remoteOptions", remoteOpt);
			        itemField.model.set("options", localOpt.concat(remoteOpt));
			        itemField.firstTime = false;
			        itemField.dirty = false;
			    }
			});
			return this;
		},							
        validate: function(){
        	var drpValue;
        	if(!this.model.get("disabled")) {
        		drpValue = (this.model.get("options").length > 0)? this.$el.find("select").val() : "";
	            this.model.set({value: drpValue}, {validate: true});
	            if (this.validator) {
	                this.validator.$el.remove();
	                this.$el.removeClass('has-error');
	            }
	            if(!this.model.isValid()){
	                this.validator = new PMDynaform.view.Validator({
	                    model: this.model.get("validator"),
	                    domain: false
	                });  
	                this.$el.find("select").parent().append(this.validator.el);
	                this.applyStyleError();
	            }
        	}        	
            return this;
        },
        updateValueControl: function () {
            var i,
            options = this.$el.find("select").find("option");

            loop:
            for (i=0; i<options.length; i+=1) {
            	if (options[i].selected) {
            		this.model.set("value", options[i].value);
            		break loop;
            	}
            }

            return this;    
        },
        on: function (e, fn) {
        	var that = this, 
        	control = this.$el.find("select");
        	this.$el.find("select")[0].onload = function (){console.log()}
        	console.log("adwadawdawd");
        	if (control) {
        		control.on(e, function(event){
	        		fn(event, that);
	        		event.stopPropagation();
	        	});
        	}
        	return this;
        },
        getHTMLControl: function () {
            return this.$el.find("select");
        },
		render: function() {
			var that = this, dependenciesField, data;
			this.createDependencies();
			/*dependenciesField = this.model.get("dependenciesField").length;
			data = this.model.get("data");
			if (dependenciesField && data){
				this.model.attributes.options.push(data)
			}*/
			this.$el.html(this.template(this.model.toJSON()));
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			this.setValueToDomain();
			this.$el.find("select").mousedown(
				function (event) {
					var options,i, option, fulloptions;
					if ( that.model.get("dependenciesField").length ) {
						if ( that.firstTime ) {
							if (!that.dirty){
								$(this).empty();
								fulloptions = that.model.get("options");
								options = that.onclickField();
								for ( i = 0 ; i < options.length ; i+=1) {
									this.appendChild($("<option value="+options[i].value+">"+options[i].label+"</option>")[0]);
								}
								that.model.set("options",options);
							}
						} else {
							that.dirty = true; 
						}
					}else{
						that.dirty = true;
						that.firstTime = false;
					}
				}
			);
			return this;
		},
		afterRender : function () {
			this.continueDependentFields();
			return this;
		},
        onclickField : function (){
        	if ( this.model.get("dependenciesField").length ) {
        		this.dirty = true;
        		return this.loadRemoteOptions();
        	}
            return [];
        },
        loadRemoteOptions : function (){
        	var fieldsParent, i, data = {}, value, name, dependents, viewItems;
        	fieldsParent = this.model.get("dependenciesField");
        	dependents = this.model.attributes.dependentFields;
        	for ( i = 0 ; i < fieldsParent.length ; i+=1 ) {
        		name = fieldsParent[i].model.get("var_name");
        		value = fieldsParent[i].model.get("value");
        		data[name] = value;
        	}
        	return this.loadOptions(data);

        	/*for ( i = 0 ; i < dependents.length  ; i+=1 ) {
        		fieldDependent = window.getFieldByName(dependents[i]);
        		fieldDependent
        	}
			viewItems = this.parent.items.asArray();
			for ( i = 0 ; i < viewItems.length ; i+=1 ) {
				if (dependents.indexOf(viewItems[i].model.get("name")) !== -1){
					console.log(viewItems[i].model.get("name"));
					if (event) {
						if (viewItems[i].onDependentHandler) {
							valueSelected = this.model.get("value");
							viewItems[i].onDependentHandler(this.model.get("name"),valueSelected,viewItems[i]);
							viewItems[i].render();
							viewItems[i].setValueDefault();																
						}
					}
				}
			}*/
        },
        loadOptions : function (data) {
        	var endpoint, url, restClient, that = this, resp;
        	endpoint =  this.model.getEndpointVariable({
			                type: "executeQuery",
			                keys: {
			                    "{var_name}": this.model.get("var_name") || ""
			                }
						});
			url = this.project.getFullURL(endpoint);
			restClient = new PMDynaform.core.Proxy ({
			    url: url,
			    method: 'POST',
			    data: data,
			    keys: this.project.token,
			    successCallback: function (xhr, response) {
			        var k, remoteOpt = [], localOpt;
			        for (k =0; k< response.length; k+=1){
			            remoteOpt.push({
			                value: response[k].value,
			                label: response[k].text
			            });
			        }
			        resp = remoteOpt;
			        //localOpt = that.model.get("localOptions");
			        //that.model.set("remoteOptions", remoteOpt);
			        //that.model.set("options", localOpt.concat(remoteOpt));*/
			    }
			});
			return resp;
        }
	});

	PMDynaform.extendNamespace("PMDynaform.view.Dropdown",DropDownView);
	
}());
