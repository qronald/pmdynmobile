(function(){
	var DropDownView = PMDynaform.view.Field.extend({
		events: {
			"change select": "continueDependentFields",
            "blur select": "validate",
            "keydown select": "preventEvents"
		},
		clicked: false,
		jsonData : {}, 
		firstLoad : true,
		dirty : false,
		previousValue : null,
		triggerCallback : false,
		dependentFields : [],
		dependentFieldsData : [],
		template: _.template( $("#tpl-dropdown").html()),
        onChangeCallback: function (){},
        setOnChange : function (fn) {
            if (typeof fn === "function") {
                this.onChangeCallback = fn;
            }
            return this;
        },
		initialize: function () {
			this.model.on("change", this.checkBinding, this, {chage:true});
		},
		checkBinding: function (event) {
            var form = this.model.get("form"), 
            	data = {}, 
            	option, 
            	opts = this.model.get("options"), 
            	that = this,i;
            if ( typeof this.onChangeCallback === 'function' ) {
                this.onChangeCallback(this.getValue(), this.previousValue);
            }

            if ( form && form.onChangeCallback /*&& this.triggerCallback*/) {
                form.onChangeCallback(this.model.get("name"), this.model.get("value"), this.previousValue);
            	//this.triggerCallback = false;
            } /*else {
            	this.triggerCallback = true;
            }*/

			if (!this.clicked) {
				this.render();
				//this.validate();
			} else {
				this.$el.find("select").val(this.model.get("value"));
				//this.render();
			}
			if (!this.firstLoad){
				this.firstLoad = false;
			}
			this.onChange(event);
			return this;
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
		setValueDefault: function(){
			var val = $(this.el).find(":selected").val();
			if(val!= undefined) {
				this.model.set("value",val);
			}
			else {
				this.model.set("value","");	
			}
		},
		onChange: function (event)  {
			var i, 
			j, 
			item, 
			dependents, 
			viewItems,
			label,
			control,
			hidden,
			sql, nameColumm, nameField, fieldDependentName;
			if ( !this.firstLoad ) 
			{
				hidden = this.$el.find("input[type='hidden']");
				control = this.$el.find("select");
				if (hidden.length && control.length && this.model.get("value")){
					if ($(control).find("option[value="+this.model.get("value")+"]")[0]){
						label = $(control).find("option[value=" + this.model.get("value")+"]")[0].text.trim();
							hidden[0].value = label || "";
							this.model.set("data",{
								value : this.model.get("value"),
								label : label
							});
					}else{
						hidden.val("");
						for ( i = 0 ; i < this.model.get("options").length ; i+=1 ) {
							if ( this.model.get("value") === this.model.get("options")[i].value){
								hidden.val(this.model.get("options")[i].label);
								break;
							}
						}
					}
				}
				viewItems = this.parent.items.asArray();
				//find dependent fields
				if (this.model.get("group") === "grid") {
					nameField = this.model.get("columnName");
					for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
						if (nameField !== viewItems[i].model.get("columnName")) {
							sql = viewItems[i].model.get("sql");
							if (sql && (
									sql.indexOf("@#"+nameField) > -1 ||
									sql.indexOf("@@"+nameField) > -1 ||
									sql.indexOf("@="+nameField) > -1 ||
									sql.indexOf("@%"+nameField) > -1 ||
									sql.indexOf("@$"+nameField) > -1 ||
									sql.indexOf("@?"+nameField) > -1 )
								) {
								if (viewItems[i].onDependentHandler){
									viewItems[i].onDependentHandler();
								}
							}
						}					
					}
				} else {
					if (this.model.get("variable") && this.model.get("variable").trim().length){
						nameField = this.model.get("variable");	
					}else{
						nameField = this.model.get("id");
					}
					for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
						if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
							fieldDependentName = viewItems[i].model.get("id");
						}
						if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
							fieldDependentName = viewItems[i].model.get("name");
						}
						if (fieldDependentName !== nameField) {
							sql = viewItems[i].model.get("sql");
							if (sql && (
									sql.indexOf("@#"+nameField) > -1 ||
									sql.indexOf("@%"+nameField) > -1 ||
									sql.indexOf("@@"+nameField) > -1 ||
									sql.indexOf("@?"+nameField) > -1 ||
									sql.indexOf("@$"+nameField) > -1 ||
									sql.indexOf("@="+nameField) > -1 )
								) {
								if (viewItems[i].onDependentHandler){
									viewItems[i].firstLoad = false;
									viewItems[i].onDependentHandler();
								}
							}
						}					
					}
				}

				/*if (dependents.length > 0) {
					for (i = 0; i < viewItems.length; i+=1) {
						for (j = 0; j < dependents.length; j+=1) {
							item = viewItems[i].model.get("name");	
							if(dependents[j] === item) {
								if (event) {
									if (viewItems[i].onDependentHandler) {
										viewItems[i].onDependentHandler();
										viewItems[i].render();
										if (viewItems[i].setValueDefault){
											viewItems[i].setValueDefault();
										} 
									}
								}
							}
						}
					}
				}*/
				this.clicked = false;				
			}
			return this;
		},
		createDependencies : function () {
			var i, 
			j, 
			item, 
			dependents, 
			viewItems;
			dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
			viewItems = this.parent.items.asArray();			
			if (dependents.length > 0) {
				for (i = 0; i < viewItems.length; i+=1) {
					for (j = 0; j < dependents.length; j+=1) {
						item = viewItems[i].model.get("name");
						if(dependents[j] === item) {
							if (viewItems[i].model.setDependencies) {
								viewItems[i].model.setDependencies(this);
							}
						}
					}
				}
			}
			return this;
		},		
		continueDependentFields: function () {
			var newValue, 
			auxValue;
			this.previousValue = this.model.get("value");
			this.clicked = true;
			auxValue = $(this.el).find(":selected").val();
			newValue = (auxValue === undefined)? "" : auxValue;			
			this.model.set("value", newValue);
			//this.onChange(event);
			this.changeValuesFieldsRelated();
			
			return this;
		},
		onDependentHandler: function (target , datavalue) {
			var i, localOpt, remoteOptions;
			this.jsonData = this.generateDataDependenField2();
			remoteOptions = this.executeQuery();
			this.mergeOptions(remoteOptions);
			if(this.firstLoad){
				this.render();
			}
			return this;
		},							
        validate: function(){
        	var drpValue;
        	if(!this.model.get("disabled")) {
        		drpValue = (this.model.get("options").length > 0)? this.$el.find("select").val() : "";
	            this.model.set({value: drpValue}, {validate: true});
	            if (this.validator) {
	                this.validator.$el.remove();
	                this.$el.removeClass('has-error');
	            }
	            if(!this.model.isValid()){
	                this.validator = new PMDynaform.view.Validator({
	                    model: this.model.get("validator"),
	                    domain: false
	                });  
	                this.$el.find("select").parent().append(this.validator.el);
	                this.applyStyleError();
	            }
        	}        	
            return this;
        },
        updateValueControl: function () {
            var i,
            options = this.$el.find("select").find("option");
            loop:
            for (i=0; i<options.length; i+=1) {
            	if (options[i].selected) {
            		this.model.set("value", options[i].value);
            		break loop;
            	}
            }
            return this;    
        },
        on: function (e, fn) {
        	var that = this, 
        	control = this.$el.find("select");
        	if (control) {
        		control.on(e, function(event){
	        		fn(event, that);
	        		event.stopPropagation();
	        	});
        	}
        	return this;
        },
        getHTMLControl: function () {
            return this.$el.find("select");
        },
		render: function() {
			var that = this,
			dependenciesField = this.model.get("dependenciesField"),
			hidden,
			name;
			this.createDependencies();
			this.$el.html(this.template(this.model.toJSON()));
			if (this.model.get("group") === "grid") {
				hidden = this.$el.find("input[type = 'hidden']")[0];
				name = this.model.get("name");
				name = name.substring(0,name.length-1).concat("_label]");
				hidden.name = hidden.id = "form" + name;
			}
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			if (this.firstLoad){
				if (this.model.get("group")=== "form") {
					this.getDependeciesField();
				}else{
					this.getDependeciesFieldGrid();
				}
				if (this.dependentFields.length ){
					if (this.model.get("group") == "grid") {
						if (this.model.get("options").length){
							if (this.model.get("data") && this.model.get("data")["value"] && this.model.get("data")["value"].trim().length){
								this.model.set(this.model.get("options").concat([this.model.get("data")]));
							}
						}else{
							if (this.model.get("data") && this.model.get("data")["value"] && this.model.get("data")["value"].trim().length) {
								this.model.set("options",this.model.get("localOptions").concat([this.model.get("data")]));
							}
						}
					}else{
						this.model.set("options",this.model.get("localOptions").concat([this.model.get("data")]));
					}
				} else {
					this.model.set("options",
						this.model.get("localOptions"));
				}
			}
			this.setValueToDomain();
			this.$el.find("select").mousedown(
				function (event) {
					var data = {}, remoteOptions, options, option, i, val;
					that.dependentFields = [];
					that.dependentFieldsData = [];
					if (that.model.get("group") === "grid"){
						that.getDependeciesFieldGrid();
					}else{
						that.getDependeciesField();
					}
					if (that.dependentFields.length){
						if (that.firstLoad){
							if (!that.dirty){
								val = this.value;
								$(this).empty();
								if (that.model.get("group") === "grid"){
									that.jsonData = that.generateDataDependenField2();
								}else{
									that.jsonData = that.generateDataDependenField();
								}
								remoteOptions = that.executeQuery(true);
								options = that.mergeOptions(remoteOptions);
								for ( i = 0 ; i < options.length ; i+=1) {
									option = $("<option value=" + options[i].value + 
										">" + options[i].label+"</option>")[0];
									this.appendChild(option);
								}
								if (val !== ""){
									this.value = val;
								}
								that.dirty = true;
							}
							that.firstLoad = false;
						}
					} else {
						that.firstLoad = false;
					}
				}
			);
			if (this.model.get("name").trim().length === 0){
				this.$el.find("select").attr("name","");
				this.$el.find("input[type='hidden']").attr("name","");
			}
			return this;
		},
		afterRender : function () {
			this.continueDependentFields();
			return this;
		},
		executeQuery : function (clicked) {
			var restClient, key, resp, prj, endpoint, url, data;
			data = this.jsonData || {};	

			if (this.model.get("group") === "grid"){
 				data["field_id"] = this.model.get("columnName");
			}else{
				data["field_id"] = this.model.get("id");
			}
			if ( this.model.get("form") ) {
				if (this.model.get("form").model.get("form")){
					data["dyn_uid"] = this.model.get("form").model.get("form").model.get("id");				
				}else{
					data["dyn_uid"] = this.model.get("form").model.get("id");
				} 
			}
			prj = this.model.get("project");
			if (this.model.get("group") !== "grid") {
				endpoint = this.model.getEndpointVariable({
				                type: "executeQuery",
				                keys: {
				                    "{var_name}": this.model.get("var_name") || ""
				                }
				            });
				url = prj.getFullURL(endpoint);
			} else {
				endpoint = this.model.getEndpointVariable({
				                type: "executeQuery",
				                keys: {
				                    "{var_name}": this.model.get("var_name") || ""
				                }
				            });
				url = prj.getFullURL(endpoint);
			}	
			resp = [];
			restClient = new PMDynaform.core.Proxy ({
			    url: url,
			    method: "POST",
			    data: data,
			    keys: prj.token,
			    successCallback: function (xhr, response) {
			    	resp = response;
			    }
			});
			return resp;
		},
		mergeOptions : function (remoteOptions){
			var k, remoteOpt = [], localOpt = [], options;
			for ( k = 0; k < remoteOptions.length; k+=1) {
				remoteOpt.push({
					value : remoteOptions[k].value,
					label : remoteOptions[k].text
				});
			}
			localOpt = this.model.get("localOptions");
			this.model.set("remoteOptions", remoteOpt);
			options = localOpt.concat(remoteOpt);
			if (options.length){
				if (this.model.get("group") == "grid"){
					this.model.attributes.options = options;
					this.render();
				}else{
					this.model.set("value",options[0].value);
				}
				if (!this.dirty){
					this.model.set("data",{
						value : options[0].value,
						label : options[0].label 
					});
					if (this.model.get("validator")){
						this.model.get("validator").set("valid",true);
					}
				}else{
					this.model.set("data",{
						value : "",
						label : "" 
					});
				}
			}else{
				this.model.set("value","");
			}
			this.model.set("options", options);
			//this.render();
			return options;
		},
		generateDataDependenField : function () {
			var data, dependenciesField, name, value, i;
			dependenciesField = this.model.get("dependenciesField");
			data = {};
			for ( i = 0 ; i  < dependenciesField.length ; i+=1 ) {
				if (this.model.get("group") === "grid") {
					name = dependenciesField[i].model.get("columnName");
				}else{
					name = dependenciesField[i].model.get("name");
				}
				if ( dependenciesField[i].model.get("type") === "text"){
					value = dependenciesField[i].model.get("keyValue");
				}else{
					value = dependenciesField[i].model.get("value");
				}
				data[name] = value;
			}
			return data;
		},
		getDependeciesField : function () {
			var i, items, nameField, j, sql;
			nameField = this.model.get("name");
			if (this.parent && this.parent.model.get("items").length) {
				items  = this.parent.model.get("items");
				for ( i = 0 ; i < items.length ; i+=1 ) {
					if (items[i]){
						for ( j = 0 ; j < items[i].length ; j+=1 ){
							if (items[i][j].name && nameField !== items[i][j].name) {
								sql = this.model.get("sql");
								if (sql &&
									(
										sql.indexOf("@#"+items[i][j].name) > -1 ||
										sql.indexOf("@@"+items[i][j].name) > -1 ||
										sql.indexOf("@%"+items[i][j].name) > -1 ||
										sql.indexOf("@="+items[i][j].name) > -1 ||
										sql.indexOf("@?"+items[i][j].name) > -1 ||
										sql.indexOf("@$"+items[i][j].name) > -1 
									)
								) {
									if(this.dependentFields.indexOf(items[i][j].name) === -1) {
										this.dependentFields.push(items[i][j].name);
										this.dependentFieldsData.push(items[i][j]);
									}
								}
							}
						}
					}
				}
			}
		},
		getDependeciesFieldGrid : function () {
			var i, items, nameField, j, sql;
			nameField = this.model.get("columnName");
			if (this.parent && this.parent.items.asArray().length) {
				items  = this.parent.items.asArray()
				for ( i = 0 ; i < items.length ; i+=1 ) {
					if (items[i]){
						if (items[i].model.get("name") && nameField !== items[i].model.get("name")) {
							sql = this.model.get("sql");
							if (sql &&
								(
									sql.indexOf("@#" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@@" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@%" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@=" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@?" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@$" + items[i].model.get("columnName")) > -1 
								)
							) {
								if(this.dependentFields.indexOf(items[i].model.get("columnName")) === -1) {
									this.dependentFields.push(items[i].model.get("columnName"));
									this.dependentFieldsData.push(items[i]);
								}
							}
						}
					}
				}
			}
		},		
		generateDataDependenField2 : function () {
			var i, 
			j, 
			item, 
			dependents, 
			viewItems,
			label,
			control,
			hidden,
			sql, nameColumm, nameField, fieldDependentName, data = {};

			viewItems = this.parent.items.asArray();
			//find dependent fields
			if (this.model.get("group") === "grid") {
				nameField = this.model.get("columnName");
				for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
						if (this.dependentFields.indexOf( viewItems[i].model.get("columnName")) !== -1 ) {
							if ( viewItems[i].model.get("columnName") !== nameField){
								data[viewItems[i].model.get("columnName")] =  viewItems[i].model.get("value");
							}
						}			
				}
			} else {
				if (this.model.get("variable") && this.model.get("variable").trim().length){
					nameField = this.model.get("variable");	
				}else{
					nameField = this.model.get("id");
				}
				for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
					if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
						fieldDependentName = viewItems[i].model.get("id");
					}
					if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
						fieldDependentName = viewItems[i].model.get("name");
					}
					if (fieldDependentName !== nameField) {
						if (this.dependentFields.indexOf(fieldDependentName) !== -1 ) {
							data[fieldDependentName] =  viewItems[i].model.get("value");
						}
					}					
				}
			}
			return data;
		},
		generateDataDependenField2 : function () {
			var i, 
			j, 
			item, 
			dependents, 
			viewItems,
			label,
			control,
			hidden,
			sql, nameColumm, nameField, fieldDependentName, data = {};

			viewItems = this.parent.items.asArray();
			//find dependent fields
			if (this.model.get("group") === "grid") {
				nameField = this.model.get("columnName");
				for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
					if (this.dependentFields.indexOf( viewItems[i].model.get("columnName")) !== -1 ) {
						if ( viewItems[i].model.get("columnName") !== nameField){
                        	if (viewItems[i].model.get("type") === "text"){
                            	data[viewItems[i].model.get("columnName")] =  viewItems[i].fiendValueDependenField(viewItems[i].model.get("value"));
                        	} else {
                        		data[viewItems[i].model.get("columnName")] =  viewItems[i].model.get("value");
                        	}
						}
					}			
				}
			} else {
				if (this.model.get("variable") && this.model.get("variable").trim().length){
					nameField = this.model.get("variable");	
				}else{
					nameField = this.model.get("id");
				}
				for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
					if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
						fieldDependentName = viewItems[i].model.get("id");
					}
					if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
						fieldDependentName = viewItems[i].model.get("name");
					}
					if (fieldDependentName !== nameField) {
						if (this.dependentFields.indexOf(fieldDependentName) !== -1 ) {
							if (viewItems[i].model.get("type") === "text") {
								data[fieldDependentName] = viewItems[i].fiendValueDependenField(viewItems[i].model.get("value"));
							}else{
								data[fieldDependentName] = viewItems[i].model.get("value");
							}
						}
					}					
				}
			}
			return data;
		}
	});
	PMDynaform.extendNamespace("PMDynaform.view.Dropdown",DropDownView);
}());
