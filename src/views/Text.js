(function () {
var TextView = PMDynaform.view.Field.extend({
		template: _.template($("#tpl-text").html()),
		validator: null,
		keyPressed: false,
		fieldValid:[],
		previousValue : null,
		firstLoad : true,
		formulaFieldsAssociated: [],
		jsonData : {},
        dependentFields : [],
        dependentFieldsData : [],		
		events: {
			"blur input": "validate",
			//"keyup input": "validate",
			"keydown input": "refreshBinding",
		},
		onChangeCallback: function (){},
		setOnChange : function (fn) {
			if (typeof fn === "function") {
				this.onChangeCallback = fn;
			}
			return this;
		},
		setOnChangeCallbackOperation: function (fn) {
			if (typeof fn === "function") {
				this.onChangeCallbackOperation = fn;
			}
			return this;
		},
		initialize: function () {
			var that = this;
			this.formulaFieldsAssociated = [];
			this.model.on("change", this.checkBinding, this);
		},
		on: function (e, fn) {
			var that = this, 
			control,
			localEvents = {
				"changeValues": "setOnChangeCallbackOperation"
			};
			if (localEvents[e]) {
				this[localEvents[e]](fn);
			} else {
				control = this.$el.find("input");
				if (control) {
					control.on(e, function(event){
						fn(event, that);
						event.stopPropagation();
					});
				} else {
					throw new Error ("Is not possible find the HTMLElement associated to field");
				} 
			}
			return this;
		},
		checkBinding: function (event) {
			var form = this.model.get("form"), keyValue;
			if (this.model.get("operation")){
				this.onChangeCallbackOperation(this);
			}
			if ( typeof this.onChangeCallback === 'function' ) {
				this.onChangeCallback(this.getValue(), this.previousValue);
			}
			if ( form && form.onChangeCallback ) {
				form.onChangeCallback(this.model.get("name"), this.model.get("value"), this.previousValue);
			}
			//If the key is not pressed, executes the render method
			//this.onChangeHandler();
			if (!this.keyPressed) {
				this.render();
				this.onChange(event);
			} else {
				keyValue = this.findKeyValue(this.model.get("value")) || "";
				this.model.set("keyValue",keyValue);
				this.onChange(event);
			}
		},
		findKeyValue : function (value){
			var i, options = this.model.get("options");
			for ( i = 0 ; i < options.length ; i+=1 ) {
				if ( options[i]["label"] === value ) {
					return options[i]["value"];
				}
			}
			return null;
		},
		refreshBinding: function (event) {
			//Validation for the Submit event
			if (event.which === 13) {
				event.preventDefault();
				event.stopPropagation();
			}
			this.keyPressed = true;
			return this;
		},
		onChange : function () {
            var i, 
            j, 
            item, 
            dependents, 
            viewItems, 
            valueSelected,
            hidden,
			nameField, fieldDependentName, sql;
            this.previousValue = this.model.get("value");
            if ( !this.firstLoad ) {
                hidden = this.$el.find("input[type='hidden']");
                if (hidden.length && this.model.get("value")){
                    valueSelected = this.model.get("value");
                    hidden.val(valueSelected||"");
                }

				viewItems = this.parent.items.asArray();
				if (this.model.get("group") === "grid") {
					nameField = this.model.get("columnName");
					for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
						if (nameField !== viewItems[i].model.get("columnName")) {
							sql = viewItems[i].model.get("sql");
							if (sql &&
								(
									sql.indexOf("@#"+nameField) > -1 ||
									sql.indexOf("@@"+nameField) > -1 ||
									sql.indexOf("@="+nameField) > -1 ||
									sql.indexOf("@%"+nameField) > -1 ||
									sql.indexOf("@$"+nameField) > -1 ||
									sql.indexOf("@?"+nameField) > -1 
								)
							) {
								if (event) {
									if (viewItems[i].onDependentHandler){
										//this.jsonData[nameField] = 
										viewItems[i].onDependentHandler();
									}
								}
							}
						}					
					}
				} else {
					if (this.model.get("variable") && this.model.get("variable").trim().length){
						nameField = this.model.get("variable");	
					}else{
						nameField = this.model.get("id");
					}
					for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
						if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
							fieldDependentName = viewItems[i].model.get("id");
						}
						if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
							fieldDependentName = viewItems[i].model.get("name");
						}
						if (fieldDependentName !== nameField) {
							sql = viewItems[i].model.get("sql");
							if (sql && 
								(
									sql.indexOf("@#"+nameField) > -1 ||
									sql.indexOf("@%"+nameField) > -1 ||
									sql.indexOf("@@"+nameField) > -1 ||
									sql.indexOf("@?"+nameField) > -1 ||
									sql.indexOf("@$"+nameField) > -1 ||
									sql.indexOf("@="+nameField) > -1 )
							){
								if (event) {
									if (viewItems[i].onDependentHandler){
										viewItems[i].onDependentHandler();
									}
								}
							}
						}					
					}
				}
                this.clicked = false;
            }
            return this;
		},
        createDependencies : function () {
            var i, 
            j, 
            item, 
            dependents, 
            viewItems;
            dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
            viewItems = this.parent.items.asArray();            
            if (dependents.length > 0) {
                for (i = 0; i < viewItems.length; i+=1) {
                    for (j = 0; j < dependents.length; j+=1) {
                        item = viewItems[i].model.get("name");
                        if (dependents[j] === item) {
                            if (viewItems[i].model.setDependencies) {
                                viewItems[i].model.setDependencies(this);
                            }
                        }
                    }
                }
            }
            return this;
        },
        continueDependentFields: function (e) {
            var newValue,
            content;
            this.model.set("clickedControl",true);
            this.clicked = true;
            this.keyPressed = false;
            content = $(e.currentTarget).text();
            //newValue = $(this.el).find(":input").val();          
            $(this.el).find(":input[type='suggest']").val(content);
            this.model.set("value", $(e.currentTarget).find("span").data().value);
            this.containerList.remove();
            this.stackRow = 0;
            this.clicked = false; 
            return this;
        },
        onDependentHandler: function (target , datavalue) {
            var i, localOpt, remoteOptions;
            this.jsonData = this.generateDataDependenField2();
			//this.jsonData[target] = datavalue;
            remoteOptions = this.executeQuery();
            this.mergeOptions(remoteOptions);
            this.firstLoad = false;
            return this;
        },
        executeQuery : function (data){
            var restClient, resp, prj, endpoint, url , data = this.jsonData;
			if (this.model.get("group") === "grid"){
 				data["field_id"] = this.model.get("columnName");
			}else{
				data["field_id"] = this.model.get("id");
			}
			if ( this.model.get("form") ) {
				if (this.model.get("form").model.get("form")){
					data["dyn_uid"] = this.model.get("form").model.get("form").model.get("id");				
				}else{
					data["dyn_uid"] = this.model.get("form").model.get("id");
				} 
			}
            prj = this.model.get("project");
            endpoint = this.model.getEndpointVariable({
                            type: "executeQuery",
                            keys: {
                                "{var_name}": this.model.get("var_name") || ""
                            }
                        });
            url = prj.getFullURL(endpoint);         
            resp = [];
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: "POST",
                data: data,
                keys: prj.token,
                successCallback: function (xhr, response) {
                    resp = response;
                }
            });
            return resp;
        },
        mergeOptions : function (remoteOptions, click){
            var k, remoteOpt = [], localOpt = [], options;
            for ( k = 0; k < remoteOptions.length; k+=1) {
                remoteOpt.push({
                    value : remoteOptions[k].value,
                    label : remoteOptions[k].text
                });
            }
            localOpt = this.model.get("localOptions");
            this.model.set("remoteOptions", remoteOpt);
            options = localOpt.concat(remoteOpt);
            this.model.attributes.options = options;
            if (!click){
	            if (options.length){
	            	this.model.set({
	            		"keyValue" : options[0].value,
	            		"value" : options[0].label
	            	});
	            }else{
					this.model.set({	
	            		"keyValue" : "",
	            		"value" : ""
	            	});
				}
            }

            return options;
        },
        fiendValueDependenField : function (dataLabel) {
			var i;
            for ( i = 0 ; i  < this.model.get("options").length ; i+=1 ) {
				if (this.model.get("options")[i]["label"] === dataLabel) {
					return this.model.get("options")[i]["value"];
				}
            }
			return null;
        },
		validate: function (event,b,c) {
			var $inputField, 
			fieldsAssoc,
			originalValue,
			fieldsObj,
			maskLength,
			inputValue,
			i;
			this.keyPressed = true;
			if (event) {
					if ((event.which === 9) && (event.which !==0)) { //tab key
						//this.keyPressed = false;
					}
			}
			if (!this.model.get("disabled")) {
				$inputField = this.$el.find("input");
				if (this.model.get("mask")) {
					inputValue = $inputField.val();
					originalValue = $inputField.cleanVal();
					maskLength = this.model.get("mask").length;
					if (inputValue.length > maskLength ) {
						$inputField.val(inputValue.substring(0,maskLength));
					}
				} else {
					originalValue = $inputField.val();
				}
				//Before save an object
				this.onTextTransform(originalValue);
				this.model.set({value: originalValue}, {validate: true});
				if (this.validator) {
					this.validator.$el.remove();
					this.$el.removeClass('has-error has-feedback');
				}
				if(!this.model.isValid()){
					this.validator = new PMDynaform.view.Validator({
						model: this.model.get("validator")
					});
					$inputField.parent().append(this.validator.el);
					this.applyStyleError();
				}
			}
			this.onFieldAssociatedHandler();
			this.changeValuesFieldsRelated();
			this.keyPressed = false;
			this.previousValue = this.model.get("value");
			return this;
		},
		updateValueControl: function () {
			var inputVal = this.$el.find("input").val();
			this.model.set("value", inputVal);
			return this; 
		},
		onFieldAssociatedHandler: function() {
			var i,
			fieldsAssoc = this.formulaFieldsAssociated;
			if (fieldsAssoc.length > 0) {
				for (i=0; i<fieldsAssoc.length; i+=1) {
					if (fieldsAssoc[i].model.get("formulator") instanceof PMDynaform.core.Formula) {
						this.model.addFormulaTokenAssociated(fieldsAssoc[i].model.get("formulator"));
						this.model.updateFormulaValueAssociated(fieldsAssoc[i]);
					}
				}
			}
			return this;
		},
		onTextTransform: function (val) {
			var transformed,
			transform = this.model.get("textTransform"),
			availables = {
				upper: function () {
					return val.toUpperCase();
				},
				lower: function () {
					return val.toLowerCase();
				},
				none: function() {
					return val;
				},
				capitalizePhrase : function (){
					return val.capitalize();
				},
				titleCase : function (){
					return val.charAt(0).toUpperCase() + val.slice(1);
				}
			};
			if (transform) {
				transformed = (availables[transform]) ? availables[transform]() : availables["none"]();
				this.$el.find("input").val(transformed);
			}
			return this;
		},
		onFormula: function() {
			var fieldsList = this.parent.items,
			that = this,
			allFields,
			allFieldsView,
			index,
			formulaField,
			idFields = {},
			fieldFormula,
			fieldValid,
			resultField,
			fieldAdded = [],
			fieldSelected,
			obj,
			i;
			//All Fields from the FORM
			allFieldsView = (fieldsList instanceof Array)? fieldsList: fieldsList.asArray();

			for (index = 0 ; index < allFieldsView.length ; index+=1 ) {
				if (allFieldsView[index] instanceof PMDynaform.view.Text) {
					idFields[allFieldsView[index].model.get("id")] = allFieldsView[index];
				} 
			}

			fieldSelected = {};
			//Fields from the Formula PROPERTY
			formulaField = this.model.get("formula");
			fieldFormula = formulaField.split(/[\-(,|+*/\)]+/);
			for (i=0; i<fieldFormula.length; i+=1) {
				fieldFormula[i] = fieldFormula[i].trim();
			}
			this.fieldValid = fieldFormula.filter(function existElement(element) {
				var result = false;
				if ((idFields[element] !== undefined) && ($.inArray(element, fieldAdded) === -1)) {
					fieldAdded.push(element);
					result = true
				}
				return result;
			});
			//Insert the Formula object to fields selected
			for (var obj = 0 ; obj < this.fieldValid.length ; obj+=1 ) {
				this.model.addFormulaFieldName(this.fieldValid[obj]);
				idFields[this.fieldValid[obj]].formulaFieldsAssociated.push(that);
			}
			return this;
		},
		getHTMLControl: function () {
			return this.$el.find("input");
		},
		render: function() {
			var hidden, name, that;
			this.createDependencies(), that = this;
			this.$el.html( this.template(this.model.toJSON()) );
			if (this.model.get("hint") !== "") {
				this.enableTooltip();
			}
			this.previousValue = this.model.get("value");
			if (this.model.get("group") === "grid") {
				hidden = this.$el.find("input[type = 'hidden']")[0];
				name = this.model.get("name");
				name = name.substring(0,name.length-1).concat("_label]");
				hidden.name = hidden.id = "form" + name;
				hidden.value = this.model.get("value"); 
			}

            if (this.model.get("group")=== "form") {
                this.getDependeciesField();
            }else{
                this.getDependeciesFieldGrid();
            }

			if (this.model.get("formula")) {
				this.onFormula();
			}

			this.$el.find("input[type='text']").focusin(function(event){
                var data = {}, remoteOptions, options, option, i, val, value;
                that.clicked = true;
                that.dependentFields = [];
                that.dependentFieldsData = [];
                if (that.model.get("group") === "grid"){
                    that.getDependeciesFieldGrid();
                }else{
                    that.getDependeciesField();
                }
                if (that.dependentFields.length){
                    if (that.firstLoad){
                        if (!that.dirty){
                            $(this).empty();
							if (that.model.get("group") === "grid"){
                                that.jsonData = that.generateDataDependenField2();
                            }else{
                                that.jsonData = that.generateDataDependenField2();
                            }
                            remoteOptions = that.executeQuery(data);
                            that.mergeOptions(remoteOptions, true);
                            that.dirty = true;
                        }
                        that.firstLoad = false;
                    }
                } else {
                    that.firstLoad = false;
                }
            });

			if (this.model.get("name").trim().length === 0){
				this.$el.find("input[type='text']").attr("name","");
				this.$el.find("input[type='hidden']").attr("name","");
			}
			return this;
		},
        getDependeciesField : function () {
            var i, items, nameField, j, sql;
            nameField = this.model.get("name");
            this.dependenciesField = [];
            this.dependentFieldsData = [];
            if (this.parent && this.parent.model.get("items").length) {
                items  = this.parent.model.get("items");
                for ( i = 0 ; i < items.length ; i+=1 ) {
                    if (items[i]){
                        for ( j = 0 ; j < items[i].length ; j+=1 ){
                            if (items[i][j].name && nameField !== items[i][j].name) {
                                sql = this.model.get("sql");
                                if (sql && (
									sql.indexOf("@#"+items[i][j].name) > -1 ||
									sql.indexOf("@@"+items[i][j].name) > -1 ||
									sql.indexOf("@%"+items[i][j].name) > -1 ||
									sql.indexOf("@="+items[i][j].name) > -1 ||
									sql.indexOf("@?"+items[i][j].name) > -1 ||
									sql.indexOf("@$"+items[i][j].name) > -1 )
								) {
                                    if(this.dependentFields.indexOf(items[i][j].name) === -1) {
                                        this.dependentFields.push(items[i][j].name);
                                        this.dependentFieldsData.push(items[i][j]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        getDependeciesFieldGrid : function () {
            var i, items, nameField, j, sql;
            nameField = this.model.get("columnName");
            if (this.parent && this.parent.items.asArray().length) {
                items  = this.parent.items.asArray()
                for ( i = 0 ; i < items.length ; i+=1 ) {
                    if (items[i]){
                        if (items[i].model.get("name") && nameField !== items[i].model.get("name")) {
                            sql = this.model.get("sql");
                            if (sql && 
								(
									sql.indexOf("@#" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@@" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@%" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@=" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@?" + items[i].model.get("columnName")) > -1 ||
									sql.indexOf("@$" + items[i].model.get("columnName")) > -1 
								)
							) {
                                if(this.dependentFields.indexOf(items[i].model.get("columnName")) === -1) {
                                    this.dependentFields.push(items[i].model.get("columnName"));
                                    this.dependentFieldsData.push(items[i]);
                                }
                            }
                        }
                    }
                }
            }
        },
        generateDataDependenField2 : function () {
            var i, 
            j, 
            item, 
            dependents, 
            viewItems,
            label,
            control,
            hidden,
            sql, nameColumm, nameField, fieldDependentName, data = {};

            viewItems = this.parent.items.asArray();
            //find dependent fields
            if (this.model.get("group") === "grid") {
                nameField = this.model.get("columnName");
                for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
                    if (this.dependentFields.indexOf( viewItems[i].model.get("columnName")) !== -1 ) {
                        if ( viewItems[i].model.get("columnName") !== nameField){
                        	if (viewItems[i].model.get("type") === "text"){
                            	data[viewItems[i].model.get("columnName")] =  viewItems[i].fiendValueDependenField(viewItems[i].model.get("value"));
                        	} else {
                        		data[viewItems[i].model.get("columnName")] =  viewItems[i].model.get("value");
                        	}
                        }
                    }
                }
            } else {
                if (this.model.get("variable") && this.model.get("variable").trim().length){
                    nameField = this.model.get("variable"); 
                }else{
                    nameField = this.model.get("id");
                }
                for ( i = 0  ; i < viewItems.length ; i+=1 ) { 
                    if (viewItems[i].model.get("id") && viewItems[i].model.get("id").trim().length ) {
                        fieldDependentName = viewItems[i].model.get("id");
                    }
                    if ( viewItems[i].model.get("name") && viewItems[i].model.get("name").trim().length ) {
                        fieldDependentName = viewItems[i].model.get("name");
                    }
                    if (fieldDependentName !== nameField) {
                        if (this.dependentFields.indexOf(fieldDependentName) !== -1 ) {
							if (viewItems[i].model.get("type") === "text") {
								data[fieldDependentName] = viewItems[i].fiendValueDependenField(viewItems[i].model.get("value"));
							}else{
								data[fieldDependentName] = viewItems[i].model.get("value");
							}
                        }
                    }                   
                }
            }
            return data;
        }
	});
	PMDynaform.extendNamespace("PMDynaform.view.Text",TextView);
}());