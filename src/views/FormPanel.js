(function(){
    var FormPanel = Backbone.View.extend({
        tagName: "form",
        content : null,    
        template: null,
        items: new PMDynaform.util.ArrayList(),
        views:[],
        templateRow: _.template($('#tpl-row').html()),
        colSpanLabel: 3,
        colSpanControl: 9,
        project: null,
        preTargetControl : null,
        events: {
            'submit': 'onSubmit'
        },
        onChange: function (){},
        /*
        requireVariableByField: [
            "text",
            "textarea",
            "checkbox",
            "radio",    
            "dropdown",
            "datetime",
            "suggest",
            "hidden",
            "label"
        ],*/
        requireVariableByField: [],
        checkBinding: function () {
            this.onChangeCallback(this.model.get("name"), this.previusValue, this.model.get("value"));
            //If the key is not pressed, executes the render method
            if (!this.keyPressed) {
                this.render();
            }
        },
        setOnChange : function (handler) {
            if (typeof handler === "function") {
                this.onChangeCallback = handler;
            }
            return this;
        },
        onChangeHandler : function() {
            var that = this;
            return function(field, newValue, previousValue) {
                if ( typeof that.onChange === 'function' ) {
                    that.onChange(field, newValue, previousValue);
                }
            };
        },        
        initialize: function(options) {
            var defaults = {
                factory : {
                    products: {
                        "text": {
                            model: PMDynaform.model.Text,
                            view: PMDynaform.view.Text
                        },
                        "textarea": {
                            model: PMDynaform.model.TextArea,
                            view: PMDynaform.view.TextArea
                        },
                        "checkbox": {
                            model: PMDynaform.model.Checkbox,
                            view: PMDynaform.view.Checkbox
                        },
                        "radio": {
                            model: PMDynaform.model.Radio,
                            view: PMDynaform.view.Radio
                        },
                        "dropdown": {
                            model: PMDynaform.model.Dropdown,
                            view: PMDynaform.view.Dropdown
                        },
                        "button": {
                            model: PMDynaform.model.Button,
                            view: PMDynaform.view.Button
                        },
                        "submit": {
                            model: PMDynaform.model.Submit,
                            view: PMDynaform.view.Submit
                        },
                        "datetime": {
                            model: PMDynaform.model.Datetime,
                            view: PMDynaform.view.Datetime
                        },
                        "fieldset": {
                            model: PMDynaform.model.Fieldset,
                            view: PMDynaform.view.Fieldset
                        },                    
                        "suggest": {
                            model: PMDynaform.model.Suggest,
                            view: PMDynaform.view.Suggest
                        },                                        
                        "link": {
                            model: PMDynaform.model.Link,
                            view: PMDynaform.view.Link
                        },                                        
                        "hidden": {
                            model: PMDynaform.model.Hidden,
                            view: PMDynaform.view.Hidden
                        },
                        "title": {
                            model: PMDynaform.model.Title,
                            view: PMDynaform.view.Title
                        },
                        "subtitle": {
                            model: PMDynaform.model.Title,
                            view: PMDynaform.view.Title
                        },
                        "label": {
                            model: PMDynaform.model.Label,
                            view: PMDynaform.view.Label
                        },
                        "empty": {
                            model: PMDynaform.model.Empty,
                            view: PMDynaform.view.Empty
                        },
                        "file": {
                            model: PMDynaform.model.File,
                            view: PMDynaform.view.File
                        },
                        "image": {
                            model: PMDynaform.model.Image,
                            view: PMDynaform.view.Image
                        },
                        "geomap": {
                            model: PMDynaform.model.GeoMap,
                            view: PMDynaform.view.GeoMap
                        },
                        "grid": {
                            model: PMDynaform.model.GridPanel,
                            view: PMDynaform.view.GridPanel
                        },
                        "form": {
                            model: PMDynaform.model.SubForm,
                            view: PMDynaform.view.SubForm
                        },
                        "annotation": {
                            model: PMDynaform.model.Annotation,
                            view: PMDynaform.view.Annotation  
                        },
                        "location" : {
                            model: PMDynaform.model.Geomap_mobile,
                            view: PMDynaform.view.Geomap_mobile  
                        },
                        "scannercode" : {
                            model: PMDynaform.model.Qrcode_mobile,
                            view: PMDynaform.view.Qrcode_mobile  
                        },
                        "signature": {
                            model: PMDynaform.model.Signature_mobile,
                            view: PMDynaform.view.Signature_mobile  
                        },
                        "imagemobile" : {
                            model: PMDynaform.model.Image_mobile,
                            view: PMDynaform.view.Image_mobile  
                        },
                        "audiomobile" : {
                            model: PMDynaform.model.Audio_mobile,
                            view: PMDynaform.view.Audio_mobile  
                        },
                        "videomobile" : {
                            model: PMDynaform.model.Video_mobile,
                            view: PMDynaform.view.Video_mobile
                        },
                        "panel" : {
                            model: PMDynaform.model.PanelField,
                            view: PMDynaform.view.PanelField
                        }
                    },
                    defaultProduct: "empty"
                }       
            };
            this.items = new PMDynaform.util.ArrayList();
            if(options.project) {
                this.project = options.project;
            }
            this.setFactory(defaults.factory);
            this.makeItems();
            //this.setFieldRelated();
        },
        setAction: function() {
            this.$el.attr("action", this.model.get("action"));

            return this;
        },
        setMethod: function() {
            this.$el.attr("method", this.model.get("method"));

            return this;
        },
        setFactory: function (factory) {
            this.factory = factory;
            return this;
        },
        getData: function() {
            return this.model.getData();
        },
        setData: function (data) {
            var i,
            j,
            cloneData = data,
            items = this.items.asArray();
            if (typeof data === "object") {
                for (i=0; i<items.length; i+=1) {
                    for (j in cloneData) {
                        if (items[i].model.attributes.variable) {
                            if (items[i].model.attributes.variable.var_name === j) {
                                items[i].model.set("value", cloneData[j]);
                            }
                        }
                    }
                    if (items[i] instanceof PMDynaform.view.SubForm) {
                        items[i].setData(data);
                    }

                    if (items[i] instanceof PMDynaform.view.GridPanel) {
                        items[i].setData(data);
                        //Nothing
                    }
                }
            } else {
                //console.log("Error, The 'data' parameter is not valid. Must be an array.");
            }
            
            return this;
        },
        setData2 : function(data){
            var i, cloneData, items, j, k, type, options, valueViewMode, mode,value, singleControl, valor, richi,option;
            singleControl = ["text","textarea","datetime","radio","link", "dropdown"]
            items = this.items.asArray();
            for ( i = 0 ; i < items.length ; i+=1 ) {
                if (data[items[i].model.get("name")] !== undefined) {
                    mode = items[i].model.get("mode");
                    type = items[i].model.get("type");
                    if (mode === "edit" || mode === "disabled") {
                        if (singleControl.indexOf(type) !== -1 ) {
                            items[i].model.set("value", data[items[i].model.get("name")]);
                            if ( items[i].clicked) {
                                items[i].render();
                            }
                        }
                        if (type === "suggest") {
                            for ( richi = 0 ; richi < items[i].model.get("localOptions").length ; richi +=1 ) {
                                option  = items[i].model.get("localOptions")[richi].value;
                                if (option === data[items[i].model.get("name")]){
                                    value = items[i].model.get("localOptions")[richi].label;
                                    break;
                                }
                            }
                            if (value && !value.length){
                                for ( richi = 0 ; richi < items[i].model.get("options").length ; richi +=1 ) {
                                    option  = items[i].model.get("options")[richi].value;
                                    if (option === data[items[i].model.get("name")]){
                                        value = items[i].model.get("options")[richi].label;
                                        break;
                                    }
                                }
                            }

                            $(items[i].el).find(":input").val(value);
                            items[i].model.attributes.value = data[items[i].model.get("name")];
                        }
                        if (type === "checkbox") {
                            options = items[i].model.get("options");
                            if ( items[i].model.get("dataType") === "boolean" ) {
                                if ( data[items[i].model.get("name")] === options[0].value ){
                                    options[1].selected = false;
                                    options[0].selected = true;
                                } else {
                                    delete options[0].selected;
                                    options[1].selected = true;
                                    options[0].selected = false;
                                }
                            } else {
                                for ( k = 0 ; k < options.length; k+=1 ) {
                                    delete options[k].selected;
                                    if (data[items[i].model.get("name")].indexOf(options[k].value) !== -1){
                                        options[k].selected = true;
                                    }
                                }                                
                            }
                            items[i].model.set("options", options);
                            //items[i].model.initControl();
                            items[i].render();
                            items[i].model.attributes.value = [data[items[i].model.get("name")]];
                        }
                        if (type === "grid") {
                            items[i].setData2(data[items[i].model.get("name")]);
                        }
                    }
                    if (mode === "view") {
                        if (items[i].model.get("originalType") === "checkbox"){
                            items[i].model.set("options", data[items[i].model.get("name")]);
                        }else if (items[i].model.get("originalType") === "grid" ){
                            items[i].setData2(data[items[i].model.get("name")]);
                        } else if (items[i].model.get("originalType") === "dropdown" ||
                                    items[i].model.get("originalType") === "suggest") {
                            value = [];
                            for ( richi = 0 ; richi < items[i].model.get("localOptions").length ; richi +=1 ) {
                                option  = items[i].model.get("localOptions")[richi].value;
                                if (option === data[items[i].model.get("name")]){
                                    value.push(items[i].model.get("localOptions")[richi].label);
                                    items[i].model.set("fullOptions", value);
                                    break;
                                }
                            }
                            if (!value.length){
                                for ( richi = 0 ; richi < items[i].model.get("options").length ; richi +=1 ) {
                                    option  = items[i].model.get("options")[richi].value;
                                    if (option === data[items[i].model.get("name")]){
                                        value.push(items[i].model.get("options")[richi].label);
                                        items[i].model.set("fullOptions", value);
                                        break;
                                    }
                                }
                            }
                        }else{
                            value = [];
                            value.push(data[items[i].model.get("name")]);
                            items[i].model.set("fullOptions", value);
                        }
                    }
                }
                if ( items[i].model.get("data") && items[i].$el.find("input[type='hidden']").length === 1) {
                    //console.log("\n");
                }
            }
        },
        validateVariableField: function (field) {
            var isOk = false;

            if ($.inArray(field.type, this.requireVariableByField) >= 0) {
                if (field.var_uid) {
                    isOk = true;
                }
            } else {
                isOk = "NOT";
            }

            return isOk;
        },
        makeItems: function() {
            var i,
            j,
            factory = this.factory, 
            product, 
            variableEnabled,
            productBuilt, 
            rowView,
            productModel,
            jsonFixed,
            fieldModel,
            fields,
            items;
            
            fields =  this.model.get("items");
            this.viewsBuilt = [];
            this.items.clear();

            for(i=0; i<fields.length; i+=1) {
                rowView = [];
                for(j=0; j<fields[i].length; j+=1) {
                    variableEnabled = this.validateVariableField(fields[i][j]);
                    if (fields[i][j] !== null && (variableEnabled === true || variableEnabled === "NOT") ) {
                        if (fields[i][j].type) {
                            if (fields[i][j].type === "checkbox" && fields[i][j].dataType === "boolean") {
                                if (fields[i][j].data){
                                    if (typeof fields[i][j].data["value"] === "boolean"){
                                        if(fields[i][j].data["value"]){
                                            fields[i][j].data["value"] = ["1"];
                                            fields[i][j].data["label"] = JSON.stringify(["1"]);
                                        }else{
                                            fields[i][j].data["value"] = ["0"];
                                            fields[i][j].data["label"] = JSON.stringify(["0"]);
                                        }
                                    }else{
                                        if (fields[i][j].data["value"] == 1){
                                            fields[i][j].data["value"] = ["1"];
                                            fields[i][j].data["label"] = JSON.stringify(["1"]);
                                        }else{
                                            fields[i][j].data["value"] = ["0"];
                                            fields[i][j].data["label"] = JSON.stringify(["0"]);
                                        }
                                    }
                                }
                            }
                            jsonFixed  = new PMDynaform.core.TransformJSON({
                                parentMode: this.model.get("mode"),
                                field: fields[i][j]
                            });
                            product =   factory.products[jsonFixed.getJSON().type.toLowerCase()] ? 
                                factory.products[jsonFixed.getJSON().type.toLowerCase()] : factory.products[factory.defaultProduct];
                        } else {
                            jsonFixed  = new PMDynaform.core.TransformJSON({
                                parentMode: this.model.get("mode"),
                                field: fields[i][j]
                            });
                            product = factory.products[factory.defaultProduct];
                        }
                        
                        //The number 12 is related to 12 columns from Bootstrap framework
                        fieldModel = {
                            colSpanLabel: this.createColspan(fields[i][j].colSpan, "label"),
                            colSpanControl: this.createColspan(fields[i][j].colSpan, "control"),
                            project: this.project,
                            parentMode: this.model.get("mode"),
                            namespace: this.model.get("namespace"),
                            variable: (variableEnabled !== "NOT")? this.getVariable(fields[i][j].var_uid) : null,
                            fieldsRelated: [],
                            name : fields[i][j].name,
                            options : fields[i][j].options,
                            form : this
                        };
                        if (fields[i][j].type === "form" || fields[i][j].type === "grid") {
                            fieldModel.variables = this.model.get("variables") || [];
                            fieldModel.data = this.model.get("data") || [];
                        }

                        $.extend(true, fieldModel, jsonFixed.getJSON());
                        
                        if ( fieldModel.type === "form" && fieldModel.mode === "parent") {
                            fieldModel.mode = this.model.get("mode");
                        }

                        productModel = new product.model(fieldModel);
                        productBuilt = new product.view({
                            model: productModel,
                            project:this.project,
                            parent: this
                        });

                        productBuilt.parent = this;
                        productBuilt.project = this.project;
                        this.project.addViewFields(productBuilt);
                        rowView.push(productBuilt);
                        this.items.insert(productBuilt);
                        productBuilt.model.set("view", productBuilt);
                    } else {
                        console.error ("The field must have the variable property and must to be an object: ", fields[i][j]);
                    }
                }
                if (rowView.length) {
                    this.viewsBuilt.push(rowView);
                }
            }
            //this.runningFormulator();
            return this;
        },
        createColspan : function  (colSpan, target) {
            var colspan;
            switch (parseInt(colSpan)) {
                case 12:
                    if (target === "label"){
                        colspan = 2;
                    } else { 
                        colspan = 10;
                    }
                break;
                case 11:
                    if (target === "label"){
                        colspan = 2;
                    } else {
                        colspan = 10;
                    }
                break;
                case 10:
                    if (target === "label"){
                        colspan = 2;
                    } else { 
                        colspan = 10;
                    }
                break;
                case 9:
                    if (target === "label"){
                        colspan = 2;
                    } else { 
                        colspan = 10;
                    }
                break;
                case 8:
                    if (target === "label"){
                        colspan = 2;
                    } else { 
                        colspan = 10;
                    }
                break;
                case 7:
                    if (target === "label"){
                        colspan = 2;
                    } else { 
                        colspan = 10;
                    }
                break;
                case 6:
                    if (target === "label"){
                        colspan = 4;
                    } else { 
                        colspan = 8;
                    }
                break;
                case 5:
                    if (target === "label"){
                        colspan = 5;
                    } else { 
                        colspan = 7;
                    }                
                break;
                case 4:
                    if (target === "label"){
                        colspan = 4;
                    } else { 
                        colspan = 8;
                    }                
                break;
                case 3:
                    if (target === "label"){
                        colspan = 5;
                    } else { 
                        colspan = 7;
                    }
                break;
                case 2:
                    if (target === "label"){
                        colspan = 5;
                    } else { 
                        colspan = 7;
                    }
                break;
                case 1:
                    if (target === "label"){
                        colspan = 4;
                    } else { 
                        colspan = 8;
                    }
                break;
            }
            return colspan;
        },
        runningFormulator: function () {
            var items, field, item, i,j,k, fieldsAsocied;
            items = this.viewsBuilt;
            for ( i = 0 ; i < items.length ; i+=1 ) {
                for ( j = 0 ; j < items[i].length ; j+=1 ) {
                    field = items[i][j];
                    if ( field.model.get("type") === "form" ) {
						if (field.runningFormulator){
							field.runningFormulator();
						}
                    }else{
                        if (field.model.get("formula") && field.model.get("formula").trim().length){
                            fieldsAsocied = items.filter(function(element){
                                if ( field.fieldValid.indexOf(element[0].model.get("id")) > -1) {
                                    element[0].onFieldAssociatedHandler()
                                    return element;
                                }
                            });
                        }
                    }
                }
            }
            return this;
        },
        setFieldRelated: function () {
            var i, 
            j,
            k,
            l,
            fieldA,
            fieldB,
            related,
            relatedA,
            relatedB,
            fieldsSubForm,
            relatingField,
            fields = this.items.asArray();

            for (i=0; i<fields.length; i+=1) {
                fieldA = fields[i].model.get("variable");
                if (fieldA) {
                    for (j=0; j<fields.length; j+=1) {
                        if (i !== j) {
                            fieldB = fields[j].model.get("variable");
                            if (fieldB) {
                                if (fieldA.var_uid === fieldB.var_uid) {
                                    related = fields[i].model.get("fieldsRelated");
                                    related.push(fields[j]);
                                    fields[i].model.set("fieldsRelated", related);
                                }
                            }
                        }
                    }
                }

                if (fields[i].model.get("type") === "form") {
                    fieldsSubForm = fields[i].getItems();
                    for (k=0; k<fields.length; k+=1) {
                        fieldA = fields[k].model.get("variable");
                        if (fieldA) {
                            for (l=0; l<fieldsSubForm.length; l+=1) {
                                fieldB = fieldsSubForm[l].model.get("variable");
                                if (fieldB) {
                                    if (fieldA.var_uid === fieldB.var_uid) {
                                        relatedA = fields[k].model.get("fieldsRelated");
                                        relatedA.push(fieldsSubForm[l]);
                                        fields[k].model.set("fieldsRelated", relatedA);

                                        relatedB = fieldsSubForm[l].model.get("fieldsRelated");
                                        relatedB.push(fields[k]);
                                        fieldsSubForm[l].model.set("fieldsRelated", relatedB);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return this;
        },
        getVariable: function (var_uid) {
            var i,
            varSelected,
            variables = this.model.attributes.variables;

            loop_variables:
            for (i=0; i<variables.length; i+=1) {
                if (variables[i] && variables[i].var_uid === var_uid) {
                    varSelected = variables[i];
                    break loop_variables;
                }
            }
            return varSelected;
        },
        getFields: function () {
            return (this.items.getSize() > 0)? this.items.asArray(): [];
        },
        beforeRender: function (){
            return this;
        },
        disableContextMenu: function() {
            this.$el.on("contextmenu", function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
            
            return this;
        },
        onSubmit: function(event) {
            var booResponse, i, restData, restClient, items;

            if (!this.isValid(event)) {
                booResponse =  false;
            } else {
                items = this.items.asArray();
                for (i=0; i<items.length; i+=1) {
                    if(items[i].applyStyleSuccess) {
                        items[i].applyStyleSuccess();
                    }
                }
                booResponse =  true;
            }
            if(this.project.submitRest){
                event.preventDefault();
                if(booResponse){
                    this.project.onSubmitForm();
                }
            }
            if (booResponse) {
                this.$el.find(".form-control").prop('disabled', false);
            }
            return booResponse;
        },
        isValid: function(event) {
            var i, formValid = true,
            itemsField = this.items.asArray();

            if (itemsField.length > 0) {
                for (i = 0; i < itemsField.length; i+=1) {
                    if(itemsField[i].validate) {
                        if (event){
                            itemsField[i].validate(event);
                            if (!itemsField[i].model.get("valid")) {
                                formValid = itemsField[i].model.get("valid");   
                            }
                        }else{
                            itemsField[i].validate();
                            formValid = itemsField[i].model.get("valid");
                            if (!formValid){
                                return false;
                            }   
                        }
                    }
                }
            }
			if (formValid){
				for (i = 0; i < itemsField.length; i+=1) {
                    if( ( itemsField[i].model.get("var_name") !== undefined) && (itemsField[i].model.get("var_name").trim().length === 0 )) {
						if (itemsField[i].model.get("type") === "radio") {
							itemsField[i].$el.find("input").attr("name","");
						}
                    }
                    console.log("adawdadadaw");
                }
			}
            return formValid;
        },
        render : function (){
            var i,j, $rowView;
            for(i=0; i<this.viewsBuilt.length; i+=1){
                $rowView = $(this.templateRow());
                for(j=0; j<this.viewsBuilt[i].length; j+=1){
                    /*if (this.viewsBuilt[i][j].model.attributes.type === "form") {
                        this.viewsBuilt[i][j].model.attributes.type = "subform";
                    }*/
                    $rowView.append(this.viewsBuilt[i][j].render().el);
                }                
                this.$el.append($rowView);
            }
            this.$el.attr("role","form");
            this.$el.addClass("form-horizontal pmdynaform-form");
			this.el.style.height = "99%";
            this.setAction();
            this.setMethod();
            this.$el.attr("id",this.model.get("id"));
            if (this.model.get("target")) {
                this.$el.attr("target", this.model.get("target"));
            }
            this.disableContextMenu();
          return this;
        },
        afterRender: function () {
            var i,
            j,
            items = this.items.asArray();;

            for (i=0; i<items.length; i+=1) {
                if (items[i].afterRender) {
                    items[i].afterRender();
                }
            }
            /*for(i=0; i<this.viewsBuilt.length; i+=1){
                for(j=0; j<this.viewsBuilt[i].length; j+=1){
                    if (this.viewsBuilt[i][j].afterRender) {
                        this.viewsBuilt[i][j].afterRender();
                    }
                }
            }*/

            if (this.model.attributes.data) {
                this.setData(this.model.get("data"));
            }
            
            
            return this;
        },

    });

    PMDynaform.extendNamespace("PMDynaform.view.FormPanel", FormPanel);
    
}());
