(function(){
	var CheckboxView = PMDynaform.view.Field.extend({
		item: null,	
		template: _.template( $("#tpl-checkbox").html()),
		previousValue : null,
		events: {
	        "click input": "onChange",
            "blur input": "validate",
            "keydown input": "preventEvents"
	    },
		onChangeCallback: function (){},
        setOnChange : function (fn) {
            if (typeof fn === "function") {
                this.onChangeCallback = fn;
            }
            return this;
        },
		initialize: function (){
			this.model.on("change", this.checkBinding, this);
		},
		checkBinding: function () {
			var form = this.model.get("form");
			if ( typeof this.onChangeCallback === 'function' ) {
                this.onChangeCallback(JSON.stringify(this.getValue()), JSON.stringify(this.previousValue));
            }
            if ( form && form.onChangeCallback) {
                form.onChangeCallback(this.model.get("name"), JSON.stringify(this.model.get("value")), JSON.stringify(this.previousValue));
            }
			this.render();
			//this.validate();
			return this;
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            
            return this;
        },
		render : function () {
			var hidden, name;
            this.$el.html( this.template(this.model.toJSON()));
            if (this.model.get("hint") !== "") {
                this.enableTooltip();
            }
            this.setValueToDomain();
            if (this.model.get("group") === "grid") {
                hidden = this.$el.find("input[type = 'hidden']")[0];
                name = this.model.get("name");
                name = name.substring(0,name.length-1).concat("_label]");
                hidden.name = hidden.id = "form" + name;
            }else{
                this.$el.find("input[type='hidden']")[0].name = "form[" + this.model.get("name")+"_label]";
            }
            this.setValueHideControl();
            this.previousValue = this.model.get("value");

			if (this.model.get("name").trim().length === 0){
				this.$el.find("input[type='checkbox']").attr("name","");
				this.$el.find("input[type='hidden']").attr("name","");
			}
			
            return this;
		},
		validate: function() {
			if (!this.model.get("disabled")) {
				this.model.set({},{validate: true});
				//this.model.get("validator").attributes.valid = true;
				//this.model.attributes.validate = true;
				if (this.validator) {
	                this.validator.$el.remove();
	                this.$el.removeClass('has-error has-feedback');
	            }
				if(!this.model.isValid()) {
	                this.validator = new PMDynaform.view.Validator({
	                    model: this.model.get("validator")
	                });  
	                this.$el.find(".pmdynaform-control-checkbox-list").parent().append(this.validator.el);
	                this.applyStyleError();
	            }
			}
			
			return this;
		},
		updateValueControl: function () {
            var i,
            inputs = this.$el.find("input");

            for (i=0; i<inputs.length; i+=1) {
            	if (inputs[i].checked) {
            		this.model.setItemChecked({
						value: inputs[i].value,
						checked: true
					});
            	}
            }
        	
            return this;    
        },
		onChange: function (event) {
			var checked, data = {};
			this.previousValue = this.model.get("value");
			$(event.target).val();
			checked = event.target.checked;
			this.$el.find("input[type='hidden']").val(JSON.stringify([]));
			if (this.model.get("dataType") === "boolean"){
				if ( checked ) {
					this.$el.find("input[type='checkbox']")[0].checked = true;
					this.$el.find("input[type='checkbox']")[1].checked = false;
					//this.model.set("value",[this.model.get("options")[0].value]);
					this.$el.find("input[type='hidden']").val(JSON.stringify([this.model.get("options")[0].value]));
					data["value"] = [this.model.get("options")[0].value];
					data["label"] = [this.model.get("options")[0].label];
				} else {
					this.$el.find("input[type='checkbox']")[0].checked = false;
					this.$el.find("input[type='checkbox']")[1].checked = true;
					//this.model.set("value",[this.model.get("options")[1].value]);
					this.$el.find("input[type='hidden']").val(JSON.stringify([this.model.get("options")[1].label]));
					data["value"] = [this.model.get("options")[1].value];
					data["label"] = [this.model.get("options")[1].label];
				}
				this.model.attributes.data = data;
				this.model.setItemChecked({
					value: event.target.value,
					checked: checked
				});
			} else {
				this.model.setItemChecked({
					value: event.target.value,
					checked: checked
				});
				this.$el.find("input[type='hidden']").val(JSON.stringify(this.model.get("value")));
				this.validate();
			}
		},
		getHTMLControl: function () {
            return this.$el.find(".pmdynaform-control-checkbox-list");
        },
        setValueHideControl : function () {
        	var control;
        	control = this.$el.find("input[type='hidden']");
        	if (this.model.get("dataType") === "boolean") {
        		if ( jQuery.isArray(this.model.get("data")["value"] ) ) {
        			$(control).val(JSON.stringify(this.model.get("data")["value"]));
        		}else{
        			$(control).val(this.model.get("data")["value"]);
        		}
        	} else {
        		$(control).val(JSON.stringify(this.model.get("data")["value"]));
        	}
        	return this;
        }
	});

	PMDynaform.extendNamespace("PMDynaform.view.Checkbox",CheckboxView);
}());
