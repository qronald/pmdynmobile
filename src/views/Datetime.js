/**
 * The Datetime class was developed with the help of DateBootstrap plugin	
 */
(function(){
	var DatetimeView = PMDynaform.view.Field.extend({
		template : _.template($("#tpl-datetime2").html()),
		validator : null,
		keyPressed: false,
		previousValue : null,
		triggerCallback : false,
		events: {
                "blur input": "validate",
                "keydown input": "refreshBinding"                
        },
        outFocus : false,
		initialize: function () {
            var that = this;
            this.model.on("change", this.checkBinding, this);
        },
        checkBinding: function () {
        	var form = this.model.get("form");
            if ( typeof this.onChangeCallback === 'function' ) {
                this.onChangeCallback(this.getValue(), this.previousValue);
            }

            if ( (form && form.onChangeCallback) && this.triggerCallback ) {
                form.onChangeCallback(this.model.get("name"), this.model.get("value"), this.previousValue);
            	this.triggerCallback = false;
            }else{
            	this.triggerCallback = true;
            }
            //If the key is not pressed, executes the render method
            if (!this.keyPressed) {
                this.render();
            }
        },
        onChangeCallback: function (){},
        setOnChange : function (fn) {
            if (typeof fn === "function") {
                this.onChangeCallback = fn;
            }
            return this;
        },
	    validate: function(event){
			if (event && event.type == "focusout") {
				this.outFocus = true;
			}else{
				this.outFocus = false;
			}
	    	this.previousValue = this.model.get("value");
	    	if (event) {
	    		if ((event.which === 9) && (event.which !==0)) { //tab key
	                this.keyPressed = true;
	            }
	    	}
	    	
	    	if(!this.model.get("disabled")) {
	    		this.model.set({value: this.$el.find("input").val()}, {validate: true});
	            if (this.validator) {
	                this.validator.$el.remove();
	                this.$el.removeClass('has-error');
	            }
	            if(!this.model.isValid()){
	                this.validator = new PMDynaform.view.Validator({
	                    model: this.model.get("validator")
	                });  
	                //this.$el.find(".input-group")[0].insertBefore( this.validator.el ,this.$el.find(".input-group-addon")[0]);
	                //this.$el.find(".input-group").parent().append(this.validator.el);
	                //this.$el.find(".pmdynaform-field-control").append(this.validator.el);
	                this.$el.find(".datetime-container").append(this.validator.el)
	                this.applyStyleError();
	            }
	    	}else{
				itemsField[i].model.isValid();
	    	}
            return this;
        },
        refreshBinding: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            this.keyPressed = true;
            return this;
        },
		render : function (isConsole){
			var data = {}, date, that = this, clickEvent;
			if (!isConsole){
	            this.$el.html( this.template(this.model.toJSON()) );
	            if (this.model.get("hint") !== "") {
	                this.enableTooltip();
	            }
	            if (!this.outFocus){
                        try{
					this.$el.find('#datetime-container-control').datetimepicker({
				            format  : this.model.get("format"),
				            stepping  : this.model.get("stepping"),
				            minDate  : this.model.get("minDate"),
				            maxDate  : this.model.get("maxDate"),
				            useCurrent  : this.model.get("useCurrent"),
				            collapse  : this.model.get("collapse"),
				            defaultDate  : this.model.get("defaultDate"),
				            disabledDates  : this.model.get("disabledDates"),
				            sideBySide  : this.model.get("sideBySide"),
				            daysOfWeekDisabled  : this.model.get("daysOfWeekDisabled"),
				            calendarWeeks  : this.model.get("calendarWeeks"),
				            viewMode  : this.model.get("viewMode"),
				            toolbarPlacement  : this.model.get("toolbarPlacement"),
				            showClear  : this.model.get("showClear"),
				            widgetPositioning  : this.model.get("widgetPositioning"),
				            date : this.model.get("value"),
				            showTodayButton  : true

					});
                        }catch(e){
							this.$el.find('#datetime-container-control').datetimepicker({
				            format  : this.model.get("format"),
				            stepping  : this.model.get("stepping"),
				            useCurrent  : this.model.get("useCurrent"),
				            collapse  : this.model.get("collapse"),
				            defaultDate  : this.model.get("defaultDate"),
				            disabledDates  : this.model.get("disabledDates"),
				            sideBySide  : this.model.get("sideBySide"),
				            daysOfWeekDisabled  : this.model.get("daysOfWeekDisabled"),
				            calendarWeeks  : this.model.get("calendarWeeks"),
				            viewMode  : this.model.get("viewMode"),
				            toolbarPlacement  : this.model.get("toolbarPlacement"),
				            showClear  : this.model.get("showClear"),
				            widgetPositioning  : this.model.get("widgetPositioning"),
				            date :this.model.get("value"),
				            showTodayButton  : true

					});
                        }
	            } else { 
                        try{
					this.$el.find('#datetime-container-control').datetimepicker({
				            format  : this.model.get("format"),
				            stepping  : this.model.get("stepping"),
				            minDate  : this.model.get("minDate"),
				            maxDate  : this.model.get("maxDate"),
				            useCurrent  : this.model.get("useCurrent"),
				            collapse  : this.model.get("collapse"),
				            defaultDate  : this.model.get("defaultDate"),
				            disabledDates  : this.model.get("disabledDates"),
				            sideBySide  : this.model.get("sideBySide"),
				            daysOfWeekDisabled  : this.model.get("daysOfWeekDisabled"),
				            calendarWeeks  : this.model.get("calendarWeeks"),
				            viewMode  : this.model.get("viewMode"),
				            toolbarPlacement  : this.model.get("toolbarPlacement"),
				            showClear  : this.model.get("showClear"),
				            widgetPositioning  : this.model.get("widgetPositioning"),
							showTodayButton  : true
					});
                        } catch(e){
							this.$el.find('#datetime-container-control').datetimepicker({
				            format  : this.model.get("format"),
				            stepping  : this.model.get("stepping"),
				            useCurrent  : this.model.get("useCurrent"),
				            collapse  : this.model.get("collapse"),
				            defaultDate  : this.model.get("defaultDate"),
				            disabledDates  : this.model.get("disabledDates"),
				            sideBySide  : this.model.get("sideBySide"),
				            daysOfWeekDisabled  : this.model.get("daysOfWeekDisabled"),
				            calendarWeeks  : this.model.get("calendarWeeks"),
				            viewMode  : this.model.get("viewMode"),
				            toolbarPlacement  : this.model.get("toolbarPlacement"),
				            showClear  : this.model.get("showClear"),
				            widgetPositioning  : this.model.get("widgetPositioning"),
							showTodayButton  : true
					});
                        }
	            }

	            this.model.attributes.value = this.$el.find("input[type='text']").val();

				if ( this.model.get("value").trim().length ) {
					data["value"] = this.formatData( this.model.get("value"));
					data["label"] = this.formatData( this.model.get("value"));
					this.model.attributes.data = data;
				}
				if (this.model.get("value").trim().length){
					this.$el.find("input[type='hidden']").val(this.model.get("data").value);
				}
            }

			if (this.model.get("name").trim().length === 0){
				this.$el.find("input[type='text']").attr("name","");
				this.$el.find("input[type='hidden']").attr("name","");
			}
			return this;
		},
		formatData : function(date){
			date = date.replace(/-/g,"/");
			var now, year, month, day, hour, minute, second;
			now = new Date(date);
			year = "" + now.getFullYear();
			month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
			day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
			hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
			minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
			second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
			return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
		}
	})
	PMDynaform.extendNamespace("PMDynaform.view.Datetime",DatetimeView);
}());
