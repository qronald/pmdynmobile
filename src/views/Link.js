(function(){
    
    var LinkView = PMDynaform.view.Field.extend({
        template: _.template($("#tpl-link").html()),
        validator: null,
        
        initialize: function (){
            var that = this;
            this.model.on("change", this.render, this);
        }
    });

    PMDynaform.extendNamespace("PMDynaform.view.Link",LinkView);
}());
