(function(){
	var SubFormModel = Backbone.Model.extend({
		defaults: {
			colSpan: 12,
			namespace: "pmdynaform",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("form"),
			type: "form",
			mode: "edit",
			valid: true,
			modelForm: null
		},
		initialize: function () {
			
		},
		getData: function () {
			return {
				name: this.get("name"),
				id: this.get("id"),
				variables: {}
			}
		}
	});
	
	PMDynaform.extendNamespace("PMDynaform.model.SubForm", SubFormModel);
}());