(function(){
	
	var Validator = Backbone.Model.extend({
        defaults: {
            message: {},
            title: "",
            type: "",
            dataType: "",
            value: "",
            valid: true,
            maxLength: null,
            required: false,
            domain: false,
            options: [],
            factory: {},
            valueDomain: null,
            regExp : null,
            haveOptions: [
                "suggest",
                "checkbox",
                "radio",
                "dropdown"
            ]
        },
        initialize: function() {
        	var factoryValidator = {
        		"text": "requiredText",
    			"checkbox": "requiredCheckBox",
    			"radio": "requiredRadioGroup",
    			"dropdown": "requiredDropDown",
    			"textarea": "requiredText",
    			"datetime": "requiredText",
                "suggest": "requiredText" ,
                "file" : "requiredFile"                
        	};
        	this.setFactory(factoryValidator);
            this.checkDomainProperty();
        },
        setFactory: function(obj) {
        	this.set("factory", obj);
        	return this;
        },
        checkDomainProperty: function () {
            this.attributes.domain = ($.inArray(this.get("type"), this.get("haveOptions")) >= 0)? true: false;
            return this;
        },
        verifyValue: function () {
            var value = this.get('value'),
            valueDomain = this.get('valueDomain'),
            options = this.get('options'),
            validator = this.attributes.factory[this.get("type").toLowerCase()],
            regExp;

            this.set("valid", true);
            delete this.get("message")[validator];
            if (this.get("required")) {
            	if (PMDynaform.core.Validators[validator].fn(value) === false) {
	                this.set("valid", false);
	                this.set("message", {
	                    validator: PMDynaform.core.Validators[validator].message
	                });
                    return this;
	            }
            }

            if (this.get("dataType") !== "" && value !== "") {
                if (PMDynaform.core.Validators[this.get("dataType")] && PMDynaform.core.Validators[this.get("dataType")].fn(value) === false) {
                    this.set("valid", false);
                    this.set("message", {
                        "validator": PMDynaform.core.Validators[this.get("dataType")].message
                    });
                    return this;
                }
            }

            if (this.get("maxLength")) {
                 if (PMDynaform.core.Validators.maxLength.fn( value, parseInt(this.get("maxLength"))) === false) {
                    this.set("valid", false);
                    this.set("message", {
                        validator: PMDynaform.core.Validators.maxLength.message + " " +this.get("maxLength") + " characters"
                    });
                }
            }

            if (this.get("regExp") && this.get("regExp").validate !== "any"){
                regExp = new RegExp(this.get("regExp").validate);
                if (!regExp.test(value) ) {
                    this.set("valid", false);
                    this.set("message", {validator:this.get("regExp").message});
                    return this;
                }
            }

            /*if (this.get("domain") === true && value !== "") {
                if (PMDynaform.core.Validators["domain"].fn(valueDomain, options) === false) {
                    this.set("valid", false);
                    this.set("message", {
                        validator: PMDynaform.core.Validators['domain'].message
                    });
                }
            }*/
        }
    });
    PMDynaform.extendNamespace("PMDynaform.model.Validator", Validator);

}());