(function(){
	var ImageModel = PMDynaform.model.Field.extend({
		defaults: {
			colSpan: 12,
			colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
			disabled: false,
			defaultValue: "",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("image"),
			label: "untitled label",
			crossorigin: "anonymous",
			alt: "",
			src: "",
			height: "",
			width: "",
			mode: "view",
			shape: "thumbnail",
			shapeTypes: {
				thumbnail: "img-thumbnail",
				rounded: "img-rounded",
				circle: "img-circle"
			},
			type: "image",
            columnName : null,
            originalType : null,
            group : "form"
		},
		initialize: function (options) {
			var defaults;

			this.set("label", this.checkHTMLtags(this.get("label")));
            this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));
            this.on("change:label", this.onChangeLabel, this);
            this.on("change:value", this.onChangeValue, this);
			if(options.project) {
                this.project = options.project;
            }
            this.setShapeType();
		},
		setShapeType: function () {
			var shape = this.get("shape"),
			types = this.get("shapeTypes"),
			selected;

			selected =types[shape] ? types[shape] : types["thumbnail"];
			this.set("shape", selected);
			return;
		}
	});
	
	PMDynaform.extendNamespace("PMDynaform.model.Image", ImageModel);
}());
