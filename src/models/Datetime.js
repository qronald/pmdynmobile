(function(){
	var DatetimeModel =  PMDynaform.model.Field.extend({
		defaults: {
			colSpan : 12,
			colSpanLabel : 3,
			colSpanControl : 9,
			namespace : "pmdynaform",
			dataType : "date",
			group : "form",
			hint : "",
			id : "",
			name : "",
			placeholder : "",
			required : false,
			validator : null,
			originalType : null,
			disabled : false,
			format : false,
			mode : "edit",
			data : null,
			value :"",
			stepping : 1,
			minDate : false,
			maxDate : false,
			useCurrent : false,
			collapse : true,
			defaultDate : false,
			disabledDates : [],
			sideBySide : false,
			daysOfWeekDisabled : [],
			calendarWeeks : true,
			viewMode : "days",
			toolbarPlacement : "default",
			showTodayButton : true,
			showClear : true,
			widgetPositioning : {
                horizontal : "left",
                vertical : "bottom"
			},
			keepOpen : false,
			dayViewHeaderFormat : "MMMM YYYY",
			pickType : "datetime"
		},
		getData: function() {
			if (this.get("group") == "grid"){
				return {
					name : this.get("columnName") ? this.get("columnName"): "",
					value :  this.get("value")
				}

			} else {
				return {
					name : this.get("name") ? this.get("name") : "",
					value :  this.get("value")
				}
			}
			return this;
		},
		initialize: function(options) {
			var useDefaults = {
				showClear : false,
				useCurrent : true
			}, 
			useCurrentOptions = ['year', 'month', 'day', 'hour', 'minute'],
			viewMode = ['years', 'months', 'days'],
			data = {
				value : "",
				label : ""
			}, defaultDate, maxOrMinDate, flag = true;
			
			if (this.get("useCurrent") === "true") {
				this.attributes.useCurrent = JSON.parse(this.get("useCurrent"));
			}
			if (useCurrentOptions.indexOf(this.get("useCurrent")) === -1){
				this.attributes.useCurrent = useDefaults["useCurrent"];
			}
			
			if (this.get("showClear") === "true"){
				this.attributes.showClear = JSON.parse(this.get("showClear"));
			}

			if (this.get("showClear") === "false"){
				this.attributes.showClear = JSON.parse(this.get("showClear"));			
			}
			if ( typeof this.get("showClear") !== "boolean" ){
				this.attributes.showClear = useDefaults["showClear"];
			}
			
			if (this.get("format") === "false"){
				this.attributes.format = JSON.parse(this.get("format"));			
			}
			
			if ( viewMode.indexOf(options["viewMode"]) === -1){
				this.attributes.viewMode = "days";
			}

			this.customPickTimeIcon(this.get("pickType"));

			if ( !_.isEmpty(this.get("data")) && (this.get("data")["value"].trim().length || this.get("data")["label"].trim().length)   ) {
				this.set("defaultDate",false);
			}else{
				this.set("data",data);
			}
			
			
			if ( this.get("maxDate").trim().length && this.get("defaultDate") && this.get("defaultDate").trim().length ) {
				defaultDate =  this.get("defaultDate").split("-");
				maxOrMinDate = this.get("maxDate").split("-");
				if ( (parseInt(defaultDate[0]) <= parseInt(maxOrMinDate[0])) ){
					if( (parseInt(defaultDate[1]) <= parseInt(maxOrMinDate[1])) ){
						if( (parseInt(defaultDate[2]) <= parseInt(maxOrMinDate[2])) ) {
							flag = true;
						}else{
							flag = false;
						}
					} else{
						flag = false;
					}
				}else{
					flag = false;
				}
				if (!flag){
					this.set("defaultDate",false);
				}
			}
			if (flag){
				if ( this.get("minDate").trim().length && this.get("defaultDate") && this.get("defaultDate").trim().length ) {
					defaultDate =  this.get("defaultDate").split("-");
					maxOrMinDate = this.get("minDate").split("-");
					if ( (parseInt(defaultDate[0]) >= parseInt(maxOrMinDate[0])) ){
						if( (parseInt(defaultDate[1]) >= parseInt(maxOrMinDate[1])) ){
							if( (parseInt(defaultDate[2]) >= parseInt(maxOrMinDate[2])) ) {
								flag = true;
							}else{
								flag = false;
							}
						} else{
							flag = false;
						}
					}else{
						flag = false;
					}
					if (!flag){
						this.set("defaultDate",false);
					}
				}
			}			
			this.attributes.value = this.get("data")["value"];
            this.set("validator", new PMDynaform.model.Validator({
            	required : this.get("required"),
            	type : this.get("type"),
            	dataType :this.get("dataType")
            }));

			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
			return this;
		},
		customPickTimeIcon : function (format) {
			
		},
        isValid: function(){
            //this.set("valid", this.get("validator").get("valid"));
            this.attributes.valid = this.get("validator").get("valid");
            return this.get("valid");
        },
        validate: function (attrs) {
            var valueFixed = attrs.value.trim();
            this.set("value",valueFixed);
            //this.attributes.value = valueFixed;
            this.get("validator").set("value", valueFixed);
            this.get("validator").verifyValue();
            this.isValid();
            return this.get("valid");
        }
	});

	PMDynaform.extendNamespace("PMDynaform.model.Datetime", DatetimeModel);
}());