(function(){
	var DropdownModel =  PMDynaform.model.Field.extend({
		defaults: {
			colSpan: 12,
            colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
            dataType: "string",
            defaultValue: "",
            dependenciesField: [],
            disabled: false,
            executeInit: true,
            group: "form",
            hint: "",
            id: PMDynaform.core.Utils.generateID(),
            name: PMDynaform.core.Utils.generateName("dropdown"),
			label: "untitled label",
			localOptions: [],
            mode: "edit",
            options: [
                {
                    label: "Empty",
                    value: "empty"
                }
            ],
            remoteOptions: [],
            required: false,
            type: "text",
            valid: true,
            validator: null,
            variable: null,
            var_uid: null,
            var_name: null,
            variableInfo: {},
            value: "",
            columnName : null,
            originalType : null,
            data : null,
            itemClicked : false,
		},
		initialize: function(options) {
            var data;
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.on("change:label", this.onChangeLabel, this);
            this.on("change:value", this.onChangeValue, this);
            this.on("change:options", this.onChangeOptions, this);
            this.set("validator", new PMDynaform.model.Validator({
                domain: true
            }));
            this.set("dependenciesField",[]);
            this.setLocalOptions();
            this.setDefaultValue ();
            data = this.get("data");
            if ( data ) {
                this.attributes.value = data["value"];
            } else {
                if (this.get("options").length){
                    this.set("value",this.get("options")[0]["value"]);
                    this.set("data",{
                        value:this.get("options")[0]["value"],
                        label : this.get("options")[0]["label"]
                    });
                } else {
                    this.set("data",{value:"", label:""});
                    this.set("value","");
                }
            }
			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
        },
        getData : function (){
            if (this.get("group") == "grid"){
                return {
                    name : this.get("columnName") ? this.get("columnName"): "",
                    value :  this.get("value")
                }

            } else {
                return {
                    name : this.get("name") ? this.get("name") : "",
                    value :  this.get("value")
                }
            }
        },
        setDefaultValue: function () {
            var options = this.get("options"),
            defaultValue = this.get("defaultValue");
            
            if ($.inArray(defaultValue.trim(), ["", null, undefined]) > 0) {
                this.set("defaultValue", options[0].value);
                this.set("value", options[0].value);
            }

            return this;
        },
        setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
        setDependencies: function(newDependencie) {
            var arrayDep, i, result, newArray = [];
            arrayDep = this.get("dependenciesField");
            if(arrayDep.indexOf(newDependencie) === -1){
            	arrayDep.push(newDependencie);
            }
            //this.set("dependenciesField",[]);
            this.set("dependenciesField",arrayDep);
        },
        isValid: function(){
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        validate: function (attrs) {
        	
    		var valueFixed = attrs.value.trim();
            this.attributes.value = valueFixed; 
            //this.set("value", valueFixed);
            this.get("validator").set("type", attrs.type);
            this.get("validator").set("required", attrs.required);
            this.get("validator").set("value", valueFixed);
            
            this.get("validator").set("dataType", attrs.dataType);
            this.get("validator").verifyValue();
        	this.isValid();
            return this.get("valid");
        },
        onChangeValue: function (attrs, options) {
            var i, opts, data = {};
            this.attributes.value = this.checkHTMLtags(attrs.attributes.value);
            if (this.attributes.options) {
                this.get("validator").set({
                    valueDomain: this.get("value"),
                    options: this.get("options") || []
                });
                this.get("validator").verifyValue();
            }
            if (!this.itemClicked){
                opts = this.get("options");
                for ( i = 0 ; i < opts.length ; i+=1 ) {
                    if (opts[i]["value"] === this.get("value")){
                        data["value"] = opts[i]["value"];
                        data["label"] = opts[i]["label"];
                        break;
                    }
                }
                this.attributes.data = data;
            }
            this.itemClicked = false;
            return this;
        }
	});
	PMDynaform.extendNamespace("PMDynaform.model.Dropdown", DropdownModel);

}());