(function(){

	var TextModel = PMDynaform.model.Field.extend({
		defaults: {
			type: "text",
			placeholder: "",
			label: "untitled label",
			id: PMDynaform.core.Utils.generateID(),
			name: PMDynaform.core.Utils.generateName("text"),
			colSpan: 12,
			colSpanLabel: 3,
			colSpanControl: 9,
			maxLengthLabel: 15,
			namespace: "pmdynaform",
			operation: null,
			tooltipLabel: "",
			value: "",
			group: "form",
			defaultValue: "",
			dataType: "string",
			hint: "",
			mask: "",
			disabled: false,
			maxLength: null,
			mode: "edit",
			autoComplete: "off",
			required: false,
			formulator: null,
			validator: null,
			textTransform: "",
			valid: true, 
			variable: null,
			var_uid: null,
			var_name: null,
			columnName : null,
			originalType : null,
			data : null,
			localOptions: [],
			options: [
				{
					label: "Empty",
					value: "empty"
				}
			],
			keyValue : null,
			remoteOptions: [],
			dependenciesField: []
		},
		initialize: function(attrs) {
			var data, maxLength;
			this.set("dataType", this.get("dataType").trim().length ? this.get("dataType") : "string");
			this.on("change:label", this.onChangeLabel, this);
			this.on("change:options", this.onChangeOptions, this);
			this.on("change:value", this.onChangeValue,this);
			this.on("change:value", this.onChangeData,this);
			this.set("label", this.checkHTMLtags(this.get("label")));
			this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));
			this.set("validator", new PMDynaform.model.Validator({
				"type"  : this.get("type"),
				"required" : this.get("required"),
				"maxLength" : this.get("maxLength"),
				"dataType" : this.get("dataType") || "string",
				"regExp" : { 
					validate : this.get("validate"), 
					message : this.get("validateMessage")
				}
			}));
			this.set("dependenciesField",[]);
			data = this.get("data");
			if ( data ) {
				this.set("value", data["label"]);
			} else {
				this.set("data",{value:"", label:""});
				this.set("value","");
			}
			this.initControl();
			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
		},
		onChangeData : function () {
			
		},
		initControl: function() {
			if (this.get("defaultValue")) {
				this.set("value", this.get("defaultValue"));
			}
			if (typeof this.get("formula") === "string" && 
				this.get('formula') !== "undefined" &&
				this.get('formula') !== "null" &&
				this.get('formula').length > 1) {
				this.set("formulator", new PMDynaform.core.Formula(this.get("formula")));
				this.set("disabled", true);
			}
		},
		addFormulaTokenAssociated: function(formulator) {
			if (formulator instanceof PMDynaform.core.Formula) {
				//formulator.addField("field", this.get("name"));
				formulator.addTokenValue(this.get("id"), this.get("value"));
			}
			return this;
		},
        setDependencies: function(newDependencie) {
            var arrayDep, i, result, newArray = [];
            arrayDep = this.get("dependenciesField");
            if(arrayDep.indexOf(newDependencie) === -1){
            	arrayDep.push(newDependencie);
            }
            this.set("dependenciesField",arrayDep);
        },
		addFormulaFieldName: function(otherField) {
			this.get("formulator").addField("field", otherField);
			return this;
		},
		updateFormulaValueAssociated: function(field) {
			var resultField = field.model.get("formulator").evaluate();

			field.model.set("value", resultField);
			return this;
		},
		isValid: function() {
			this.attributes.valid = this.get("validator").get("valid");
			//this.set("valid", this.get("validator").get("valid"));
			return this.get("valid");
		},
		validate: function (attrs) {
			var valueFixed = attrs.value.trim();
			this.set("value", valueFixed);
			this.get("validator").set("value", valueFixed);
			this.get("validator").verifyValue();
			this.isValid();
			return this.get("valid");
		},
		getData: function() {
			if (this.get("group") == "grid"){
				return {
					name : this.get("columnName") ? this.get("columnName"): "",
					value :  this.get("value")
				}

			} else {
				return {
					name : this.get("name") ? this.get("name") : "",
					value :  this.get("value")
				}
			}
		},
		getData2: function() {
			var data = {}, name, value;
			name = this.get("variable") ? this.get("variable").var_name : this.get("name");
			value = this.get("value");
			data[name] = value;
			return data;
		}
	});
	PMDynaform.extendNamespace("PMDynaform.model.Text", TextModel);
}());