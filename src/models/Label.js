(function(){

	var Label = PMDynaform.model.Field.extend({
		defaults: {
            colSpan: 12,
            group: "form",
            hint: "",
            namespace: "pmdynaform",
            id: PMDynaform.core.Utils.generateID(),
            name: PMDynaform.core.Utils.generateName("label"),
            label: "untitled label",
            mode: "view",
            options: [],
            required: false,
            type: "label",
            columnName : null,
            originalType : null,
            variable: null,
            var_uid: null,
            var_name: null,
            localOptions : null,
            remoteOptions : null,
            fullOptions : [""],
            data : null
        },
        getData: function() {
            if (this.get("group") == "grid"){
                return {
                    name : this.get("columnName") ? this.get("columnName"): "",
                    value :  this.get("value")
                }
            } else {
                return {
                    name : this.get("name") ? this.get("name") : "",
                    value :  this.get("value")
                }
            }
        },
        initialize: function(options) {
            var i, aux,
            newOptions = [],
            fullOptions = this.get("options"),
			data;

            this.set("label", this.checkHTMLtags(this.get("label")));
            this.on("change:label", this.onChangeLabel, this);
            this.on("change:options", this.onChangeOptions, this);

            this.setLocalOptions();
            for (i=0; i<fullOptions.length; i+=1) {
                newOptions.push(this.checkHTMLtags(fullOptions[i]));
            }
            this.set("options", newOptions);
			data = this.get("data");
			if ( data ) {
				this.set("value", data["label"]);
			} else {
				this.set("data",{value:"", label:""});
				this.set("value","");
			}
            if ( this.get("var_name").trim().length === 0) {
                if ( this.get("group") === "form" ) {
                    this.attributes.name = "";
                } else {
                    this.attributes.name = this.get("id");
                }
            }
        },
        setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
    });
    PMDynaform.extendNamespace("PMDynaform.model.Label", Label);
}());