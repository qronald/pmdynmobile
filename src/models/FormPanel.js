(function(){
	var FormPanel = Backbone.Model.extend({
		defaults: {
			action: "",
			autocomplete: "on",
			script: {},
			data: [],
			items: [],
			name: 'PMDynaform-form',
			method: "get",
			namespace: "pmdynaform",
			target: null,
			type: "panel"
		},
		getData: function(){
			return {
				type: this.get("type"),
				action: this.get("action"),
				method: this.get("method")
			}
		}
	});
	PMDynaform.extendNamespace("PMDynaform.model.FormPanel", FormPanel);
}());