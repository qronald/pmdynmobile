(function(){
	var CheckboxModel =  PMDynaform.model.Field.extend({
		defaults: {
			colSpan: 12,
            colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
            dataType: "string",
            dependenciesField: [],
            disabled: false,
            group: "form",
            hint: "",
            id: PMDynaform.core.Utils.generateID(),
            name: PMDynaform.core.Utils.generateName("checkbox"),
			label: "",
            localOptions: [],
            maxLengthLabel: 15,
            mode: "edit",
            options: [
                {
                    label: "empty",
                    value: "empty"
                }
            ],
            readonly: false,
            required: false,
            remoteOptions: [],
            selected: [],
            type: "checkbox",
            tooltipLabel: "",
            validator: null,
            valid: true,
            var_name: null,
            var_uid: null,
            value: [],
            variableInfo: {},
            columnName : null,
            originalType : null,
            data : null,
            defaultValue : null
		},
		initialize: function (attrs) {
            var i,j,d,data, that = this, option;
            if(this.get("group") === "grid"){
				this.attributes.options = [
					{
						"value": "1",
						"label": "true"
					},
					{
						"value": "0",
						"label": "false"
					}
                ];
                if(!_.isEmpty(this.get("data") ) ) {
                    if(JSON.parse(this.get("data")["label"])[0] == "1" ||  this.get("data")["label"] == true){
                       this.get("data")["value"] = "1" 
                    }else{
                       this.get("data")["value"] = "0" 
                    }
                }
				this.set("dataType","boolean");
			}
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.on("change:label", this.onChangeLabel, this);
            this.on("change:options", this.onChangeOptions, this);
            this.on("change:value", this.updateItemSelected, this);
			this.set("validator", new PMDynaform.model.Validator({
                type: attrs.type,
                required: attrs.required,
                dataType: attrs. dataType
            }));
            if ( !this.get("data") || !this.get("data").value.length) {
                data = {
                    value : JSON.stringify([]),
                    label : JSON.stringify([])
                }
                if (this.get("dataType") === "boolean") {
                    if (this.get("defaultValue") === "true") {
                        data["value"] = [this.get("options")[0].value];
                        data["label"] = [this.get("options")[0].label];
                        this.set("value", this.get("options")[0].value);
                    } else {
                        data["value"] = [this.get("options")[1].value];
                        data["label"] = [this.get("options")[1].label];
                        this.set("value", this.get("options")[1].value);
                    }
                }
                this.set("data",data);
            }

            this.initControl();
            this.attributes.value = this.get("data").value; 
            //this.set("value",this.get("data").value);
            this.get("validator").set("value",this.get("value"));
            this.set("dependenciesField",[]);
            this.setLocalOptions();
			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
            return this;
		},
		initControl: function() {
			var opts = this.get("options"), 
            i,
            newOpts = [],
			itemsSelected = [];

			for (i=0; i<opts.length; i+=1) {
				if (!opts[i].label) {
					throw new Error ("The label parameter is necessary for the field");
				}
				if (!opts[i].value && (typeof opts[i].value !== "number") ) {
					opts[i].value = opts[i].label;
				}
                if (this.get("data") && this.get("data").value){
                    if (this.get("dataType") === "boolean") {
                        if (this.get("data").value.indexOf(opts[i].value) > -1 ) {
                            opts[i].selected = true;
                        }else{
                            opts[i].selected = false;
                        }
                    } else {
                        if (this.get("data").value.indexOf(opts[i].value) > -1 ) {
                            opts[i].selected = true;
                        }  
                    }
                }
                newOpts.push({
                    label: this.checkHTMLtags(opts[i].label),
                    value: this.checkHTMLtags(opts[i].value),
                    selected: opts[i].selected? true : false
                });
			}
            this.set("options", newOpts);
			this.set("selected", itemsSelected);
		},
		setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
		getData: function() {
            if (this.get("group") == "grid"){
                return {
                    name : this.get("columnName") ? this.get("columnName") : "",
                    value :  this.get("dataType") === "boolean" ? this.get("value")[0] : this.get("value") 
                }

            } else {
                return {
                    name : this.get("name") ? this.get("name") : "",
                    value :  this.get("dataType") === "boolean" ? this.get("value")[0] : this.get("value") 
                }
            }
            return this;
			/*return {
                name: this.get("variable").var_name,
                value: this.get("selected").toString()
            };*/

		},	
		validate: function(attrs) {
			
            //this.get("validator").set("type", attrs.type);
            this.get("validator").set("value", attrs.selected.length);
            //this.get("validator").set("required", attrs.required);
            //this.get("validator").set("dataType", attrs.dataType);
            if(this.get("options").length){
                this.get("validator").set("options",this.attributes.options); 
            }
            this.get("validator").verifyValue();
            this.isValid();
            return this.get("valid");
		},
		isValid: function(){
            this.attributes.valid = this.get("validator").get("valid"); 
            //this.set("valid", this.get("validator").get("valid"));
        	return this.get("valid");
        },
		setItemChecked: function(itemUpdated) {
			var opts = this.get("options"),
				selected = [],
				i;
			if (opts) {
                if (this.get("dataType") !== "boolean") {
    				for(i=0; i<opts.length; i+=1) {
    					if(opts[i].value.toString() === itemUpdated.value.toString()) {
    						opts[i].selected = itemUpdated.checked;
    					}
    				}
                } else {
                    if (itemUpdated.checked) {
                        opts[0].selected = true;
                        opts[1].selected = false;
                    } else {
                        opts[0].selected = false;
                        opts[1].selected = true;
                    }
                }
                this.set("options", opts);

                for ( i = 0; i < opts.length; i+=1 ) {
                    if ( opts[i].selected) {
                        selected.push(opts[i].value);
                    }
                }
                if (selected.length) {
                    this.attributes.value = selected;
                }else{
                    this.attributes.value = [];
                }
                this.set("selected", selected);
                //this.changeValuesFieldsRelated();
			}

            return this;
		},
        updateItemSelected: function () {
            var i, data = {},
            selected = this.get("selected"), auxValue, opts = this.get("options");

            if (typeof this.attributes.value === "string"
                && this.attributes.value.length > 0) {
                selected = this.attributes.value.split(/,/g);    
            }
            if ($.isArray(this.get("value"))) {
                this.set("selected",[]);
                selected = this.get("selected");
                for ( i = 0 ; i < opts.length ; i+=1 ){
                    opts[i].selected = false;                    
                }
                this.set("options",opts);
                auxValue = this.get("value");
                if (this.get("dataType") !== "boolean" ) {
                    for ( i = 0 ; i < auxValue.length ; i+=1 ){
                        this.setItemChecked({
                            value: auxValue[i],
                            checked: true
                        });
                    }
                } else { 
                    this.setItemChecked({
                        value: this.get("options")[0],
                        checked: parseInt(auxValue.toString()) === 1 ? true : false
                    });
                }
            } else {
                this.setItemChecked({
                    value: this.attributes.value,
                    checked: true
                });                
            }
            for (i=0; i<selected.length; i+=1) {
                this.setItemChecked({
                    value: selected[i].trim ? selected[i].trim() : selected[i],
                    checked: true
                });
            }
            if (!this.attributes.disabled) {
                this.get("validator").set({
                    valueDomain: this.get("value"),
                    options: this.get("options")
                    //domain: this.attributes.value !== ""? true: false
                });
                this.get("validator").set("value", this.get("selected").length);
                /*if(this.get("options").length){
                    this.get("validator").set("options",this.attributes.options); 
                }*/
                this.get("validator").verifyValue();
            }
            if (this.attributes.data) {
                this.attributes.data["value"] = this.get("value");
            } 
            return this;
        }
	});

	PMDynaform.extendNamespace("PMDynaform.model.Checkbox",CheckboxModel);
}());