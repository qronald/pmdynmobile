(function(){
	var PanelField = PMDynaform.model.Field.extend({
		defaults: {
            type: "panel",
            showHeader : false,
            showFooter : false,
            title : "untitled-panel",
            footerContent : "<div>footer pmdynaform!</div>",
            content : "<div>content Body in panel PMDynaform</div>",
            id: PMDynaform.core.Utils.generateID(),
            colSpan: 12,
            namespace: "pmdynaform",
            typePanel : "default",
            border : "1px"
        },
        initialize : function (options) {
            var length;
            if (options["border"]){
                length = this.verifyLenght(options["border"]);
                this.set("border",length);
            }

        },
        verifyLenght : function (length) {
            var length;
            if(typeof length === 'number') {
                length = length + "px";   
            } else if(Number(length).toString() != "NaN"){
                length = length + "px";
            }
            else if(/^\d+(\.\d+)?px$/.test(length)) {
                length = length;
            } else if(/^\d+(\.\d+)?%$/.test(length)) {
                length = length;
            } else if(/^\d+(\.\d+)?em$/.test(length)) {
                length = length;
            } else if(length === 'auto' || length === 'inherit') {
                length = length;
            } else {
                length = "1px";   
            }
            return length;
        }

    });
    PMDynaform.extendNamespace("PMDynaform.model.PanelField", PanelField);
}());