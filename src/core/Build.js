(function(){
	var Build = function(fieldSettings) {
        this.field = fieldSettings;
        this.variable = null;
        this.restriction={ 
			"string":["text","textarea","dropdown","checkbox","radio","datetime","date","time","label"],
			"boolean":["text","dropdown","radio","checkbox","label"],
			"integer":["text","dropdown","radio","checkbox","label"],
			"float":["text","label"],
			"datetime":["datetime","date","time","label"]			
		};
        //Build.prototype.init.call(this);  
    };

    Build.prototype.select = function (){
		for (var i=0; i<this.restriction[this.variable.fieldType].length;i++){
			if(this.restriction[this.variable.fieldType][i]==this.field.type)
				return true;
		}
		return false;
	};

	Build.prototype.selectVar = function (){
		for (var i=0 ; i< PMDynaform.variables.length ; i++){
			if(PMDynaform.variables[i].name == this.field.variable)
				return PMDynaform.variables[i];
		}
		return null;
	};	   

    Build.prototype.init = function () {
    	this.variable=this.selectVar();
    	if(this.select()&& this.variable!=null){
			switch (this.field.type) { 
				case "text": 
					 vars = this.buildText();	
					 break 
				case "textarea": 
					 vars = this.buildTextArea();	
					 break 
				case "dropdown": 
					 vars = this.buildDropDown();	
					 break
				case "radio": 
					 vars = this.buildRadio();	
					 break
				case "checkbox": 
					 vars = this.buildCheckbox();			
					 break
				case "datetime": 
					 vars = this.buildDatetime();			
					 break	 
				default: 
					 vars= this.field;	
			}
		}				
        return vars;	
    };

	Build.prototype.buildText = function () {
		var defaults,vars, res; 
		defaults= {
			id: PMDynaform.core.Utils.generateID(),
			type: "text",
			placeholder: "untitled",
			label: "untitled label",			
			name: "name",
			colspan: 12,
            value: "",
            validationType: "",
            required: false
        };
        jQuery.extend(true, defaults, this.field);
		vars={
		   "label":{
				name:"label",
				priority:"view"
		    },																		
		   "defaultValue":{
				name:"value",
				priority:"view"
		    },
		   "fieldType":{
				name:"validationType",
				priority:"view"
		    }   	
		};
		//validation maxlength
		if(defaults.maxLength){
			defaults.maxLength = parseInt(defaults.maxLength);
			if(!(this.variable.fieldSize >= defaults.maxLength &&  defaults.maxLength>=0)){
				defaults.maxLength = parseInt(this.variable.fieldSize);				
			}
		}

		res = this.replaceVariables(defaults, vars);
		return res;	
    };
    
    Build.prototype.buildTextArea = function (){
		var defaults,vars, res; 
		defaults= {
			id: PMDynaform.core.Utils.generateID(),
			type: "text",
			placeholder: "untitled",
			label: "untitled label",			
			name: "name",
			colspan: 12,
            value: "",
            validationType: "",
            required: false
        };
        jQuery.extend(true, defaults, this.field);
		vars={
		   "label":{
				name:"label",
				priority:"view"
		    },																		
		   "defaultValue":{
				name:"value",
				priority:"view"
		    },
		   "fieldType":{
				name:"validationType",
				priority:"view"
		    }   	
		};
		//validation maxlength
		if(defaults.maxLength){
			defaults.maxLength = parseInt(defaults.maxLength);
			if(!(this.variable.fieldSize >= defaults.maxLength &&  defaults.maxLength>=0)){
				defaults.maxLength = parseInt(this.variable.fieldSize);				
			}
		}


		res = this.replaceVariables(defaults, vars);
		return res;	
	};

	Build.prototype.buildDropDown = function (){
		var drop, defaults, vars, res;
		defaults = {
			id: "",
			type: "text",
			options: [],
			label: "untitled label",
			name: "name",
			colspan: 12,
            value:"",
            validationType: "",
            required: false
        };
        jQuery.extend(true, defaults, this.field);

        if(this.variable.dbConnection != null && this.variable.sql != null){			
			/*drop = [
					    {
					        "label": "option0",
					        "value": "option0"
					    },
					    {
					        "label": "option1",
					        "value": "option1"
					    },
					    {
					        "label": "option2",
					        "value": "option2"
					    },
					    {
					        "label": "option3",
					        "value": "option3"
					    }
					];
			if(variableZ.acceptedValues){
				for (var i = 0; i< drop.length;i++){
					if(drop[0].value == )
				}
			}*/		
		}


		vars={
			   "label":{
					name:"label",
					priority:"view"
			    },
			   "default":{
					name:"defaultValue",
					priority:"view"
			    },
			   "fieldType":{
					name:"validation",
					priority:"view"
			    },
			    "options":{
					name:"options",
					priority:"view"
			    }   	
			};
		//VALIDATIONS OF ACCEPTED VALUES
		/*if(vars.options.priority == "view"){
			if(this.field.options.length != 0){	
				if(this.variable.acceptedValues.length !=0){
					for (var i = 0; i< field.options.length;i++){
						if(jQuery.inArray(this.field.options[i].value, this.variable.acceptedValues) == -1){
							console.log("Value not in accepted values");
						}
					} 
				}
			}
		}*/ 	
		//this.variable.options = drop;
		res = this.replaceVariables(defaults, vars);
		return res;
	};

	Build.prototype.buildRadio = function (){
		var radio, defaults, vars, res;
		defaults = {
			id: "",
			type: "text",
			options: [],
			label: "untitled label",
			name: "name",
			colspan: 12,
            value:"",
            validationType: "",
            required: false
        };
        jQuery.extend(true, defaults, this.field);

        if(this.variable.dbConnection != null && this.variable.sql != null){
			//crear las opciones
			radio = [
					    {
					        "label": "option0",
					        "value": "option0"
					    },
					    {
					        "label": "option1",
					        "value": "option1"
					    },
					    {
					        "label": "option2",
					        "value": "option2"
					    },
					    {
					        "label": "option3",
					        "value": "option3"
					    }
					];
		}
		this.variable.options = radio;	
		vars={
			   "label":{
					name:"label",
					priority:"view"
			    },
			   "default":{
					name:"defaultValue",
					priority:"view"
			    },
			   "fieldType":{
					name:"validation",
					priority:"view"
			    },
			    "options":{
					name:"options",
					priority:"variable"
			    }   	
			};
		res = this.replaceVariables(defaults, vars);
		return res;
	};
 
 	Build.prototype.buildCheckbox= function (){
		var check, defaults, vars, res;
		defaults = {
			id: "",
			type: "text",
			options: [],
			label: "untitled label",
			name: "name",
			colspan: 12,
            value:"",
            validationType: "",
            required: false
        };
        jQuery.extend(true, defaults, this.field);

        if(this.variable.dbConnection != null && this.variable.sql != null){
			//crear las opciones
			check = [
					    {
					        "label": "option0",
					        "value": "option0"
					    },
					    {
					        "label": "option1",
					        "value": "option1"
					    },
					    {
					        "label": "option2",
					        "value": "option2"
					    },
					    {
					        "label": "option3",
					        "value": "option3"
					    }
					];
		}
		this.variable.options = check;	
		vars={
			   "label":{
					name:"label",
					priority:"view"
			    },
			   "default":{
					name:"defaultValue",
					priority:"view"
			    },
			   "fieldType":{
					name:"validation",
					priority:"view"
			    },
			    "options":{
					name:"options",
					priority:"variable"
			    }   	
			};
		res = this.replaceVariables(defaults, vars);
		return res;
	};

	Build.prototype.buildDatetime= function (){
		var check, defaults, vars, res;
		defaults= {
			id: "",
			type: "datetime",
			pickType:"datetime",
			label:"label"			
		};
        jQuery.extend(true, defaults, this.field);        
		vars={
			   "label":{
					name:"label",
					priority:"view"
			    }   	
			};
		res = this.replaceVariables(defaults, vars);
		return res;
	};

	Build.prototype.replaceVariables = function (defaults, vars){
		for (var property in vars){
            if (typeof vars[property] != 'function'){
                if(vars[property].priority =="variable"){                	
                	defaults[vars[property].name] = this.variable[property];
                }
            }
        }
		return defaults;
	};
	
	PMDynaform.extendNamespace("PMDynaform.core.Build", Build);
	
}());