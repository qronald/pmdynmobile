(function(){
	var TransformJSON = function (settings) {
		this.parentMode = null;
		this.field = null;
		this.json = null;
		this.jsonBuilt = null;
		TransformJSON.prototype.init.call(this, settings);
	};

	TransformJSON.prototype.init = function (settings) {
		var defaults = {
			parentMode: "edit",
			field: {},
			json: {
				text: TransformJSON.prototype.text,
				textarea: TransformJSON.prototype.textArea,
				checkbox: TransformJSON.prototype.checkbox,
				radio: TransformJSON.prototype.radio,
				dropdown: TransformJSON.prototype.dropdown, 
				button: TransformJSON.prototype.button,
				submit: TransformJSON.prototype.submit,
				datetime: TransformJSON.prototype.datetime,
				suggest: TransformJSON.prototype.suggest,
				link: TransformJSON.prototype.link, 
				file: TransformJSON.prototype.file,
				grid: TransformJSON.prototype.grid
			}		
		};

		jQuery.extend(true, defaults, settings);

		this.jsonBuilt = defaults.field;
		this.setParentMode(defaults.parentMode)
			.setField(defaults.field)
			.setJSONFactory(defaults.json)
			.buildJSON();

		return this;
	};
	TransformJSON.prototype.setParentMode = function (mode) {
		this.parentMode = mode;

		return this;
	};
	TransformJSON.prototype.text = function (field){
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: [
            	field.defaultValue || field.value
            ],
            data : field.data
		};
	};
	TransformJSON.prototype.textArea = function (field) {
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: [
            	field.defaultValue || field.value
            ],
            data : field.data
		};
	};
	TransformJSON.prototype.checkbox = function (field) {
		var validOpt = [],
		i;

		for (i=0; i<field.options.length; i+=1) {
			if (field.options[i].selected) {
				if (field.options[i].selected === true) {
					validOpt.push(field.options[i].label);
				}
			}
			
		}
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: validOpt,
            data : field.data
		};
	};
	TransformJSON.prototype.radio = function (field) {
		var validOpt = [],
		i;

		for (i=0; i<field.options.length; i+=1) {
			if (field.defaultValue) {
				if (field.options[i].value.toString() === field.defaultValue.toString()) {
					validOpt.push(field.options[i].label);
				}
			}
			
		}
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: validOpt,
            data : field.data
		};
	};
	TransformJSON.prototype.dropdown = function (field) {
		var validOpt = [],
		i;

		for (i=0; i<field.options.length; i+=1) {
			if (field.defaultValue) {
				if (field.options[i].value.toString() === field.defaultValue.toString()) {
					validOpt.push(field.options[i].label);
				}
			}
		}
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: validOpt,
            data : field.data
		};
	};
	TransformJSON.prototype.button = function (field) {
		var fieldExtended = field;

		return fieldExtended;
	};
	TransformJSON.prototype.submit = function (field) {
		var fieldExtended = field;

		return fieldExtended;
	};
	TransformJSON.prototype.datetime = function (field) {
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: [
            	field.defaultValue || field.value
            ],
            data : field.data
		};
	};
	TransformJSON.prototype.suggest = function (field) {
		var validOpt = [],
		i, aux;

		for (i=0; i<field.options.length; i+=1) {
			if (field.defaultValue) {
				if (field.options[i].value.toString() === field.defaultValue.toString()) {
					validOpt.push(field.options[i].label);
				}
			}
		}
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            fullOptions: validOpt,
            data : field.data
		};
	};
	TransformJSON.prototype.link = function (field) {
		return {
			type: "label",
            colSpan: field.colSpan,
            label: field.label,
            options: [
            	field.value
            ]
		};
	};
	TransformJSON.prototype.file = function (field) {
		var fieldExtended = field;

		return fieldExtended;
	};
	TransformJSON.prototype.grid = function (field) {
		var fieldExtended = field;

		return fieldExtended;
	};
	TransformJSON.prototype.setField = function (field) {
		this.field = field;

		return this;
	};
	TransformJSON.prototype.setJSONFactory = function (factory) {
		this.json = factory;
		return this;
	};
	TransformJSON.prototype.discardViewField = function (type) {
        var disabled = [
            "button",
            "submit",
            "image",
            "label",
            "title",
            "subtitle"
        ];

        return ($.inArray(type, disabled) < 0) ? true : false;
    };
	TransformJSON.prototype.reviewField = function (field) {
		var jsonBuilt = field,
		sigleControl = ["text","suggest","textarea","datetime"], data, i;

		if (this.json[field.type] && this.discardViewField(field.type) ) {
			switch (field.mode) {
				case "disabled":
					jsonBuilt = field;
					jsonBuilt.disabled = true;
				break;
				case "parent":
					field.mode = this.parentMode;
					jsonBuilt = this.reviewField(field);
					//jsonBuilt = this.json[field.type](field);
				break;
				case "view":
					jsonBuilt = this.json[field.type](field);
				break;
				default :
					jsonBuilt = field;
			}
		}
		//if (jsonBuilt.fullOptions)
		jsonBuilt.originalType = field.originalType || field.type;
		jsonBuilt.var_name = field.var_name || "";
		jsonBuilt.var_uid = field.var_uid || "";
		jsonBuilt.options || field.var_accepted_values;
		if (field.data){
           	jsonBuilt.fullOptions = [];
           	if (sigleControl.indexOf(jsonBuilt.originalType) !== -1){
           		if (jsonBuilt.originalType === "suggest"){
					jsonBuilt.fullOptions = [ field.data["label"] || field.defaultValue];           			
           		}else{
					jsonBuilt.fullOptions = [ field.data["value"] || field.defaultValue];
           		}
			} else {
				if (jsonBuilt.originalType === "checkbox" ){
	                data = [];
	                for ( i = 0 ; i < field["options"].length ; i+=1 ) {
	                    if (field.data["value"].indexOf(field.options[i]["value"]) !==- 1){
	                    	data.push(field.options[i]["label"]);
	                    }
	                }
					jsonBuilt.fullOptions = data || [field.defaultValue];
				}else{
					jsonBuilt.fullOptions = [ field.data["label"] || field.defaultValue];
				}
			}
		}
		return jsonBuilt;
	};
	TransformJSON.prototype.buildJSON = function () {
		this.jsonBuilt = this.reviewField(this.field);
		
		return this;
	};
	
	TransformJSON.prototype.getJSON = function () {
		
		return this.jsonBuilt;
	};
	PMDynaform.extendNamespace("PMDynaform.core.TransformJSON", TransformJSON);

}());