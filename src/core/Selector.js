(function(){
	/*
	 * @param {String}
	 * The following key selectors are availables for the
	 * getField and getGridField methods
	 * - Using '#', is possible select a field with the identifier of the field
	 * - Using ''. is possible select a field with the className of the field
	 * - Putting 'attr[name="my-name"]' is possible select fields with the same name attribute
	 *
	 **/
	var Selector = function (options) {
		this.onSupportSelectorFields = null;
		this.fieldType = null;
		this.fields = [];
		this.queries = [];
		this.forms = [];

		Selector.prototype.init.call(this, options);
	};
	Selector.prototype.init = function (options) {
		var defaults = {
			fields: [],
			queries: [],
			forms : [],
			onSupportSelectorFields: {
				text: "onTextField",
				textarea: "onTextAreaField"
			}
		};

		$.extend(true, defaults, options);
		
		this.setOnSupportSelectorFields(defaults.onSupportSelectorFields)
			.setFields(defaults.fields)
			.setForms(defaults.forms)
			.applyGlobalSelectors();
	};
	Selector.prototype.addQuery = function (query) {
		if (typeof query === "string") {
			this.queries.push(query);
		} else {
			throw new Error ("The query selector must be a string");
		}

		return this;
	};
	Selector.prototype.setOnSupportSelectorFields = function (support) {
		if (typeof support === "object") {
			this.onSupportSelectorFields = support;
		} else {
			throw new Error ("The parameter for the support fields is wrong");
		}

		return this;
	};
	Selector.prototype.setFields = function (fields) {
		if (typeof fields === "object") {
			this.fields = fields;
		}

		return this;
	};

	Selector.prototype.setForms = function (forms) {
		if (jQuery.isArray(forms)){
			this.forms = forms;
		}
		return this;
	};

	Selector.prototype.onTextField = function (selector) {
		//console.log("selector text field", selector);

		return this;
	};
	Selector.prototype.onTextAreaField = function (selector) {
		//console.log("selector textarea field", selector);
		
		return this;
	};

	Selector.prototype.findFieldById = function (selectorAttr) {
		var i,
		fieldFinded = null;

		searching:
		for (i=0; i<this.fields.length; i+=1) {
			if (this.fields[i].model.id === selectorAttr) {
				fieldFinded = this.fields[i];
				break searching;
			}
		}
		return fieldFinded;
	};
	Selector.prototype.findFormById = function (selectorId) {
		var i;
		for ( i = 0 ; i < this.forms.length ; i+=1 ) {
			if ( this.forms[i].model.id === selectorId ) {
				return this.forms[i];
			}
		}
		return null;
	},
	Selector.prototype.findFieldByName = function (selectorAttr) {
		var i,
		fieldFinded = [];

		for (i=0; i<this.fields.length; i+=1) {
			if (this.fields[i].model.get("name") === selectorAttr) {
				fieldFinded.push(this.fields[i]);
			}
		}
		return fieldFinded;
	};
	Selector.prototype.findFieldByAttribute = function (parameter, value) {
		var i,
		fieldFinded = [];

		for (i=0; i<this.fields.length; i+=1) {
			if (this.fields[i].model.attributes[parameter]) {
				if (this.fields[i].model.get(parameter) === value) {
					fieldFinded.push(this.fields[i]);
				}
			}
		}
		return fieldFinded;
	};
	Selector.prototype.applyGlobalSelectors = function () {
		var sel,
		i,
		that = this;
		
		window.getFieldByAttribute = function (attr, value) {
			that.addQuery(attr + ": " + value);
			return that.findFieldByAttribute(attr, value);
		};
		window.getFieldById = function (query) {
			that.addQuery("id: " + query);
			return that.findFieldById(query);
		};
		window.getFieldByName = function (query) {
			that.addQuery("name: " + query);
			return that.findFieldByName(query);
		};
		window.getFormById = function (query) {
			that.addQuery("id: " + query);
			return that.findFormById(query);
		}

		return this;
	};

	PMDynaform.extendNamespace("PMDynaform.core.Selector", Selector);
}());