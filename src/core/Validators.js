(function() {
    var Validators = {
        /*
         * 
         * @type type
         */
        requiredText: {
            message: "This field is required",
            fn: function(value) {
                value = value.trim();
                if (value === null || value.length === 0 || /^\s+$/.test(value)) {
                    return false;
                }
                return true;
            }
        },
        /*
         * 
         * @type type
         */
        requiredDropDown: {
            message: "This field is required",
            fn: function(value) {
                value = value.trim();
                if (value === null || value.length === 0 || /^\s+$/.test(value)) {
                    return false;
                }
                return true;
            }
        },
        requiredCheckBox: {
            message: "This field is required",
            fn: function(value) {
                if (typeof value === "number") {
                    var bool = (value > 0)? true: false;    
                } else {
                    bool = false;
                }
                return bool;
            }
        },
        requiredFile: {
            message: "This field File is required",
            fn: function(value) {
                value = value.trim();
                if (value === null || value.length === 0 || /^\s+$/.test(value)) {
                    return false;
                }
                return true;
            }
        },
        requiredRadioGroup: {
            message: "This field is required",
            fn: function(value) {
                if (typeof value === "number") {
                    var bool = (value > 0)? true: false;    
                } else {
                    bool = false;
                }
                return bool;
            }
        },
        integer: {
            message: "Invalid value for the integer field",
            mask: /[\d\.]/i,
            fn: function(n) {
                return (n != "" && !isNaN(n) && Math.round(n) == n);
            }
        },
        float: {
            message: "Invalid value for the float field",
            fn: function(n) {
                return  /^-?\d+\.?\d*$/.test(n);
            }
        },
        string: {
            fn: function(string){
                return true;
            }
        },
        boolean: {
            fn: function(string) {
                return true;
            }
        },
        maxLength: {
            message: "The maximum length are ",
            fn: function(value, maxLength) {
                var maxLen;
                if (typeof maxLength !== "number") {
                    throw new Error ("The parameter maxlength is not a number");
                }
                maxLen = (value.toString().length <= maxLength)? true : false;                
                return maxLen;                                
            }
        },

        /*
         * 
         * @type type
         */
        /*
         * 
         * @type type
         */
        /*
        email: {
            message: "Invalid value for field email",
            fn: function(value) {
                if (!(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value))) {
                    return false;
                }
                return true;
            }
        },*/

        /*
         * 
         * @type type
         */
        
        /*datetime: {
            message: "",
            format: "",
            fn: function(ano, mes, dia) {
                var date = new Date(ano, mes, dia);
                if (!isNaN(date)) {
                    return false;
                }
                return true;
            }
        },*/
        
        /*
         * 
         * @type type
         */
        /*password: {
            message: "",
            fn: function(field) {
                if (!/^(?=.*\d)(?=.*[a-z])\w{8,}/i.test(field.value)) {
                    return false;
                }
                return true;
            }
        },*/
        /*mask: {
            fn: function(value, mask) {
                
            }
        },*/
        /*domain: {
            message: "The value is not valid for the options domain",
            fn: function(value, options) {
                var i, 
                validated = true
                for (i=0; i<options.length; i+=1) {
                    if ( (value !== null) && (options[i].value.toString() === value.toString())){
                        validated = false;
                    }
                }

                return true;
            }
        }*/
    };
    
    PMDynaform.extendNamespace("PMDynaform.core.Validators", Validators);
}());
