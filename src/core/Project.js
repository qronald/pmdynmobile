(function () {
    var Project = function (options) {
        this.model = null;
        this.view = null;
        this.data = null;
        this.fields = null;
        this.keys = null;
        this.token = null;
        this.renderTo = null;
        this.urlFormat = null;
        this.endPointsPath = null;
        this.forms = null;
        this.externalLibs = null;
        this.dependentLibraries = null;
        this.submitRest = null;
        this.onSubmitForm = new Function();
        Project.prototype.init.call(this, options);
    };

    Project.prototype.init = function (options) {
        var defaults = {
            submitRest: false,
            data: {},
            urlFormat: "{server}/{apiName}/{apiVersion}/{workspace}/{keyProject}/{projectId}/{endPointPath}",
            keys: {
                server: "",
                projectId: "",
                workspace: "",
                keyProject: "project",
                apiName: "api",
                apiVersion: "1.0"
            },
            token: {
                accessToken: "",
                clientId: "x-pm-local-client",
                clientSecret: "",
                expiresIn: "",
                refreshToken: "",
                scope: "",
                tokenType: "bearer"
            },
            endPointsPath: {
                project: "",
                createVariable: "process-variable",
                variableList: "process-variable",
                /**
                 * @key {var_uid} Defines the identifier of the variable
                 * The Endpoint is for get all information about Variable
                 **/
                variableInfo: "process-variable/{var_uid}",
                /**
                 * @key {var_name} Defines the variable name
                 * The Endpoint executes the query associated to variable
                 **/
                executeQuery: "process-variable/{var_name}/execute-query",
                /**
                 *
                 * @key {field_name} Defines the field name
                 * The Endpoint uploads a file
                 **/
                uploadFile: "uploadfile/{field_name}",
                executeQuerySuggest: "process-variable/{var_name}/execute-query-suggest"
            },
            externalLibs: "",
            renderTo: document.body,
            onLoad: new Function()
        };

        if (!_.isEmpty(options.data) && options.data.items[0] && options.data.items[0]["externalLibs"]) {
            this.externalLibs = options.data.items[0]["externalLibs"].split(",");
        }
        var that = this;
        //start loading
        $("body").append("<div class='pmDynaformLoading' style='position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url(/lib/img/loading.gif) 50% 50% no-repeat #f9f9f9;'></div>");
        this.setExternalLibreries(this.externalLibs, 0, function () {
            jQuery.extend(true, defaults, options);
            that.setData(defaults.data)
                    .setUrlFormat(defaults.urlFormat)
                    .setKeys(defaults.keys)
                    .setToken(defaults.token)
                    .setRenderTo(defaults.renderTo)
                    .setEndPointsPath(defaults.endPointsPath)
                    .checkDependenciesLibraries();
            that.submitRest = defaults.submitRest;
            defaults.onLoad();
            //stop loading
            $("body").find(".pmDynaformLoading").remove();
        });
    };
    Project.prototype.setExternalLibreries = function (libs, i, fn) {
        var item, type, that = this;
        if (jQuery.isArray(libs) && i < libs.length) {
            item = libs[i].trim();
            type = item.substring(item.lastIndexOf(".") + 1);
            switch (type) {
                case "js" :
                    var script = document.createElement('script');
                    script.onload = function () {
                        that.setExternalLibreries(libs, i + 1, fn);
                    };
                    script.type = 'text/javascript';
                    script.src = item;
                    document.head.appendChild(script);
                    break;
                case "css" :
                    var link = document.createElement("link");
                    link.onload = function () {
                        that.setExternalLibreries(libs, i + 1, fn);
                    };
                    link.rel = "stylesheet";
                    link.href = item;
                    document.head.appendChild(link);
                    break;
            }
        } else {
            fn();
        }
    };
    Project.prototype.setData = function (data) {
        if (typeof data === "object") {
            this.data = data;
        }
        if (this.view) {
            this.destroy();
            this.loadProject();
        }

        return this;
    };
    Project.prototype.setData2 = function (data) {
        this.view.setData2(data);
        return this;
    };
    Project.prototype.setUrlFormat = function (url) {
        if (typeof url === "string") {
            this.urlFormat = url;
        }

        return this;
    };
    Project.prototype.setKeys = function (keys) {
        var keysFixed = {},
                key,
                leftBracket;

        if (typeof keys === "object") {
            for (key in keys) {
                leftBracket = (keys[key][0] === "/") ? keys[key].substring(1) : keys[key];
                keysFixed[key] = (leftBracket[leftBracket.length - 1] === "/") ? leftBracket.substring(0, leftBracket.length - 1) : leftBracket;
            }
            keysFixed.server = keysFixed.server.replace(/\https:\/\//, "").replace(/\http:\/\//, "");
            this.keys = keysFixed;
        }

        return this;
    };
    Project.prototype.setToken = function (objToken) {
        if (typeof objToken === "object") {
            this.token = objToken;
        }

        return this;
    };
    Project.prototype.setRenderTo = function (to) {
        this.renderTo = to;

        return this;
    };
    Project.prototype.setEndPointsPath = function (endpoints) {
        var leftBracket,
                point,
                endpointsVerified = {};

        for (point in endpoints) {
            if (typeof endpoints[point] === "string") {
                leftBracket = (endpoints[point][0] === "/") ? endpoints[point].substring(1) : endpoints[point];
                endpointsVerified[point] = (endpoints[point][endpoints[point].length - 1] === "/") ?
                        endpoints[point].substring(0, endpoints[point].length - 1) :
                        endpoints[point];
            } else {
                throw new Error("The endpoint path is not correct, " + endpoints[point]);
            }
        }
        this.endPointsPath = endpointsVerified;

        return this;
    };
    Project.prototype.checkDependenciesLibraries = function () {
        var i,
                libs = [],
                enableGeoMap = false,
                searchingMap,
                forms = this.data.items;

        searchingMap = function (fields) {
            var j,
                    k,
                    l,
                    dependent = [
                        "geomap",
                        "other"
                    ];

            outer_loop:
                    for (j = 0; j < fields.length; j += 1) {
                for (k = 0; k < fields[j].length; k += 1) {
                    if ($.inArray(fields[j][k].type, dependent) >= 0) {
                        enableGeoMap = true;
                        break outer_loop;
                    } else if (fields[j][k].type === "form") {
                        searchingMap(fields[j][k].items);
                    }
                }
            }
        };

        for (i = 0; i < forms.length; i += 1) {
            searchingMap(forms[i].items);
        }

        if (enableGeoMap) {
            this.loadGeoMapDependencies();
        } else {
            this.loadProject();
        }

        return this;
    };
    Project.prototype.checkScript = function () {
        var i,
                code;

        for (i = 0; i < this.forms.length; i += 1) {
            if (!_.isEmpty(this.forms[i].model.get("script"))) {
                code = new PMDynaform.core.Script({
                    script: this.forms[i].model.get("script").code
                });
                code.render();
            }
        }

    };
    Project.prototype.setAllFields = function (fields) {
        if (typeof fields === "object") {
            this.fields = fields;
            this.selector.setFields(fields);
            //console.log("set fields");
        }
        return this;
    };
    Project.prototype.loadProject = function () {
        var that = this;

        this.model = new PMDynaform.model.Panel(this.data);
        this.view = new PMDynaform.view.Panel({
            tagName: "div",
            renderTo: this.renderTo,
            model: this.model,
            project: this
        });
        this.forms = this.view.getPanels();
        this.createGlobalPmdynaformClass(this.view);
        this.createSelectors();
        this.checkScript();
        this.createMessageLoading();
        that.view.afterRender();
        that.view.$el.find(".pmdynaform-form-message-loading").remove();
        $("#shadow-form").remove();

        return this;
    };
    Project.prototype.createMessageLoading = function () {
        var msgTpl = _.template($('#tpl-loading').html());
        this.view.$el.prepend(msgTpl({
            title: "Loading",
            msg: "Please wait while the data is loading..."
        }));
        this.view.$el.find("#shadow-form").css("height", this.view.$el.height() + "px");
    };
    Project.prototype.createSelectors = function () {
        var i,
                eachForm,
                fields = [];

        eachForm = function (items) {
            var jFields;

            for (jFields = 0; jFields < items.length; jFields += 1) {
                if (items[jFields].model.get("type") === "form") {
                    eachForm(items[jFields].formView.getFields());
                } else {
                    fields.push(items[jFields]);
                }
            }
        };

        //Each Form
        for (i = 0; i < this.forms.length; i += 1) {
            eachForm(this.forms[i].getFields());
        }

        this.fields = fields;
        this.selector = new PMDynaform.core.Selector({
            fields: fields,
            forms: this.forms
        });

        return this;
    };
    Project.prototype.createGlobalPmdynaformClass = function (form) {

    };
    Project.prototype.loadGeoMapDependencies = function () {
        var i,
                auxClass,
                instanceClass,
                that = this,
                loadScript = true,
                libs = "";

        libs = document.body.getElementsByTagName("script");
        outer_script:
                for (i = 0; i < libs.length; i += 1) {
            if ($(libs[i]).data) {
                if ($(libs[i]).data("script") === "google") {
                    loadScript = false;
                    break outer_script;
                }
            }
        }
        if (loadScript) {
            auxClass = function (params) {
                this.project = params.project;
            };
            auxClass.prototype.load = function () {
                this.project.loadProject();
            };
            window.pmd = new auxClass({project: this});
            var script = document.createElement('script');
            script.type = 'text/javascript';
            $(script).data("script", "google");
            ;
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&callback=pmd.load';

            document.body.appendChild(script);
        } else {
            this.loadProject();
        }


        return this;
    };
    Project.prototype.registerKey = function (key, value) {
        if ((typeof key === "string") && (typeof value === "string")) {
            if (!this.keys[key]) {
                this.keys[key] = value;
            } else {
                throw new Error("The key already exists.");
            }
        } else {
            throw new Error("The parameters must be strings.");
        }

        return this;
    };
    Project.prototype.getEndPoint = function (type) {
        return this.endPointsPath[type];
    };
    Project.prototype.setModel = function (model) {
        if (model instanceof Backbone.Model) {
            this.model = model;
        }
        return this;
    };
    Project.prototype.setView = function (view) {
        if (view instanceof Backbone.View) {
            this.view = view;
        }
        return this;
    };
    Project.prototype.getFullURL = function (endpoint) {
        var k,
                keys = this.keys,
                urlFormat = this.urlFormat;

        for (k in keys) {
            if (keys.hasOwnProperty(k)) {
                urlFormat = urlFormat.replace(new RegExp("{" + k + "}", "g"), keys[k]);
                //endPointFixed =endpoint.replace(new RegExp(variable, "g"), keys[variable]);	
            }
        }
        urlFormat = window.location.protocol + "//" + urlFormat.replace(/{endPointPath}/, endpoint);

        return urlFormat;
    };
    Project.prototype.getForms = function () {
        var forms,
                panels = [];

        if (this.view instanceof PMDynaform.view.Panel) {
            forms = this.view.getPanels();
        }

        return forms;
    };
    Project.prototype.getData = function () {
        var formData = this.view.getData();

        return formData;
    };
    Project.prototype.destroy = function () {
        this.view.$el.remove();

        return this;
    };
    PMDynaform.extendNamespace("PMDynaform.core.Project", Project);

}());