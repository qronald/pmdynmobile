var language = {
	"en":{
			"ERROR_NETWORK_FORM_DATA":"Network Error - Error getting form data",
			"ERROR_NETWORK_SUBMIT_NEW": "Network Error - Error submitting a new case",
			"ERROR_NETWORK_SUBMIT_CASE": "Network Error - Error submitting a case",
			"ERROR_NETWORK_ROUTE": "Network Error - Error deriving a case",		
			"ERROR_NETWORK_JSON_FORM": "Network Error - Error in the form definition",			
			"ERROR_NETWORK_TRIGGER": "Network Error - Error in trigger execute",						
			"INFO_PMDYNAFORM_JSON":"There isn't a JSON definition",
			"INFO_PMDYNAFORM_DERIVATED":"Routed successfully",
			"INFO_PMDYNAFORM_SUBMIT":"Submited successfully",
			"INFO_CASE_FINISHED":"Case finished"			
		 }
};