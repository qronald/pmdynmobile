(function(){
	var DropdownModelMobile =  PMDynaform.model.FieldMobile.extend({
		defaults: {
			colSpan: 12,
            dataType: "string",
            defaultValue: "",
            dependenciesField: [],
            disabled: false,
            executeInit: true,
            group: "form",
            hint: "",
            id: PMDynaform.core.Utils.generateID(),
			label: "untitled label",
			localOptions: [],
            mode: "edit",
            name: "name",
            options: [
                {
                    label: "Empty",
                    value: "empty"
                }
            ],
            remoteOptions: [],
            required: false,
            type: "text",
            valid: true,
            validator: null,
            variable: null,
            variable_uid: null,
            variableInfo: {},
            value: ""
		},
		initialize: function() {

            this.set("validator", new PMDynaform.model.Validator());
            this.set("dependenciesField",[]);
            this.setLocalOptions();
            if (this.get("executeInit")) {
                this.reviewRemoteVariable();    
            }
        },
        setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
        setDependencies: function(newDependencie) {
            var arrayDep, i, result, newArray = [];
            arrayDep = this.get("dependenciesField");
            if(arrayDep.indexOf(newDependencie) === -1){
            	arrayDep.push(newDependencie);
            }
            this.set("dependenciesField",[]);
            this.set("dependenciesField",arrayDep);
        },
        isValid: function(){
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        validate: function (attrs) {
        	
    		var valueFixed = attrs.value.trim();
            //this.set("value", valueFixed);
            this.get("validator").set("type", attrs.type);
            this.get("validator").set("required", attrs.required);
            this.get("validator").set("value", valueFixed);
            
            this.get("validator").set("dataType", attrs.dataType);
            this.get("validator").verifyValue();
        	this.isValid();
            return this.get("valid");
        }
	});
	PMDynaform.extendNamespace("PMDynaform.model.DropdownMobile", DropdownModelMobile);

}());