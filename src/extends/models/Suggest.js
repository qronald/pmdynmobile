(function(){

	var SuggestModelMobile = PMDynaform.model.FieldMobile.extend({
		defaults: {
            id: PMDynaform.core.Utils.generateID(),
            type: "text",
            placeholder: "untitled",
            label: "untitled label",
            name: "name",
            colSpan: 12,
            colSpanLabel: 3,
            colSpanControl: 9,
            value: "",
            group: "form",
            defaultValue: "",
            autoComplete: "off",
            maxLengthLabel: 15,
            mode: "edit",
            tooltipLabel: "",
            disabled: false,
            dataType: "string",
            executeInit: true,
            required: false,
            maxLength: null,
            validator: null,
            valid: true,
            proxy: null,
            variable: null,
            variable_uid: null,
            options:[],
            localOptions: [],
            remoteOptions: [],
            dependenciesField: [],
            text:"",
            isSubmit:false
        },
        initialize: function() {
            this.set("validator", new PMDynaform.model.Validator());
            this.initControl();
            this.attributes.dependenciesField = [];
            this.setLocalOptions();
            if (this.get("executeInit")) {
                this.reviewRemoteVariable();    
            }
            
            //this.remoteProxyData();
        },
        initControl: function() {
            if (this.get("defaultValue")) {
                this.set("value", this.get("defaultValue"));
            }
        },
        setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
        isValid: function(){
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        emptyValue: function (){
            //this.set("value","");
            var val,
                arrayOptions,
                item,
                content="";
            
            val = this.get("value");
            arrayOptions = this.get("options");
            //console.log("llegue a este punto");
            for (var i=0;i<arrayOptions.length;i++){
                item = arrayOptions[i];
                if(item.value == val){
                    content = item.label;
                    break;
                }
            }
            this.set("text", content);
        },
        setValue: function (newValue){
           
        },
        setDependencies: function(newDependencie) {
            var arrayDep,i, result, newArray = [];
            arrayDep = this.get("dependenciesField");
            if(arrayDep.indexOf(newDependencie) == -1){
                arrayDep.push(newDependencie);
            }
            this.attributes.dependenciesField = arrayDep;
            
            //this.set("dependenciesField",[]);
            //this.set("dependenciesField",arrayDep);
        },
        validate: function (attrs) {
            var valueFixed = attrs.value.trim()
            this.set("value", valueFixed);
            this.get("validator").set("type", attrs.type);
            this.get("validator").set("required", attrs.required);
            this.get("validator").set("value", valueFixed);
            this.get("validator").set("dataType", attrs.dataType);
            this.get("validator").verifyValue();
            this.isValid();
            return this.get("valid");
        },
        getData: function() {
            return {
                name: this.get("name"),
                value: this.get("value")
            };
        }
    });
    PMDynaform.extendNamespace("PMDynaform.model.SuggestMobile", SuggestModelMobile);
}());