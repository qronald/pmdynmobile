(function(){
	var FieldModelMobile = PMDynaform.model.Field.extend({		
        reviewRemoteVariable: function () {
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint;

            endpoint = this.getEndpointVariable({
                type: "variableInfo",
                keys: {
                    "{var_uid}": this.get("variable") || ""
                }
            });
            url = prj.getFullURL(endpoint);
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'GET',
                data: {},
                keys: prj.token,
                successCallback: function (xhr, response) {
                    that.set("variableInfo", response.data);
                    that.remoteProxyData();
                }
            });
            this.set("proxy", restClient);
            return this;
        },
        remoteProxyData: function (dependentParam) {
            var prj = this.get("project"),
            url,
            varInfo = this.get("variableInfo"),
            data = {}, arrayDep,
            currentOpt, k, i, 
            that = this, 
            restClient, 
            endpoint, 
            variablesSql,
            remote = true;
            
            // If the var_sql parameter is empty, the remote proxy is not going to execute
            // By Default the min length for the string is 5, based in SELECT 
            if (!$.isEmptyObject(varInfo)) {
                
                //if (typeof varInfo.var_sql !== "object") {
                remote = true;
                //}
                
                if ((typeof prj.endPointsPath['executeQuery'] === "string")
                    && (typeof this.attributes.variable === "string")
                    && remote === true) {

                    endpoint = this.getEndpointVariables({
                        type: "executeQuery",
                        keys: {
                            "{var_name}": this.get("variable") || "",
                            "{processID}": prj.keys["processID"]                               
                        }
                    });
                    url = prj.getFullURL(endpoint);

                    arrayDep = this.get("dependenciesField");
                    
                    /*
                    if (varInfo.var_sql.match(/@#[a-zA-Z_]+|@@[a-zA-Z_]+/g)) {
                        variablesSql = varInfo.var_sql.match(/@#[a-zA-Z_]+|@@[a-zA-Z_]+/g).toString().replace(/@@|@#/g,"").split(",");
                        for (i=0; i< variablesSql.length; i+=1) {
                            data[variablesSql[i]] = "";
                        }    
                    }*/
                    
                    for (i=0; i< arrayDep.length; i+=1){
                        view = arrayDep[i];
                        data[view.model.get("variable")] = view.model.get("value");
                    }

                    restClient = new PMDynaform.core.Proxy ({
                        url: url,
                        method: 'POST',
                        data: data,
                        keys: prj.token,
                        successCallback: function (xhr, response) {
                            var k, remoteOpt = [];
                            for (k =0; k< response.data.length; k+=1){
                                remoteOpt.push({
                                    value: response.data[k].value,
                                    label: response.data[k].text
                                })
                            }
                            localOpt = that.get("localOptions");
                            that.set("remoteOptions", remoteOpt);
                            that.set("options", localOpt.concat(remoteOpt));
                        }
                    });
                    this.set("proxy", restClient);
                }
            }
            					
			return this;
		},
        getEndpointVariables: function (urlObj) {
            var prj = this.get("project"),
            endPointFixed,
            variable,
            endpoint;

            if (prj.endPointsPath[urlObj.type]) {
                endpoint = prj.endPointsPath[urlObj.type]
                for (variable in urlObj.keys) {
                    if (urlObj.keys.hasOwnProperty(variable)) {
                        endPointFixed =endpoint.replace(new RegExp(variable, "g"), urlObj.keys[variable]);  
                        endpoint= endPointFixed;
                    }
                }
            }

            return endPointFixed;
        }
	});
	PMDynaform.extendNamespace("PMDynaform.model.FieldMobile", FieldModelMobile);
}());