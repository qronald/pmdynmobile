
(function(){
	var DataLocalManager = function(options) {		
		this.data = null;
		this.dataLocal =null;			
		DataLocalManager.prototype.init.call(this, options);
	};
	
	DataLocalManager.prototype.init = function(options) {
		var defaults = {			
			data: {},
			dataLocal:{}
		};
		$.extend(true, defaults, options);
		this.setData(defaults.data);
		this.setDataLocal(defaults.dataDevice);		
				
	};

	DataLocalManager.prototype.setData = function(data) {
		if(data != null){
			this.data = data;
		}else{
			this.data = {};
		}
		return this;
	};

	DataLocalManager.prototype.getData = function(data) {		
		return this.data;
	};

	DataLocalManager.prototype.getDataLocal = function(data) {		
		return this.dataLocal;
	}

	DataLocalManager.prototype.getDataDevice = function(data) {		
		return this.dataLocal;
	};	


	DataLocalManager.prototype.setDataLocal = function(dataLocal) {
		if(dataLocal != null){
			this.dataLocal = dataLocal;
		}else{
			this.dataLocal = {};
		}
		return this;
	};

	DataLocalManager.prototype.addData = function(data) {
		var sw = false,
			array=[];
		$.extend(this.data,data);
		
		return this;
	};

	DataLocalManager.prototype.addDataLocal = function(data) {
		$.extend(this.dataLocal,data);	
		return this;
	};
	



	PMDynaform.extendNamespace("PMDynaform.core.DataLocalManager", DataLocalManager);
}());