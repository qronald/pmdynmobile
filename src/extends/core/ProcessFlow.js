


(function(){
	var ProjectFlow = function(options) {
		this.project = null;		
		this.sendFormData = null;
 		this.forms = null;
 		this.indexForms = 0;
		this.currentFlow = null;
		this.onLine = true;
		this.isCase = false;				
	};
	/**
	 * [setForms description]
	 * @param {[type]} forms [description]
	 */
	ProjectFlow.prototype.getIndexForms = function(forms) {
		return this.indexForms;		
	};

	ProjectFlow.prototype.setForms = function(forms) {
		this.forms = forms;
		return this;		
	};
	
	ProjectFlow.prototype.initFlow = function(project, onLine) {
		this.project = project;
		this.onLine = onLine;
		return this;		
	};

	ProjectFlow.prototype.setIsCase = function(isCase) {		
		this.isCase = isCase;
		return this;		
	};

	ProjectFlow.prototype.executeTriggerBefore = function() {		
		var project = this.project,
			formsHandler = this.project.formsHandler;

		if(formsHandler.forms[formsHandler.indexForms].triggers.before)	{
			response = project.executeTrigger(formsHandler.forms[formsHandler.indexForms].stepId,'before');
			if(response.state == 'success'){

			}
			if(response.state == 'internetFail'){
			    this.setToastMessage(project.language["ERROR_NETWORK_TRIGGER"]);	
			}
		}
		return this;		
	};

	ProjectFlow.prototype.executeTriggerAfter = function() {		
		var project = this.project,
			response,
			formsHandler = this.project.formsHandler;

		if(formsHandler.forms[formsHandler.indexForms].triggers.after)	{
			response = project.executeTrigger(formsHandler.forms[formsHandler.indexForms].stepId,'after');
			if(response.state == 'success'){

			}
			if(response.state == 'internetFail'){
			    this.setToastMessage(project.language["ERROR_NETWORK_TRIGGER"]);	
			}
		}
		return this;		
	};

	ProjectFlow.prototype.initLoad = function(data) {
		var sw = null, 
			response, 
			jsonForm,
			dataForm,
			allData,
			project = this.project,
			formsHandler = this.project.formsHandler;
			dataManager = this.project.dataManager;

		//create a new case
		if(project.caseID==null){
			this.submitNew();
			this.sendCaseInformation();					
		}
		//execute a triggers if exists
		this.executeTriggerBefore();

		if(this.project.dataForm == null){
			//if(this.isCase){
				allData = project.loadAllDataCase();
				if(allData.state == "success"){										
					dataManager.addData(allData.data);
					dataManager.addDataLocal(allData.data);											
				}else{
					this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);													
					this.internetFailData();
				}
			//}
		}			
		if(data.jsonForm != null){
			this.project.loadProject(data.jsonForm);
			formsHandler.setJsonFormCurrent(data.jsonForm);				
			dataForm = dataManager.getDataLocal();
			if(dataForm != null){
				this.project.setDataViewDelay(dataForm);
			}
			return;				
		}		
		if(data.jsonForm == null){
			jsonForm = formsHandler.getJsonFormCurrent();			
			if(jsonForm != null){
				this.project.loadProject(jsonForm);				
				formsHandler.setJsonFormCurrent(jsonForm);					
				dataForm = dataManager.getDataLocal();
				if(dataForm != null){
					this.project.setDataViewDelay(dataForm);
				}
				return;
			}
		}			
	};	
	
	ProjectFlow.prototype.submitStep = function(data) {		
		var sw = null, 
			response,
			dataLocal,
			formsHandler = this.project.formsHandler,
			dataManager = this.project.dataManager,
			project = this.project,
			indexCurrent;		

		//dataLocal= project.getDataCustom();
		dataManager.addData(data.dataSubmit);
		//dataManager.addDataLocal(dataLocal);
		dataManager.addDataLocal(data.dataSubmit);
			
		if(data.dataSubmit){
			if(this.project.typeList ==null){
				indexCurrent = formsHandler.getIndexForms();					
				if(project.caseID){
					this.submitCase(data);					
				}
				else{
					if(project.caseID==null){
						//this.submitNew();
						this.submitCase(data);
					}
				}
			}else{				
				this.submitCase(data);
			}
		}
		project.showNavBar();				
	};

	ProjectFlow.prototype.submitNew = function() {		
		var sw = null, 
			response, 
			infocase,			
			formsHandler = this.project.formsHandler,
			project =this.project;
			
			infoCase=this.project.startCase();
			if(infoCase.state=="success"){				
				this.project.setCaseID(infoCase.caseID);
				this.project.caseTitle=infoCase.caseTitle;
				this.project.caseNumber=infoCase.caseNumber;										
		   		return;
			}
			if(infoCase.state == "internetFail"){								
			    this.setToastMessage(project.language["ERROR_NETWORK_SUBMIT_NEW"]);													
				this.internetFailData();												
			}

			if(infoCase.state == "unautorized"){				
				this.setToastMessage(project.language["ERROR_NETWORK_SUBMIT_NEW"]);													
				this.internetFailData();				
			}		
		return;				
	};

	ProjectFlow.prototype.submitCase = function(data) {		
			var route,
				dataForm,
				project = this.project,
				formsHandler = this.project.formsHandler;
				
			if(data.dataSubmit){
				caseID=this.project.submitFormCase(data.dataSubmit);
				if(caseID.state=="success"){
					this.executeTriggerAfter();					
					this.sendFormData = data;
					this.project.sendSubmitInformationDevice();
					this.setToastMessage(project.language["INFO_PMDYNAFORM_SUBMIT"]);					
					project.emptyView();
					if(!this.advanceStep()){
						this.submitRoute();	
					}					
				}				
				if(caseID.state == "internetFail"){
					this.setToastMessage(project.language["ERROR_NETWORK_SUBMIT_CASE"]);
					this.internetFailData();
				}					
				if(caseID.state == "unautorized"){
					this.setToastMessage(project.language["ERROR_NETWORK_SUBMIT_CASE"]);
					this.internetFailData();
				}
			}
			return;				
	};


	ProjectFlow.prototype.submitRoute = function(data) {		
			var route, 
				str,
				dataForm,
				project = this.project,
				formsHandler = this.project.formsHandler;				
			route = this.project.routeCase();
			if(route.state == "success"){							
				if(route.data.routing.length >0){
					str = route.data.routing[0].userName;				
					str=project.language["INFO_PMDYNAFORM_DERIVATED"]+"\n Next User:"+str;								
				}else{
					str = route.data.routing.message;				
					str= "Message:"+str;								
				}
				this.setToastMessage(str);								
				this.closeWebView();
		   		return;
			}
			if(route.state == "internetFail"){
				this.setToastMessage(project.language["ERROR_NETWORK_ROUTE"]);
				this.closeWebViewInternetFailed();
			}
			if(route.state == "unautorized"){
				this.setToastMessage(project.language["ERROR_NETWORK_ROUTE"]);
				this.closeWebViewInternetFailed();
			}							
	};	

	ProjectFlow.prototype.loadFormfromJson = function() {
		var response,
			project = this.project;
			responseDevice;
		responseDevice = this.getFormDefinitionDevice();					
		if(responseDevice.state == "success"){
			this.project.loadProject(jsonForm.data);
			this.project.showNavBar();
			return							
		}else{
			response = this.project.getFormDefinition();
			switch(response.state) {
				case "success":
					this.project.loadProject(response.data);
					this.project.showNavBar();    
				    break;
				case "internetFail":
					this.setToastMessage(project.language["INFO_PMDYNAFORM_JSON"]);
					this.closeWebViewInternetFailed();    
				    break;
				case "unautorized":
					this.setToastMessage(project.language["INFO_PMDYNAFORM_JSON"]);					
					this.closeWebViewInternetFailed();    
				    break;    			
				default:
				    break;
			}
		}					
		return;
	};	


	ProjectFlow.prototype.closeWebView = function() {
		switch(this.project.userAgent) {
			case "android":
			    JsInterface.SubmitFinishedCorrectly();
			    break;			
			default:

			    break;
		}   		
	};

	ProjectFlow.prototype.closeWebViewInternetFailed = function() {
		switch(this.project.userAgent) {
			case "android":
			    JsInterface.submitFailedDueNoInternet();
			    break;			
			default:			
			    break;
		}   		
	};	

	ProjectFlow.prototype.closeWebViewDerivated = function() {
		switch(this.project.userAgent) {
			case "android":
			    JsInterface.SubmitFinishedCorrectly();
			    break;			
			default:
			    break;
		}   		
	};

	ProjectFlow.prototype.internetFail= function () {
		switch(this.project.userAgent) {
			case "android":
			    JsInterface.submitFailedDueNoInternet();
			    break;			
			default:
			    break;
		}      		
    };

    ProjectFlow.prototype.internetFailData= function () {
		var data, 
        	json, 
        	resp,
        	prj, 
        	dataManager,
        	formsHandler;
		json = this.project.model.attributes;		
		dataManager = this.project.dataManager;
		prj = this.project;
		dataManager.addDataLocal(prj.getDataCustom());		
		data= dataManager.getDataLocal();
		resp = {
			"json":json,
			"data":data,
			"accessToken":this.project.token.accessToken,
			"refreshToken":this.project.token.refreshToken, 
			"formID":this.project.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){
			       	
			JsInterface.receiveFormData(JSON.stringify(resp));
			JsInterface.submitFailedDueNoInternet();
    	}
		return this;		      		
    };

    ProjectFlow.prototype.getFormDefinitionDevice = function() {
		var response = {
			state:"internetFail"
		};
		switch(this.project.userAgent) {
			case "android":
				if(JsInterface.getFormId(this.formID)==-1){			   								
					response["state"]="failed";
				}
				else{
					response["state"]="success";
					response["data"]= JSON.parse(JsInterface.getFormId(this.formID));
				}
			    break;	
			default:
			    break;
		}
		return response;	
	};

	ProjectFlow.prototype.setToastMessage= function (message) { 
    	switch(navigator.userAgent) {
			case "formslider-android":
			    JsInterface.showToast(message);	
			    break;			
			default:
				//alert(message);
			    break;
		};			
    }; 

 	/**
 	 * [setCaseInformationDevice This methos is valid to send data for caseInformation in Device andriod RFC ]
 	 */
    ProjectFlow.prototype.setCaseInformationDevice= function () {
        var data, 
        	json, 
        	resp,
        	prj, 
        	dataManager,
        	formsHandler;
		json = this.project.model.attributes;		
		dataManager = this.project.dataManager;
		prj = this.project;
		dataManager.addDataLocal(prj.getDataCustom());		
		data= dataManager.getDataLocal();
		resp = {
			"json":json,
			"data":data,
			"accessToken":this.project.token.accessToken,
			"refreshToken":this.project.token.refreshToken, 
			"formID":this.project.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){
			var response ={
			    "caseID" : this.project.caseID,
			    "caseTitle" : this.project.caseTitle,
			    "caseNumber" : this.project.caseNumber
			};        	
			JsInterface.receiveFormData(JSON.stringify(resp));
			JsInterface.receiveCaseInformation(JSON.stringify(response));
    	}
		return this;			
    };

    /**
 	 * [setCaseInformationDevice This methos is valid to send data for caseInformation in Device andriod RFC ]
 	 */
    ProjectFlow.prototype.sendCaseInformation= function () {
        if(navigator.userAgent==="formslider-android"){
			var response ={
			    "caseID" : this.project.caseID,
			    "caseTitle" : this.project.caseTitle,
			    "caseNumber" : this.project.caseNumber
			};        				
			JsInterface.receiveCaseInformation(JSON.stringify(response));			
    	}
		return this;			
    };

    ProjectFlow.prototype.setFormInformationDeviceRFC= function () {
        var data, json, resp, formsHandler;
		json = this.project.model.attributes;
		//data = this.project.getData();
		formsHandler = this.project.formsHandler;
		dataManager = this.project.dataManager;
		
		data= dataManager.getDataLocal();
		resp = {
			"json":json,
			"data":data,
			"accessToken":this.project.token.accessToken,
			"refreshToken":this.project.token.refreshToken, 
			"formID":this.project.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){			        	
			JsInterface.receiveFormData(JSON.stringify(resp));			
    	}
		return this;			
    };


	ProjectFlow.prototype.getJsonFormFromDevice = function(formID) {
		var data,
			response = {
			state:"internetFail"
		};
		switch(navigator.userAgent) {
			case "formslider-android":
				data = JsInterface.getFormId(formID);				
 				if(data==-1){			   								
					response["state"]="failed";
				}
				else{
					response["state"]="success";
					response["data"]= JSON.parse(data);
				}
			    break;		    	
			default:
			    break;
		}
		return response;	
	};

	


	/**
	 * [advanceStep description]
	 * @return {[type]} [description] RFC
	 */
	ProjectFlow.prototype.advanceStep = function() {
		var response=null,
		    formID,
		    jsonForm,
		    allData,
		    dataForm,
		    formsHandler = this.project.formsHandler,
		    dataManager = this.project.dataManager,
		    project= this.project;		
		this.executeTriggerAfter();				
		if(formsHandler.existNextForm()){
			formsHandler.advanceForm();			
			jsonForm = this.getJsonFormRCF();
			if(jsonForm !=null){

				this.executeTriggerBefore();
				project.loadProject(jsonForm);				
				this.setFormInformationDeviceRFC();    
				//dataForm = formsHandler.getDataForm();
				allData = project.loadAllDataCase();
				if(allData.state == "success"){										
					dataManager.addData(allData.data);
					dataManager.addDataLocal(allData.data);											
				}else{
					this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);													
					this.internetFailData();
				}
				dataForm = dataManager.getDataLocal();				
				project.setDataViewDelay(dataForm); 
			}
			return true;
		}else{
			return false;
		}		
	};



	/**
	 * [backStep description]
	 * @return {[type]} [description] RFC
	 */
	ProjectFlow.prototype.backStep = function() {
		var response=null,
		    formID,
		    jsonForm,
		    allData,
		    formsHandler = this.project.formsHandler,
		    dataManager = this.project.dataManager,
		    project= this.project;		    
		if(formsHandler.existPrevForm()){
			formsHandler.backForm();						
			jsonForm = this.getJsonFormRCF();
			if(jsonForm !=null){
				this.executeTriggerBefore();
				project.loadProject(jsonForm);
				//dataForm = formsHandler.getDataForm();
				allData = project.loadAllDataCase();
				if(allData.state == "success"){										
					dataManager.addData(allData.data);
					dataManager.addDataLocal(allData.data);											
				}else{
					this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);													
					this.internetFailData();
				}
				dataForm = dataManager.getDataLocal();
				project.setDataViewDelay(dataForm); 
			}
			return;
		}else{
			return null;
		}
	};


	ProjectFlow.prototype.getJsonFormRCF = function() {
		var formLocal,
			formDevice,
			formNetwork,
			response,
			formsHandler = this.project.formsHandler,
		    project= this.project;		 
		formLocal = formsHandler.getJsonFormCurrentLocal();		
		if(formLocal!=null){			
			response = formLocal;
		}else{			
			formDevice = this.getCurrentJsonFormDeviceRCF();						
			if(formDevice != null){
				response= formDevice;
				formsHandler.setJsonFormIndexCurrent(formDevice);
			}else{
				formNetwork =this.getJsonFormNetworkRCF();				
				response= formNetwork;
			}
		}
		return response;				
	};

	ProjectFlow.prototype.getJsonFormNetworkRCF = function() {
		var formNetwork,
			response,
			formsHandler = this.project.formsHandler,
		    project= this.project;		    
			formNetwork = this.project.getJsonForm(formsHandler.formID);				  
				switch(formNetwork.state) {
					case "success":						
						response= formNetwork.data;
						formsHandler.setJsonFormIndexCurrent(formNetwork.data);						
					    break;
					case "internetFail":
						projectFlow.setToastMessage(project.language["ERROR_NETWORK_JSON_FORM"]);
						projectFlow.closeWebViewInternetFailed();    
					    break;
					case "unautorized":
						projectFlow.setToastMessage(project.language["ERROR_NETWORK_JSON_FORM"]);					
						projectFlow.closeWebViewInternetFailed();    
					    break;    			
					default:
					    break;
				}		
		return response;				
	};

	ProjectFlow.prototype.getCurrentJsonFormDeviceRCF = function() {
		var data,
			formID,
			response = null;
		formID = this.project.formsHandler.formID;
		switch(navigator.userAgent) {
			case "formslider-android":
				data = JsInterface.getFormId(formID);				
 				if(data==-1){			   								
					response = null;
				}
				else{					
					response= JSON.parse(data);
				}
			    break;		    	
			default:
			    break;
		}		
		return response;	
	};

	ProjectFlow.prototype.redirectURL = function(data) {		
		/*if(navigator.userAgent != "formslider-android" && navigator.userAgent != "formslider-ios"){
			var stringOut='<table class="table table-bordered table-striped">'+
		      '<thead>'+
		      '<tr>'+
		        '<th>#</th>'+
		        '<th>Previous</th>'+
		        '<th>Bpmn Type</th>'+
		        '<th>Bpmn Name</th>'+
		        '<th>User</th>'+
		        '<th>Thread</th>'+
		        '<th>Status</th>'+
		        '<th>Route Date</th>'+
		        '<th>Due Date</th>'+
		      '</tr>'+
		      '</thead>'+
		      '<tbody>';
		    for(var i=0; i< data.length; i++){
		    	stringOut+='<tr>'+
		          '<td>{{index}}</td>'
		          '<td>'+data.flow['previous']+'</td>'+
		          '<td>'+data.flow['bpmn_element']['type']+'</td>'+
		          '<td>'+data.flow['bpmn_element']['name']+'</td>'+
		          '<td>'+data.flow['user']['fullName']+'</td>'+
		          '<td>'+data.flow['thread_index'] +'</td>'+
		          '<td>'+data.flow['flow_status'] +'</td>'+
		          '<td>'+data.flow['route_date'] +'</td>'+
		          '<td>'+data.flow['due_date'] +'</td>'+
		        '</tr>';	
		    }
		    stringOut+='</tbody></table>';
		    this.project.emptyView();
		    this.view.$el.append(stringOut);	
		}		*/
		return this;	
	};

	PMDynaform.extendNamespace("PMDynaform.core.ProjectFlow", ProjectFlow);
}());