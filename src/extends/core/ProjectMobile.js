


(function(){
	var ProjectMobile = function(options) {
		this.projectFlow = null;
		this.dataManager = new PMDynaform.core.DataLocalManager();
		this.formsHandler = null;
		this.id = null;
		this.buttonNext= null;
		this.buttonPrev= null;
		this.navBar= null;
		this.model = null;
		this.view = null;
		this.data = null;
		this.dataForm = null;
		this.formID = null;
		this.taskID = null;
		this.caseID = null;
		this.caseFakeID = null;		
		this.workspace = null;				
		this.tokens = null;
		this.restClient = null;
		this.server = null;
		this.submitRest = false;
		this.typeList = null;
		this.onLine = false;
		this.caseTitle = null;
		this.caseNumber = null;
		this.proxy = null;
		this.keys = null;
		this.message = null;
		this.userAgent = null;
		this.currentFormID = null;
		this.dynaforms = [];
		this.container= null;
		this.viewfields =[];
		this.memoryStack = [];
		this.isCase = false;
		this.delayDataView = null;
		this.blockButtonSubmit = true;		
		options.keys= {
				server: options.server,
				processID: options.processID,
				taskID:options.taskID,
				caseID:options.caseID,
				workspace: options.workspace,
				formID: options.formID,
				keyProject : "project",
				typeList : options.typeList,
				stepID : ''
		};
		options.urlFormat = "{server}/api/1.0/{workspace}/{endPointPath}";
		this.urlFormatStreaming = "{server}/sys{workspace}/{endPointPath}";		
		options.endPointsPath ={			
			dynaformDefinition : "light/project/dynaform/{formID}",
			jsonDynaforms : "light/project/dynaforms",			
			startCase :"light/process/{processID}/task/{taskID}/start-case",
			newTokens :"oauth/token",
			caseTypeList :"case/{caseID}/dynaform/{typeList}",
			loadDynaform :"case/{caseID}/dynaform/{formID}/data",
			getFormData :"case/{caseID}/dynaform/{formID}/data",
			getAllDataCase :"case/{caseID}/variables",									
			submitFormCase :"cases/{caseID}/variable", 
			routeCase :"light/cases/{caseID}/route-case",
			createVariable: "process-variable",
			imageInfo: "light/case/{caseID}/download64",
			fileDownload: "case/{caseID}/file/{fileID}",			
			variableList: "process-variable",
			getImageGeo: "light/case/{caseID}/download64",
			imageDownload: 'light/case/{caseID}/download64',			
			generateImageGeo:"/light/case/{caseID}/input-document/location",
			variableInfo: "process-variable/{var_uid}",
			executeQuery: "process/{processID}/process-variable/{var_name}/execute-query",
			uploadFile: "",
			refreshToken: "oauth/token",
			fileStreaming : "en/neoclassic/cases/casesStreamingFile?actionAjax=streaming&a={caseID}&d={fileId}",
			executeTrigger: "light/process/{processID}/task/{taskID}/case/{caseID}/step/{stepID}/execute-trigger/{triggerOption}"
		};
		ProjectMobile.superclass.call(this, options); 		
		ProjectMobile.prototype.init.call(this, options);
	};

	PMDynaform.inheritFrom('PMDynaform.core.Project', ProjectMobile);
	ProjectMobile.prototype.init = function(options) {
		
		var that =this;
			defaults = {
			name: "",
			data: [],
			onSubmitForm: function(data){				
				that.executeSubmit(that.view.getData2());
			}
		};
		this.viewfields = [];
		$.extend(true, defaults, options);
		this.setServer(defaults.server);
		this.setProcessID(defaults.processID);
		this.setWorkspace(defaults.workspace);		
		this.setTaskID(defaults.taskID);
		this.setCaseFakeID(defaults.caseFakeID);		
		this.setCaseID(defaults.caseID);
		this.setFormID(defaults.formID);
		this.setData(defaults.data);
		this.setDataForm(defaults.dataForm);						
		this.setTypeList(defaults.typeList);				
	    this.setSubmitRest(defaults.submitRest);	    
		this.setOnLine(defaults.onLine);
		this.setDynaforms(defaults.dynaforms);
		this.setContainer(defaults.container);
		this.setLanguage();
		this.setOnSubmitForm(defaults.onSubmitForm);
		//this.loadMask = new PMDynaform.view.LoadMask();		
	};
	/**
	 * [setTokens description]
	 * @param {[type]} tokens [description]
	 */
	ProjectMobile.prototype.setTokens = function(tokens) {
		this.tokens = tokens;
		return this;
	};

	/**
	 * [setOnSubmitForm description]
	 * @param {[type]} func [description]
	 */
	ProjectMobile.prototype.setOnSubmitForm = function(func) {
		this.onSubmitForm = func;
		return this;
	};	

	/**
	 * [setIsCase description]
	 * @param {[type]} value [description]
	 */
	ProjectMobile.prototype.setIsCase = function(value) {
		if(typeof value === "boolean"){
			this.isCase = value;
		}
		return this;
	};
	/**
	 * [setCaseFakeID description]
	 * @param {[type]} caseFake [description]
	 */
	ProjectMobile.prototype.setCaseFakeID = function(caseFake) {
		if(typeof caseFake === "string"){
			this.caseFakeID = caseFake;
		}
		return this;
	};

	ProjectMobile.prototype.setKeys = function (keys) {
		var keysFixed = {},
			key,
			leftBracket;
		if (typeof keys === "object") {
			for (key in keys) {
				leftBracket = (keys[key][0]==="/")? keys[key].substring(1) : keys[key];
            	keysFixed[key] = (leftBracket[leftBracket.length-1]==="/")? leftBracket.substring(0, leftBracket.length-1) : leftBracket;
			}
			//keysFixed.server = keysFixed.server.replace(/\https:\/\//,"").replace(/\http:\/\//,"");
			this.keys = keysFixed;
		}
		return this;
	};

	ProjectMobile.prototype.setContainer = function(cont) {
		this.container = cont;
		return this;
	};

	ProjectMobile.prototype.setDynaforms = function(dyns) {
		this.dynaforms = dyns;	
		return this;
	};

	ProjectMobile.prototype.checkDependenciesLibraries = function () {
		var i,
		libs = [],
		enableGeoMap =  false,
		searchingMap,
		forms = this.data!=null?this.data.items:null;
		/*
		searchingMap = function (fields) {
			var j,
			k,
			l,
			dependent = [
				"geomap",
				"other"
			];

			outer_loop:
			for (j=0; j<fields.length; j+=1) {
				for (k=0; k<fields[j].length; k+=1) {
					if ($.inArray(fields[j][k].type, dependent) >= 0) {
						enableGeoMap = true;
						break outer_loop;
					} else if (fields[j][k].type === "subform") {
						searchingMap(fields[j][k].items);
					}
				}
			}
		};
		
		for (i=0; i<forms.length; i+=1) {
			searchingMap(forms[i].items);
		}

		if (enableGeoMap) {
			this.loadGeoMapDependencies();
		} else {
			this.loadProject();
		}*/

		return this;
	};

	ProjectMobile.prototype.setUserAgent = function() {
		if(navigator.userAgent==="formslider-android")					
			this.userAgent= "android";		
		if(navigator.userAgent==="formslider-ios")					
			this.userAgent = "ios";
	};

	/**
	 * [setData set the json form]
	 * @param {[JSON]} data [json form]
	 */
	ProjectMobile.prototype.setData = function(data) {
		if (typeof data === "object") {
			this.data = data;
		}
		return this;
	};

	ProjectMobile.prototype.setDataForm = function(dataForm) {
		if (typeof dataForm === "object") {
			this.dataForm = dataForm;
		}
		return this;
	};	

	/**
	 * [setProcessID set the processID of a project]
	 * @param {[String]} procID [description]
	 */
	ProjectMobile.prototype.setProcessID = function(procID) {
			this.processID = procID;
		return this;
	};
	/**
	 * [setOnLine verify if exists conection to internet]
	 * @param {[boolean]} onLine [description]
	 */
	ProjectMobile.prototype.setOnLine = function(onLine) {
			this.onLine = onLine;
		return this;
	};
	/**
	 * [setWorkspace is a workspace of a project]
	 * @param {[string]} workspace [description]
	 */
	ProjectMobile.prototype.setWorkspace = function(workspace) {
			this.workspace = workspace;
		return this;
	};	
	/**
	 * [setTaskID function set the taskID]
	 * @param {[type]} taskID [description]
	 */
	ProjectMobile.prototype.setTaskID = function(taskID) {
			this.taskID = taskID;
		return this;
	};
	/**
	 * [setCaseID function set the caseID]
	 * @param {[type]} caseID [description]
	 */
	ProjectMobile.prototype.setCaseID = function(caseID) {
		if(caseID){	
			this.caseID = caseID;
		}
		return this;
	};
	
	/**
	 * [setTypeList function set the typeList]
	 * @param {[type]} typeList [description]
	 */
	ProjectMobile.prototype.setTypeList = function(typeList) {
			this.typeList = typeList;
		return this;
	};
	/**
	 * [setServer function set the server]
	 * @param {[type]} server [description]
	 */
	ProjectMobile.prototype.setServer = function(server) {
		if(server)
			this.server = server;
		return this;
	};
	
	ProjectMobile.prototype.setSubmitRest = function(submit) {
		if(submit){
			if (typeof submit === "boolean") {
				this.submitRest = submit;
			}
		}
		return this;
	};

	ProjectMobile.prototype.setLanguage = function(lan) {
		if(lan){			
			this.language = language[lan];
		}else{
			this.language = language["en"];
		}
		return this;
	};	

	ProjectMobile.prototype.setModel = function(model) {
		if (model instanceof Backbone.Model) {
			this.model = model;
		}
		return this;
	};

	ProjectMobile.prototype.setFormID = function(formID) {
		this.formID = formID;
		this.keys.formID = formID;		
		return this;
	};

	ProjectMobile.prototype.setKey = function(name, value) {
		this.keys[name] = value;		
		return this;
	};

	ProjectMobile.prototype.setCaseID = function(caseID) {
		this.caseID = caseID;
		this.keys.caseID = caseID;		
		return this;
	};

	ProjectMobile.prototype.setView = function(view) {
		if (view instanceof Backbone.View) {
			this.view = view;
		}
		return this;
	};	

	ProjectMobile.prototype.setDataViewDelay = function (data) {
    	var that = this,
    		formData;
    	fomrData=this.unEscapeData(data);
    	this.delayDataView = setTimeout(function (){
    		that.view.setData2(fomrData);
    	},2000);	    	
    };

    
	ProjectMobile.prototype.setOptionsToField = function(field,data) {
		var i,j;
		options =field.attributes.options;
		splitData = data["value"].split(",");
	    for (j=0; j<options.length; j++){
	    	options[j].selected = false;  		
	    }
	    for (i=0; i<splitData.length; i++){
	    	for (j=0; j<options.length; j++){	    		
	    		if(options[j].value == splitData[i]){
	    			options[j].selected = true;
	    		}
	    	}
	    }	   
	};
	

	ProjectMobile.prototype.setProxy = function(keys) {
		this.keys = keys;	    	
    	this.restClient = new PMDynaform.core.Proxy({				
            method: 'POST',
            keys:{
            	processId: this.processID,
                workspace: this.workspace,
                token: this.tokens                
            },				
            server:this.server            
        });    
	};
	
	ProjectMobile.prototype.loadNewCase = function() {
		this.setUserAgent();
		this.setIsCase(false);		
		this.formsHandler = new PMDynaform.core.FormsHandler();
		if(this.userAgent == "ios"){
			this.projectFlow = new PMDynaform.core.ProjectFlowIOS();
		}else{
			this.projectFlow = new PMDynaform.core.ProjectFlow();
		}

		this.formsHandler.init(this, this.dynaforms, null, this.formID);
		this.projectFlow.setIsCase(false);
		
		if(this.data){
			this.formsHandler.setJsonFormIndexCurrent(this.data);
		}
		if(this.dataForm){
			this.dataManager.addData(this.dataForm);
			this.dataManager.addDataLocal(this.dataForm);			
		}
		this.projectFlow.setForms(this.dynaforms);
		this.projectFlow.initFlow(this, true);
		this.projectFlow.initLoad({
			jsonForm:this.data
		});
		adjustHeight();
		this.initNavBar();
		this.showNavBar();		
	};

	ProjectMobile.prototype.loadCase = function() {
		this.setUserAgent();
		this.setIsCase(true);		
		this.formsHandler = new PMDynaform.core.FormsHandler();
		if(this.userAgent == "ios"){
			this.projectFlow = new PMDynaform.core.ProjectFlowIOS();
		}else{
			this.projectFlow = new PMDynaform.core.ProjectFlow();
		}

		this.formsHandler.init(this, this.dynaforms, null, this.formID);
		if(this.dataForm){
			this.dataManager.addData(this.dataForm);
			this.dataManager.addDataLocal(this.dataForm);			
		}
		this.projectFlow.setIsCase(true);
		this.formsHandler.setJsonFormIndexCurrent(this.data);
		this.projectFlow.setForms(this.dynaforms);
		this.projectFlow.initFlow(this, true);
		this.projectFlow.initLoad({
			jsonForm:this.data
		});
		this.initNavBar();
		this.showNavBar();
	};

	ProjectMobile.prototype.getFormDefinition = function() {
		var restClient, 
			endpoint, 
			url, 
			that=this, 
			resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.dynaformDefinition);
        url = this.getFullURL(endpoint);
		restClient = new PMDynaform.core.Proxy ({
	            url: url,
	            method: 'GET',                    
	            keys: this.token,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"data":response.data.formContent,
	            		"state":"success"
	            	}  	                
	            },
	            failureCallback: function (xhr, response) {	            	            	
	            	that.failureResponse(response);
	           	}
	        });                		
		return resp;
	};	

	ProjectMobile.prototype.getJsonForm = function(formID) {
		var restClient, 
			endpoint, 
			url, 
			that=this,
			sendData=[], 
			resp;
		this.setFormID(formID);
		endpoint = this.getFullEndPoint(this.endPointsPath.jsonDynaforms);
		sendData.push(formID);
        url = this.getFullURL(endpoint);
		restClient = new PMDynaform.core.Proxy ({
	            url: url,
	            method: 'POST',                    
	            keys: this.token,
	            data:{
	            	formId:sendData
	            },
	            successCallback: function (xhr, response) {
	            	var respData = null;
	            	if(response.length != 0){
	            		respData = response[0].formContent;
	            	}	            	
	            	resp = {
	            		"data":respData,
	            		"state":"success"
	            	}  	                
	            },
	            failureCallback: function (xhr, response) {
	            	resp = {	            	
	            		"state":"internetFail"
	            	}
	           	}
	        });                		
		return resp;
	};

	ProjectMobile.prototype.executeTrigger = function(stepID , triggerOption) {
		var restClient, 
			endpoint, 
			url, 
			that=this,			
			resp;
		
		this.setKey('stepID', stepID);
		this.setKey('triggerOption', triggerOption);		
		endpoint = this.getFullEndPoint(this.endPointsPath.executeTrigger);
		url = this.getFullURL(endpoint);
		restClient = new PMDynaform.core.Proxy ({
	            url: url,
	            method: 'POST',                    
	            keys: this.token,
	            data:null,
	            successCallback: function (xhr, response) {	            	
	            	resp = {
	            		"state":"success"
	            	} 	                
	            },
	            failureCallback: function (xhr, response) {	            	
	            	resp = {	            	
	            		"state":"internetFail"
	            	}
	           	}
	        });                		
		return resp;
	};

	ProjectMobile.prototype.getFullEndPoint= function (urlEndpoint) {
        var k, 
		keys  = this.keys,
		urlFormat = urlEndpoint;
		for (k in keys) {
			if (keys.hasOwnProperty(k)) {
				urlFormat = urlFormat.replace(new RegExp("{"+ k +"}" ,"g"), keys[k]);				
			}
		}
		return urlFormat;
    };

    ProjectMobile.prototype.sendSubmitInformationDevice= function () {
         var data, json, resp;
		json = this.model.attributes;
		data = this.getEscapeData();
		resp = {
			"json":json,
			"data":data,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){			        	
			JsInterface.receiveFormData(JSON.stringify(resp));			
    	}
    	if(navigator.userAgent==="formslider-ios"){							
			this.executeFakeIOS("submit-nextform");						
		}			
    };

    ProjectMobile.prototype.getCaseDataIOS= function () {
        var data, json, resp, bag;
		json = this.model.attributes;
		data = this.getEscapeData();				
		resp = {
			"json":json,
			"data":data,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID
		};
		return JSON.stringify(resp);					
    };

     ProjectMobile.prototype.sendDataForm= function (submitData) {
         var data, json, resp;
		json = this.model.attributes;
		data = submitData;
		resp = {
			"json":json,
			"data":data.fields,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){			        	
			JsInterface.receiveFormData(JSON.stringify(resp));			
    	}				
    };

    ProjectMobile.prototype.getCaseInformation= function () {      
		var resp ={};
		resp["caseID"] = this.caseID;
		resp["caseTitle"] = this.caseTitle;
		resp["ticketNumber"] = this.caseNumber;
		return JSON.stringify(resp);			
    };

    ProjectMobile.prototype.getCaseData= function () {
        var data, json, resp, bag;
		json = this.model.attributes;
		data = this.getEscapeData();		
		resp = {
			"json":json,
			"data":data,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID
		};
		JsInterface.receiveFormData(JSON.stringify(resp));					
    };

	ProjectMobile.prototype.getFormDefinitionTypeList = function() {
		var restClient, endpoint, url, that=this;
		endpoint = this.getFullEndPoint(this.endPointsPath.caseTypeList);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
                that.formID = response.data[0]._id;
                return {
	            		"data":response.data.formContent,
	            		"state":"success",
	            		
	            	};             
            },
            failureCallback : function (xhr, response){
            	
			}
        });
	};

	ProjectMobile.prototype.loadFormData = function(id) {
		var restClient, endpoint, url, that=this, resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.getFormData);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
            	resp = {
            		"state":"success",
            		"data":response.data
            	};                
            },
            failureCallback : function (xhr, response){
            	resp = {
            		"state":"internetFail"
            	};	
			}
        });
		return resp;
	};

	ProjectMobile.prototype.loadAllDataCase = function() {
		var restClient, endpoint, url, that=this, resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.getAllDataCase);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
            	resp = {
            		"state":"success",
            		"data":response
            	};            	            
            },
            failureCallback : function (xhr, response){
            	resp = {
            		"state":"internetFail"
            	};	
			}
        });
		return resp;
	};

	ProjectMobile.prototype.loadProject = function(data) {
		var cont,
		    that = this;
		if(this.container !=null)
			cont=$('#'+this.container);
		else
			cont= $('#container');
		/*if(data){		
			this.model = new PMDynaform.model.Form(data);
			this.view = new PMDynaform.view.FormMobile({
					tagName: "div",
					renderTo: cont,
					model: this.model,
					project: this
				});
		}*/

		this.model = new PMDynaform.model.Panel(data);
		this.view = new PMDynaform.view.Panel({
			tagName: "div",
			renderTo: cont,
			model: this.model,
			project: this
		});
		this.forms = this.view.getPanels();
		this.createGlobalPmdynaformClass(this.view);
		this.createSelectors();
		this.checkScript();
		//this.createMessageLoading();

		that.view.afterRender();
		//that.view.$el.find(".pmdynaform-form-message-loading").remove();
		//$("#shadow-form").remove();
		this.hideMaskLoading();
		return this;
	};

	ProjectMobile.prototype.getRestClient = function (endPoint) {
		return this.restClient;		
	};	

	ProjectMobile.prototype.hideMaskLoading = function () {
		$(".pmdynaform-mobile-loading").hide();
		return this;		
	};	

	ProjectMobile.prototype.executeSubmit = function(data) {
		if(this.blockButtonSubmit){
			this.projectFlow.submitStep({
				dataSubmit:data
			});		
			adjustHeight();
		}					
	};

	ProjectMobile.prototype.startCase = function() {
		var dataSend={},
			that = this,
			i,
			restClient, 
			endpoint, 
			url,
			arrayDep, 
			self=this, 
			resp;

		endpoint = that.getFullEndPoint(that.endPointsPath.startCase);
		url = that.getFullURL(endpoint);        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data: {},
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"caseID":response.caseId,
	            		"caseTitle":response.caseNumber,
	            		"caseNumber":response.caseNumber	            		
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });

		this.caseFakeID = undefined;
	    return resp;					
	};

	ProjectMobile.prototype.submitFormCase = function(data) {
		var dataSend={},
			that = this,
			i,
			restClient, 
			endpoint, 
			url,
			arrayDep, 
			self=this,
			submitData, 
			resp;

		/*for(i=0;i<data.length;i++){
			if(data[i].value){
				dataSend[data[i].name]=data[i].value;
			}
		}*/

		endpoint = that.getFullEndPoint(that.endPointsPath.submitFormCase);
		url = that.getFullURL(endpoint);	        
	       
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'UPDATE',                    
	            keys: that.token,
	            data:data,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success"
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });
	    return resp;					
	};

	ProjectMobile.prototype.routeCase = function() {
		var dataSend={},
			that = this,
			i,
			restClient, 
			endpoint, 
			url,
			arrayDep, 			
			resp;
		
		endpoint = that.getFullEndPoint(that.endPointsPath.routeCase);
        url = that.getFullURL(endpoint);	        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'UPDATE',                    
	            keys: that.token,
	            data: {},
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"data":response	            		
	            	};
	            },
	            failureCallback: function (xhr, response) {            							
					resp= {	            		
	            		"state":"internetFail"
	            	};										 					
	           	}
	        });
	    return resp;					
	};

	ProjectMobile.prototype.refreshToken = function() {
		var restClient, dataSend={},
			self = this;
		dataSend = {
			"grant_type" : "refresh_token",
			"refresh_token" : this.token.refreshToken,
			"access_token" : this.token.accessToken  		
		};
		url = this.getFullURL(this.endPointsPath.refreshToken);
	    restClient = new PMDynaform.core.Proxy ({
	        url: url,
            method: 'POST',                    
            keys: this.token,
            data:dataSend,
            successCallback: function (xhr, response) {
            	self.token.refreshToken=response.access_token;                
            },
            failureCallback: function (xhr, response) {
            	console.log("Failure response resfres Token");
           	}
        });        		
		return this;    	
	};

	ProjectMobile.prototype.failureResponse = function(response){
		var self = this;
		if(response.error.code == 401 ){
			self.refreshToken();
		}	
		return this; 		  	
	};

    ProjectMobile.prototype.emptyView = function() {		
		this.view.$el.remove();
		this.viewfields =[];
		this.memoryStack = [];
		return this;
	};

	ProjectMobile.prototype.getFullURL = function (endpoint) {
		var k, 
		keys  = this.keys,
		urlFormat = this.urlFormat;
		urlFormat=urlFormat.replace(/{endPointPath}/, endpoint);
		for (k in keys) {
			if (keys.hasOwnProperty(k)) {							
					urlFormat = urlFormat.replace(new RegExp("{"+ k +"}" ,"g"), keys[k]);				
			}
		}
		return urlFormat;
	};
	
	ProjectMobile.prototype.getFullURLStreaming = function (endpoint) {
		var k, 
		keys  = this.keys,
		urlFormat = this.urlFormatStreaming;
		urlFormat=urlFormat.replace(/{endPointPath}/, endpoint);
		for (k in keys) {
			if (keys.hasOwnProperty(k)) {							
					urlFormat = urlFormat.replace(new RegExp("{"+ k +"}" ,"g"), keys[k]);				
			}
		}
		return urlFormat;
	};





    ProjectMobile.prototype.getToastMessageIOS= function () {     		
		return this.projectFlow.message;			
    };

    ProjectMobile.prototype.showNavBar= function () {     		
		var index,
			arrayLength,
			swPrev = false, swNext=false;	
		index = this.formsHandler.indexForms;
		arrayLength = this.dynaforms.length;
		if(index >0){
			this.buttonPrev.show();
			swPrev= true;		
		}else{
			this.buttonPrev.hide();
			swPrev= false;
		}
		if(index<arrayLength-1){
			this.buttonNext.show();
			swNext= true;
		}else{
			this.buttonNext.hide();
			swNext= false;
		}

		if(swNext || swPrev){
			$(this.navBar).show();
		}
		else{
			$(this.navBar).hide();
		}
    };

    /**
     * [initButtonNav creation of button of navigation]
     * @return {[type]} [description]
     */
    ProjectMobile.prototype.initNavBar= function () { 
    	var buttonPrev,
    		buttonNext,
    		navBar;    		;
    	navBar = document.createElement("div");
		navBar.style.cssText = "float:right; margin:1%;";
		this.navBar = navBar;
		buttonPrev = $("<button>");
		buttonPrev.addClass("btn btn-info");
		buttonPrev.append("Previous Step");
		this.buttonPrev = buttonPrev;
		this.buttonPrev.click(this.backStep());
		buttonNext = $("<button>");
		buttonNext.addClass("btn btn-info");
		buttonNext.append("Next Step");
		this.buttonNext = buttonNext;
		this.buttonNext.click(this.advanceStep());				

		$(navBar).append(buttonPrev);
		$(navBar).append(buttonNext);
		$("#container").prepend($(navBar));
		return this;		
    };

    /**
     * [advanceNextStep for project]
     * @return {[type]} [description]
     */
    ProjectMobile.prototype.advanceStep= function () { 
    	var that = this;
    	return function (){
    		var formsHandler = that.formsHandler,
    			dataManager = that.dataManager,
    			projectFlow = that.projectFlow,    		
    			data = that.getEscapeData();    			  			
    		
    		dataManager.addData(data);
    		dataManager.addDataLocal(data);	
    		that.emptyView();     		  		
    		projectFlow.advanceStep();    		
    		that.showNavBar();
    		adjustHeight();
    	};    	
    };
	
    ProjectMobile.prototype.backStep= function () { 
    	var that = this;
    	return function (){
    		var formsHandler = that.formsHandler,
    			dataManager = that.dataManager,
    			projectFlow = that.projectFlow,
    			data = that.getEscapeData();     			
    		
    		dataManager.addData(data);
    		dataManager.addDataLocal(data);    		
    		that.emptyView();
    		projectFlow.backStep();    		
    		that.showNavBar();    		
    		adjustHeight();
    	};
    };


    ProjectMobile.prototype.addViewFields= function (viewfield) { 
    	this.viewfields.push(viewfield);
    	return this;    	
    };

    ProjectMobile.prototype.getData = function () {
		var formData = this.view.getData();
		return formData;
	};

	ProjectMobile.prototype.getEscapeData = function () {
		var formData = this.view.getData2();
		for (name in formData) {		
			formData[name] =  (typeof formData[name]=== "string")?escape(formData[name]):formData[name]; 
		}
		return formData;
	};

	ProjectMobile.prototype.unEscapeData = function (data) {
		var formData = data;
		for (name in formData) {		
			formData[name] =  (typeof formData[name]=== "string")?unescape(formData[name]):formData[name]; 
		}
		return formData;
	};

    ProjectMobile.prototype.getDataCustom= function () {    	
    	var fieldsLength = this.viewfields.length,    		    		    		
    		field,
    		data=[],
    		dataSend={},
    		op={},
    		dataField;
    	for(var i=0;i<fieldsLength;i++){
    		field = this.viewfields[i];
    		if(field.model.get("type")=="file"){
    			data.push(field.model.getDataComplete());	
    		}else
    		if(field.model.get("type")=="signature"){
    			data.push(field.model.getDataCustom());	
    		}else
    		if(field.model.get("type")=="datetime" || field.model.get("type")=="time" ||field.model.get("type")=="date"){
    			op={
    				name:field.model.get("name"),
    				value: field.$el.find("input").val()
    			}
    			data.push(op);	
    		}else{
    			if(field.model.getData){
    				dataField=field.model.getData();
    				if(dataField.value)
    					dataField.value =  (typeof dataField.value=== "string")?escape(dataField.value):dataField.value; 
    				data.push(field.model.getData());
    			}
    		}	
    	}
    	for(i=0;i<data.length;i++){
			if(data[i].value){
				dataSend[data[i].name]=data[i].value;
			}
		}
    	return dataSend;    	  	    	
    };

    ProjectMobile.prototype.setFiles= function (input) {    	
    	var parseInput = JSON.parse(input),
    		fieldsLength = this.viewfields.length,
    		item;    		    		
    	for(var i=0;i<fieldsLength;i++){
    		item = this.viewfields[i];
    		if(item.model.get("name") == parseInput.idField){
    			item.setFiles(parseInput.files);
    		}
    	}    	  	    	
    };

    ProjectMobile.prototype.setLocation= function (input) {    	
    	var parseInput = JSON.parse(input),
    		fieldsLength = this.viewfields.length,
    		item;    		    	
    	for(var i=0;i<fieldsLength;i++){
    		item = this.viewfields[i];
    		if(item.model.get("name") == parseInput.idField){
    			item.setLocation(parseInput);
    		}
    	}    	  	    	
    };

    ProjectMobile.prototype.setScannerCode= function (input) {    	
    	var parseInput = JSON.parse(input),
    		fieldsLength = this.viewfields.length,
    		item,
    		obj=[];    		    	
    	for(var i=0;i<fieldsLength;i++){
    		item = this.viewfields[i];
    		if(item.model.get("name") == parseInput.idField){    			
    			item.setScannerCode(parseInput);
    		}
    	}    	  	    	
    };

    ProjectMobile.prototype.setCode= function (input) {    	
    	var parseInput = JSON.parse(input),
    		fieldsLength = this.viewfields.length,
    		item,
    		obj=[];    		    	
    	for(var i=0;i<fieldsLength;i++){
    		item = this.viewfields[i];
    		if(item.model.get("name") == parseInput.idField){    			
    			item.setScannerCode(parseInput);
    		}
    	}    	  	    	
    };
    


    ProjectMobile.prototype.changeID= function (input) {    	
    	var parseInput = JSON.parse(input),
    		fieldsLength = this.viewfields.length,
    		item;    		    	
    	for(var i=0;i<fieldsLength;i++){
    		item = this.viewfields[i];
    		if(item.model.get("name") == parseInput.idField){
    			item.changeID(parseInput.data);
    		}
    	}    	  	    	
    };

    ProjectMobile.prototype.getDataFile = function () {
    	var respData = this.memoryStack[0].data;
    	return JSON.stringify(respData);	
    };

    ProjectMobile.prototype.setMemoryStack = function (data) {
    	this.memoryStack = [];
    	this.memoryStack.push(data);
    };

    ProjectMobile.prototype.blockSubmit = function (value) {
    	if(typeof value == "boolean"){
    		this.blockButtonSubmit = value;
    	}		
    }; 

	PMDynaform.extendNamespace("PMDynaform.core.ProjectMobile", ProjectMobile);

}());