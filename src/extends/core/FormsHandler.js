
(function(){
	var FormsHandler = function(options) {
		this.project = null;
		this.forms = false;
		this.jsonForms = [];
		this.indexForms = null;
 		this.data = [];
 		this.dataDevice = [];
 		this.formID = null;
 		this.addRemoteData = false;
 		this.addRemoteDataFirstTime = false;
 		this.dataRemote = [];		 						
	};

	/**
	 * [init description]
	 * @param  {[type]} project [description]
	 * @param  {[type]} forms   [description]
	 * @param  {[type]} data    [description]
	 * @param  {[type]} formID  [description]
	 * @return {[type]}         [description]
	 */
	FormsHandler.prototype.init = function(project, forms, data, formID, jsonForm) {
		this.setProject(project);
		this.setForms(forms);
		this.setData(data);
		this.setFormID(formID);
		this.setJsonFormCurrent(jsonForm);
		return this;
	};
	/**
	 * [setForms description]
	 * @param {[type]} forms [description]
	 */
	FormsHandler.prototype.setForms = function(forms) {
		var counter;
		counter = forms.length;
		this.forms = forms;
		for(var i=0; i< counter;i++){
			this.jsonForms[i]=null;
			this.dataRemote[i]=null;
		}

		return this;
	};

	FormsHandler.prototype.setAddRemoteData = function(add) {
		this.addRemoteData = add;
	};

	/**
	 * [setData description]
	 * @param {[type]} data [description]
	 */
	FormsHandler.prototype.setData = function(data) {
		if(data != null){
			this.data = data;
		}else{
			this.data = [];
		}
		return this;
	};
	/**
	 * [setDataDevice description]
	 * @param {[type]} dataDevice [description]
	 */
	FormsHandler.prototype.setDataDevice = function(dataDevice) {
		if(dataDevice != null){
			this.dataDevice = dataDevice;
		}else{
			this.dataDevice = [];
		}
		return this;
	};	
	/**
	 * [setProject description]
	 * @param {[type]} project [description]
	 */
	FormsHandler.prototype.setProject = function(project) {
		this.project = project;
		return this;
	};
	
	/**
	 * [setFormID description]
	 * @param {[type]} formID [description]
	 */
	FormsHandler.prototype.setFormID = function(formID) {
		this.formID = formID;
		this.indexForms = 0;
		for (var i=0; i< this.forms.length ;i++){
			if(this.forms[i].mongoId == this.project.formID){
				this.indexForms = i;
				break;
			}
		}
		return this;
	};

	/**
	 * [setJsonForm description]
	 * @param {[type]} indexForm [description]
	 * @param {[type]} data      [description]
	 */
	FormsHandler.prototype.setJsonForm = function(indexForm, data) {		
		if(typeof this.jsonForms[indexForm] != "undefined"){
			this.jsonForms[indexForm] = data;
		}
		return this;
	};

	/**
	 * [setJsonFormCurrent description]
	 * @param {[type]} data [description]
	 */
	FormsHandler.prototype.setJsonFormCurrent = function(data) {		
			this.jsonForms[this.indexForms] = data;
		/*if(typeof this.jsonForms[this.indexForms] != "undefined"){
			this.jsonForms[this.indexForms] = data;
		}*/
		return this;
	};
	


	/**
	 * [setJsonFormIndexCurrent description]
	 * @param {[type]} data [description]
	 */
	FormsHandler.prototype.setJsonFormIndexCurrent = function(data) {		
		if(typeof this.jsonForms[this.indexForms] != "undefined"){
			this.jsonForms[this.indexForms] = data;
		}
		if(this.jsonForms[this.indexForms] == null){
			this.jsonForms[this.indexForms] = data;
		}
		return this;
	};

	/**
	 * [existNextForm description]
	 * @return {[type]} [description]
	 */
	FormsHandler.prototype.existNextForm = function() {		
		if(this.forms[this.indexForms+1]){
			return true;
		}else{
			return false;	
		}
	};

	/**
	 * [existPrevForm description]
	 * @return {[type]} [description]
	 */
	FormsHandler.prototype.existPrevForm = function() {		
		if(this.forms[this.indexForms-1]){
			return true;
		}else{
			return false;	
		}
	};

	/**
	 * [advanceNextForm description]
	 * @return {[type]} [description]
	 */
	FormsHandler.prototype.advanceForm = function() {		
		this.indexForms++;
		this.formID = this.forms[this.indexForms].formId;
		this.project.setFormID(this.formID);
		return this;
	};

	/**
	 * [backPrevForm description]
	 * @return {[type]} [description]
	 */
	FormsHandler.prototype.backForm = function() {		
		this.indexForms--;
		this.formID = this.forms[this.indexForms].mongoId;
		this.project.setFormID(this.formID);
		return this;
	};
	


	/**
	 * [getJsonFormIndex description]
	 * @param  {[type]} indexForm [description]
	 * @return {[type]}           [description]
	 */
	FormsHandler.prototype.getJsonFormIndex = function(indexForm) {		
		return	this.jsonForms[indexForm];		
	};

	/**
	 * [getIndexForms description]
	 * @param  {[type]} indexForm [description]
	 * @return {[type]}           [description]
	 */
	FormsHandler.prototype.getIndexForms = function(indexForm) {				
		return this.indexForms;
	};

	/**
	 * [getJsonForm description]
	 * @param  {[type]} formID [description]
	 * @return {[type]}        [description]
	 */
	FormsHandler.prototype.getJsonForm = function(formID) {
		var response,
			responseDevice,
			indexCurrent,
			projectFlow= this.projectFlow;
		responseDevice = this.getFormDefinitionDevice(formID);					
		if(responseDevice.state == "success"){		
			return responseDevice.data;							
		}else{
			response = this.project.getJsonForm(formID);
			switch(response.state) {
				case "success":
					return response.data
				    break;
				case "internetFail":
					projectFlow.setToastMessage("Connection failed - get form definition");
					projectFlow.closeWebViewInternetFailed();    
				    break;
				case "unautorized":
					projectFlow.setToastMessage("Connection failed - get form definition");					
					projectFlow.closeWebViewInternetFailed();    
				    break;    			
				default:
				    break;
			}
		}					
		return null;		
	};

	/**
	 * [getJsonFormCurrent description]
	 * @return {[type]} [description]
	 */
	FormsHandler.prototype.getJsonFormCurrent = function() {
		var response,
			responseDevice,
			indexCurrent=this.indexForms,
			projectFlow= this.project.projectFlow;
		// Verify local data json
		
		if(this.jsonForms[indexCurrent] != null){
			return this.jsonForms[indexCurrent];
		}
		
		//responseDevice = this.getJsonFormFromDevice(this.formID);
		responseDevice = projectFlow.getJsonFormFromDevice(this.formID);
		//projectFlow.nextStep("formID", null);
		if(responseDevice == null){
			return null;
		}
		if(responseDevice.state == "success"){						
			this.setJsonForm(indexCurrent, responseDevice.data);
			return responseDevice.data;							
		}else{

			response = this.project.getJsonForm(this.formID);
			
			switch(response.state) {
				case "success":
					this.setJsonForm(indexCurrent, response.data);
					return response.data
				    break;
				case "internetFail":
					projectFlow.setToastMessage("Connection failed - get form definition");
					projectFlow.closeWebViewInternetFailed();    
				    break;
				case "unautorized":
					projectFlow.setToastMessage("Connection failed - get form definition");					
					projectFlow.closeWebViewInternetFailed();    
				    break;    			
				default:
				    break;
			}
		}					
		return null;		
	};


	/**
	 * [getJsonFormCurrentNodevice description]
	 * @return {[type]} [description]
	 */
	FormsHandler.prototype.getJsonFormCurrentNodevice = function() {
		var response,
			responseDevice,
			indexCurrent=this.indexForms,
			projectFlow= this.project.projectFlow;
		// Verify local data json	
		if(this.jsonForms[indexCurrent] != null){
			return this.jsonForms[indexCurrent];
		}

		response = this.project.getJsonForm(this.formID);
		switch(response.state) {
			case "success":
				this.setJsonForm(indexCurrent, response.data);
				return response.data
			    break;
			case "internetFail":
				projectFlow.setToastMessage("Connection failed - get form definition");
				projectFlow.closeWebViewInternetFailed();    
			    break;
			case "unautorized":
				projectFlow.setToastMessage("Connection failed - get form definition");					
				projectFlow.closeWebViewInternetFailed();    
			    break;    			
			default:
			    break;
		}							
		return null;		
	};


	FormsHandler.prototype.getJsonFormCurrentNetwork = function() {
		var response,
			responseDevice,
			indexCurrent=this.indexForms,
			projectFlow= this.project.projectFlow;

		response = this.project.getJsonForm(this.formID);
		switch(response.state) {
			case "success":
				this.setJsonForm(indexCurrent, response.data);
				return response.data
			    break;
			case "internetFail":
				projectFlow.setToastMessage("Connection failed - get form definition");
				projectFlow.closeWebViewInternetFailed();    
			    break;
			case "unautorized":
				projectFlow.setToastMessage("Connection failed - get form definition");					
				projectFlow.closeWebViewInternetFailed();    
			    break;    			
			default:
			    break;
		}							
		return null;		
	};



	FormsHandler.prototype.getJsonFormCurrentLocal = function() {		
		return this.jsonForms[this.indexForms];
	};
	/**
	 * [sendData description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	FormsHandler.prototype.sendData = function(data) {
		
		return this;
	};		

	PMDynaform.extendNamespace("PMDynaform.core.FormsHandler", FormsHandler);
}());