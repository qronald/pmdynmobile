
(function(){
	var ProjectIOS = function(options) {
		this.projectFLow = new PMDynaform.core.ProjectFlowIOS();
		this.id = null;
		this.prevStep= null;
		this.nextStep= null;
		this.model = null;
		this.view = null;
		this.data = null;
		this.formID = null;
		this.taskID = null;
		this.caseID = null;		
		this.workspace = null;				
		this.tokens = null;
		this.restClient = null;
		this.server = null;
		this.submitRest = false;
		this.typeList = null;
		this.onLine = false;
		this.caseTitle = null;
		this.caseNumber = null;
		this.proxy = null;
		this.keys = null;
		this.message = null;
		this.userAgent = null;
		this.currentFormID = null;
		this.dynaforms = [];
		this.container= null;
		options.keys= {
				server: options.server,
				processID: options.processID,
				taskID:options.taskID,
				caseID:options.caseID,
				workspace: options.workspace,
				formID: options.formID,
				keyProject : "project",
				typeList : options.typeList
		};
		options.urlFormat = "{server}/{workspace}/{endPointPath}";
		options.endPointsPath ={
			project: "",
			dynaformDefinition :"dynaform/{formID}",
			submitForm :"process/{processID}/task/{taskID}/dynaform/{formID}",

			newTokens :"oauth/token",
			caseTypeList :"case/{caseID}/dynaform/{typeList}",
			loadDynaform :"case/{caseID}/dynaform/{formID}/data",
			getFormData :"case/{caseID}/dynaform/{formID}/data",

			submitFormCase :"case/{caseID}/dynaform/{formID}/savedata",
			submitFormRoute :"case/{caseID}/dynaform/{formID}/savedata/route",			
			createVariable: "process-variable",
			variableList: "process-variable",
			/**
			 * @key {var_uid} Defines the identifier of the variable
			 * The Endpoint is for get all information about Variable
			 **/
			variableInfo: "process-variable/{var_uid}",
			/**
			 * @key {var_name} Defines the variable name
			 * The Endpoint executes the query associated to variable
			 **/
			executeQuery: "process/{processID}/variable/{variable}",
			uploadFile: ""
		};
		ProjectIOS.superclass.call(this, options); 		
		ProjectIOS.prototype.init.call(this, options);
	};

	PMDynaform.inheritFrom('PMDynaform.core.Project', ProjectIOS);
	ProjectIOS.prototype.init = function(options) {
		var defaults = {
			name: "",
			data: []
		};
		$.extend(true, defaults, options);
		this.setServer(defaults.server);
		this.setProcessID(defaults.processID);
		this.setWorkspace(defaults.workspace);		
		this.setTaskID(defaults.taskID);
		this.setCaseID(defaults.caseID);
		this.setFormID(defaults.formID);
		this.setData(defaults.data);				
		this.setTypeList(defaults.typeList);				
	    this.setSubmitRest(defaults.submitRest);	    
		this.setOnLine(defaults.onLine);
		this.setDynaforms(defaults.dynaforms);
		this.setContainer(defaults.container);
								
	};
	/**
	 * [setKeys description]
	 * @param {[type]} data [description]
	 */
	ProjectIOS.prototype.setTokens = function(tokens) {
		this.tokens = tokens;
		return this;
	};

	ProjectIOS.prototype.setContainer = function(cont) {
		this.container = cont;
		return this;
	};
	


	ProjectIOS.prototype.setDynaforms = function(dyns) {
		this.dynaforms = dyns;
		this.projectFLow.setForms(dyns);
		return this;
	};
	


	ProjectIOS.prototype.setUserAgent = function() {
		if(navigator.userAgent==="formslider-android")					
			this.userAgent= "android";		
		if(navigator.userAgent==="formslider-ios")					
			this.userAgent = "ios";
	};

	/**
	 * [setData set the json form]
	 * @param {[JSON]} data [json form]
	 */
	ProjectIOS.prototype.setData = function(data) {
		if (typeof data === "object") {
			this.data = data;
		}
		return this;
	};
	/**
	 * [setProcessID set the processID of a project]
	 * @param {[String]} procID [description]
	 */
	ProjectIOS.prototype.setProcessID = function(procID) {
			this.processID = procID;
		return this;
	};
	/**
	 * [setOnLine verify if exists conection to internet]
	 * @param {[boolean]} onLine [description]
	 */
	ProjectIOS.prototype.setOnLine = function(onLine) {
			this.onLine = onLine;
		return this;
	};
	/**
	 * [setWorkspace is a workspace of a project]
	 * @param {[string]} workspace [description]
	 */
	ProjectIOS.prototype.setWorkspace = function(workspace) {
			this.workspace = workspace;
		return this;
	};	
	/**
	 * [setTaskID function set the taskID]
	 * @param {[type]} taskID [description]
	 */
	ProjectIOS.prototype.setTaskID = function(taskID) {
			this.taskID = taskID;
		return this;
	};
	/**
	 * [setCaseID function set the caseID]
	 * @param {[type]} caseID [description]
	 */
	ProjectIOS.prototype.setCaseID = function(caseID) {
			this.caseID = caseID;
		return this;
	};
	
	/**
	 * [setTypeList function set the typeList]
	 * @param {[type]} typeList [description]
	 */
	ProjectIOS.prototype.setTypeList = function(typeList) {
			this.typeList = typeList;
		return this;
	};
	/**
	 * [setServer function set the server]
	 * @param {[type]} server [description]
	 */
	ProjectIOS.prototype.setServer = function(server) {
		if(server)
			this.server = server;
		return this;
	};
	
	ProjectIOS.prototype.setSubmitRest = function(submit) {
		if(submit){
			if (typeof submit === "boolean") {
				this.submitRest = submit;
			}
		}
		return this;
	};	
	ProjectIOS.prototype.setModel = function(model) {
		if (model instanceof Backbone.Model) {
			this.model = model;
		}
		return this;
	};

	ProjectIOS.prototype.setFormID = function(formID) {
		this.formID = formID;
		this.keys.formID = formID;		
		return this;
	};

	ProjectIOS.prototype.setCaseID = function(caseID) {
		this.caseID = caseID;
		this.keys.caseID = caseID;		
		return this;
	};
	


	ProjectIOS.prototype.setView = function(view) {
		if (view instanceof Backbone.View) {
			this.view = view;
		}
		return this;
	};
	ProjectIOS.prototype.setDataView = function(data) {
		var i, j,k,l,fields, panels, viewForm=this.view;
        for(i=0; i<viewForm.views.length; i+=1){
                for(j=0; j<viewForm.views[i].viewsBuilt.length; j+=1){
                    for(k=0; k<viewForm.views[i].viewsBuilt[j].length; k+=1){
                        a = viewForm.views[i].viewsBuilt[j][k].model;
                        for (l=0; l<data.length; l+=1){
                        	if(a.get("name") == data[l]["name"]){
                        		a.set("value",data[l]["value"]);
                        		break;
                        	}
                        }
                       
                    }           
                }   
            }            
            return this;
	};		
	ProjectIOS.prototype.setProxy = function(keys) {
		//usage the dataRestClient
		//processId: "",
        //workspace: "",
        //token: "",
        //apiVersion: '1.0'
		this.keys = keys;	    	
    	this.restClient = new PMDynaform.core.Proxy({				
            method: 'POST',
            keys:{
            	processId: this.processID,
                workspace: this.workspace,
                token: this.tokens                
            },				
            server:this.server            
        });    
	};
	
	ProjectIOS.prototype.loadNewCase = function() {
		//Iniciando Flujo
		this.setUserAgent();
		this.projectFLow.initFlow(this);		
		this.projectFLow.nextStep({
			jsonForm:this.data
		});

	};

	ProjectIOS.prototype.loadCase = function() {
		//Iniciando Flujo
		this.setUserAgent();
		this.projectFLow.initFlow(this);
		this.projectFLow.nextStepCase({
			jsonForm:this.data
		});
	};
	


	ProjectIOS.prototype.getFormDefinition = function() {
		
		var restClient, endpoint, url, that=this, resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.dynaformDefinition);
	
        url = this.getFullURL(endpoint);        
		restClient = new PMDynaform.core.Proxy ({
	            url: url,
	            method: 'GET',                    
	            keys: this.token,
	            successCallback: function (xhr, response) {
	            resp= {
	            		"data":response.data.formContent,
	            		"state":"success"
	            	};
	            	
	            },
	            failureCallback: function (xhr, response) {
	            	/*resp= {
	            		"data":data2,
	            		"state":"success"
	            	}*/
	            	resp= {	            	
	            		"state":"internetFail"
	            	};
	            	
	           	}
	        });
	        
		return resp;
	};


	ProjectIOS.prototype.loadNewCaseOffLine = function() {
		//this.formID;
		//this.dynaforms;

	};
	


	ProjectIOS.prototype.loadForm = function(formID) {		
		this.emptyView();
		var restClient, endpoint, url, that=this;
		this.formID=formID;
		this.keys.formID =formID;
		endpoint = this.getFullEndPoint(this.endPointsPath.dynaformDefinition);
        url = this.getFullURL(endpoint);
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
                if(response.data.formContent){									
						that.loadProject(response.data.formContent);                    
				}
				if(navigator.userAgent==="formslider-android"){

				}
				if(navigator.userAgent==="formslider-ios"){										
				}
            },
            failureCallback: function (xhr, response) {            								
           	}
        });        		
		return this;
	};

	ProjectIOS.prototype.getFullEndPoint= function (urlEndpoint) {
        var k, 
		keys  = this.keys,
		urlFormat = urlEndpoint;
		for (k in keys) {
			if (keys.hasOwnProperty(k)) {
				urlFormat = urlFormat.replace(new RegExp("{"+ k +"}" ,"g"), keys[k]);				
			}
		}
		return urlFormat;
    };




    ProjectIOS.prototype.setToastMessage= function (message) {     
		if(navigator.userAgent==="formslider-android"){        	
			JsInterface.showToast(message);						
    	}
    	if(navigator.userAgent==="formslider-ios"){    			    						
			this.message = message;			
		}			
    };

    ProjectIOS.prototype.getToastMessageIOS= function () {     		
		return this.message;			
    };
    

    

    ProjectIOS.prototype.sendSubmitInformationDevice= function () {
         var data, json, resp;
		json = this.model.attributes;
		data = this.getData();
		resp = {
			"json":json,
			"data":data.fields,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID			
		};
    	if(navigator.userAgent==="formslider-ios"){							
			this.executeFakeIOS("submit-nextform");						
		}				
    };

     ProjectIOS.prototype.sendDataForm= function (submitData) {
         var data, json, resp;
		json = this.model.attributes;
		data = submitData;
		resp = {
			"json":json,
			"data":data.fields,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){			        	
			JsInterface.receiveFormData(JSON.stringify(resp));			
    	}
    	if(navigator.userAgent==="formslider-ios"){							
			this.executeFakeIOS("submit-nextform");
			return "ios";			
		}				
    };

     ProjectIOS.prototype.sendDataForm= function (submitData) {
         var data, json, resp;
		json = this.model.attributes;
		data = submitData;
		resp = {
			"json":json,
			"data":data.fields,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID			
		};
		if(navigator.userAgent==="formslider-android"){			        	
			JsInterface.receiveFormData(JSON.stringify(resp));			
    	}
    	if(navigator.userAgent==="formslider-ios"){							
			this.executeFakeIOS("submit-nextform");
			return "ios";			
		}				
    };

    ProjectIOS.prototype.setCaseInformationDevice= function () {        
    	if(navigator.userAgent==="formslider-ios")    								
			this.executeFakeIOS("submit-nextform");												
		return this;			
    };



    ProjectIOS.prototype.getCaseInformation= function () {      
		var resp ={};
		resp["caseID"] = this.caseID;
		resp["caseTitle"] = this.caseTitle;
		resp["ticketNumber"] = this.caseNumber;
		return JSON.stringify(resp);			
    };



    ProjectIOS.prototype.getCaseDataIOS= function () {
        var data, json, resp;
		json = this.model.attributes;
		data = this.getData();
		resp = {
			"json":json,
			"data":data.fields,
			"accessToken":this.token.accessToken,
			"refreshToken":this.token.refreshToken, 
			"formID":this.keys.formID
		};
		return JSON.stringify(resp);					
    };
    



	ProjectIOS.prototype.getFormDefinitionTypeList = function() {
		
		// end point que funciona con un case ID y un type list
		var restClient, endpoint, url, that=this;
		endpoint = this.getFullEndPoint(this.endPointsPath.caseTypeList);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
                that.formID = response.data[0]._id;
                return {
	            		"data":response.data.formContent,
	            		"state":"success",
	            		
	            	}  
                /*
                if(response.data.length>0){			
					that.loadProject(response.data[0].dyn_content);
					that.loadDataDynaform(response.data[0]._id);
					that.formID = response.data[0]._id;
				}*/
            },
            failureCallback : function (xhr, response){
            	
			}
        });
	};

	//Load data from a dynaform
	ProjectIOS.prototype.loadDataDynaform = function(id) {
		var restClient, endpoint, url, that=this;
		endpoint = this.getFullEndPoint(this.endPointsPath.loadDynaform);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
                that.setDataView(response.data); 
            },
            failureCallback : function (xhr, response){
            	// No interner connection
				if(xhr.status == 0){
					console.log("No access to Internet");
				}
				// Unauthorized
				if(response.code == 4202){					
					that.getNewTokens();
				}
			}
        });
		return this;
	};

	ProjectIOS.prototype.loadProject = function(data) {
		var cont;
		if(this.container !=null)
			cont=$('#'+this.container);
		else
			cont= $('#container');	
		this.model = new PMDynaform.model.Form(data);
		this.view = new PMDynaform.view.Form({
				tagName: "div",
				renderTo: cont,
				model: this.model,
				project: this
			});				
	};



	ProjectIOS.prototype.getData = function () {
		var formData = this.view.getData();
		return formData;
	};	
	ProjectIOS.prototype.getRestClient = function (endPoint) {
		return this.restClient;		
	};

	
	ProjectIOS.prototype.executeFields = function () {		
		var that = this;
		return function (namevariable){
			var restClient, endpoint, url,arrayDep, data={}, self=this;
			arrayDep=this.get("dependenciesField");
            for (i=0; i< arrayDep.length; i++){
                view = arrayDep[i];
                data[view.model.get("name")]=view.model.get("value");
            }
			endpoint = that.getFullEndPoint(that.endPointsPath.executeQuery);
	        url = that.getFullURL(endpoint);
	        url = url.replace(new RegExp("{variable}" ,"g"), namevariable);			
			restClient = new PMDynaform.core.Proxy ({
	            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            successCallback: function (xhr, response) {
	                var optSuggest = [];
                	for (k =0; k< response.data.length; k+=1){
                    	optSuggest.push({
                        	value: response.data[k].value,
                        	label: response.data[k].text
                    	})
                	}
                	currentOpt = this.get("options");
                	self.set("options", optSuggest);
	            },
	            failureCallback: function (xhr, response) {            		
					console.log("Fail execute dependents fields query");					
	           	}
	        });        		
			return this;
		}		
	};

	ProjectIOS.prototype.executeSubmit = function(data) {
		if(this.typeList == null){
			this.projectFLow.nextStep({
				dataSubmit:data
			});
		}
		else{
			this.projectFLow.nextStepCase({
				dataSubmit:data
			});
		}			
	};

	ProjectIOS.prototype.submitFormNew = function(data) {
		var dataSend={},that = this,i,restClient, endpoint, url,arrayDep, self=this, resp;

		for(i=0;i<data.fields.length;i++){
			if(data.fields[i].value){
				dataSend[data.fields[i].name]=data.fields[i].value;
			}
		}	
		endpoint = that.getFullEndPoint(that.endPointsPath.submitForm);
        url = that.getFullURL(endpoint);	        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data:dataSend,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"caseID":response.caseId,
	            		"nextStep":response.nextStep,
	            		"caseTitle":response.caseTitle,
	            		"caseNumber":response.caseNumber
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });
	    return resp;					
	};

	ProjectIOS.prototype.submitFormCase = function(data) {
		var dataSend={},that = this,i,restClient, endpoint, url,arrayDep, self=this, resp;

		for(i=0;i<data.fields.length;i++){
			if(data.fields[i].value){
				dataSend[data.fields[i].name]=data.fields[i].value;
			}
		}	
		endpoint = that.getFullEndPoint(that.endPointsPath.submitFormCase);
        url = that.getFullURL(endpoint);	        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data:dataSend,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"caseID":response.caseId,
	            		"nextStep":response.nextStep
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });
	    return resp;					
	};

	ProjectIOS.prototype.submitFormRoute = function(data) {
		var dataSend={},that = this,i,restClient, endpoint, url,arrayDep, self=this, resp;

		for(i=0;i<data.fields.length;i++){
			if(data.fields[i].value){
				dataSend[data.fields[i].name]=data.fields[i].value;
			}
		}	
		endpoint = that.getFullEndPoint(that.endPointsPath.submitFormRoute);
        url = that.getFullURL(endpoint);	        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data:dataSend,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"response":response.data.flows
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });
	    return resp;					
	};

	ProjectIOS.prototype.loadFormData = function(id) {
		var restClient, endpoint, url, that=this, resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.getFormData);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
            	resp = {
            		"state":"success",
            		"data":response.data
            	};                
            },
            failureCallback : function (xhr, response){
            	resp = {
            		"state":"internetFail"
            	};	
			}
        });
		return resp;
	};

	ProjectIOS.prototype.getNewTokens = function() {
		var restClient, dataSend={};
		dataSend["grant_type"]="refresh_token";
		dataSend["refresh_token"]=this.token.refreshToken;
		//dataSend["client_id"]=this.token.clientId;
		dataSend["access_token"]=this.token.accessToken;		
        url = this.getFullURL(this.endPointsPath.newTokens);
	    restClient = new PMDynaform.core.Proxy ({
	        url: url,
            method: 'POST',                    
            keys: this.token,
            data:dataSend,
            successCallback: function (xhr, response) {
                console.log("change of refresh token success");
            },
            failureCallback: function (xhr, response) {
            	console.log("failure response new tokens");
           	}
        });        		
		return this;    	
	};

    ProjectIOS.prototype.emptyView = function() {
		this.view.$el.empty();    		
	};

	ProjectIOS.prototype.closeWebView = function() {
		this.executeFakeIOS("close-webview");		   		
	};

	
	ProjectIOS.prototype.closeWebViewDerivated = function() {
	    	this.executeFakeIOS("derivated");		   		
	};

	ProjectIOS.prototype.internetFail= function () {
	    this.executeFakeIOS("close");		      		
    };

	

	ProjectIOS.prototype.getFullURL = function (endpoint) {
		var k, 
		keys  = this.keys,
		urlFormat = this.urlFormat;
		for (k in keys) {
			if (keys.hasOwnProperty(k)) {							
					urlFormat = urlFormat.replace(new RegExp("{"+ k +"}" ,"g"), keys[k]);				
			}
		}

		return urlFormat.replace(/{endPointPath}/, endpoint);
		
	};

	ProjectIOS.prototype.getFormDefinitionDevice = function() {
		var response = {state:"internetFail"};
		switch(this.userAgent) {				
			default:
			    break;
		}
		return response;	
	};

	PMDynaform.extendNamespace("PMDynaform.core.ProjectIOS", ProjectIOS);

}());