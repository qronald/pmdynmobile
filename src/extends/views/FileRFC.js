(function(){
	var FileMobile = PMDynaform.view.File.extend({
		template: _.template( $("#tpl-extfile").html()),		
		templateAudio: _.template( $("#tpl-extaudio").html()),
		templateVideo: _.template( $("#tpl-extvideo").html()),
		templateMediaVideo: _.template( $("#tpl-media-video").html()),		
		templateMediaAudio: _.template( $("#tpl-media-audio").html()),		
		templateImage: _.template( $("#tpl-extfile").html()),
		templatePlusImage: _.template( $("#tpl-extfile-plus-image").html()),
		templatePlusAudio: _.template( $("#tpl-extfile-plus-audio").html()),
		templatePlusVideo: _.template( $("#tpl-extfile-plus-video").html()),
		boxPlus:null,
		viewsFiles:[],
		mediaVideos:[],
		events: {
			"click buttonImage": "onClickButtonMobile",
	        "click .pmdynaform-file-container .form-control": "onClickButton",
	        "click div[name='button-all'] .pmdynaform-file-buttonup": "onUploadAll",
	        "click div[name='button-all'] .pmdynaform-file-buttoncancel": "onCancelAll",
	        "click div[name='button-all'] .pmdynaform-file-buttonremove": "onRemoveAll"
	    },
		initialize: function () {
			//this.setOnChangeFiles();
			//this.attributes.files= [];
			this.model.on("change", this.render, this);
		},
		onClickButtonMobile: function (event) {			
			var model;
			model = this.model;			
			switch (model.get("subType")) {
			    case "image":
			        this.onClickButtonImage(event);
			        break; 
			    case "audio":
			        this.onClickButtonAudio(event);
			        break;
			    case "video":
			        this.onClickButtonVideo(event);
			        break;
			    default: 
			        this.$el.find("input").trigger( "click" );
			}
			event.preventDefault();
			event.stopPropagation();			
			return this;
		},
		
		onClickButtonImage: function (event) {		
			var respData; 
			respData = {
				idField: this.model.get("name"),
				type:"image"					
			};			
			if(navigator.userAgent == "formslider-android"){				
				JsInterface.getPicture(JSON.stringify(respData));				
			}			
			if(navigator.userAgent == "formslider-ios"){
				this.model.attributes.project.setMemoryStack({"data":respData});
				this.model.attributes.project.projectFlow.executeFakeIOS("upload-file");
			}					
			return this;
		},	
		onClickButtonAudio: function (event) {		
			var respData; 
			respData = {
				idField: this.model.get("name"),
				type:"audio"					
			};			
			if(navigator.userAgent == "formslider-android"){
				JsInterface.getAudio(JSON.stringify(respData));				
			}			
			if(navigator.userAgent == "formslider-ios"){
				this.model.attributes.project.setMemoryStack({"data":respData});
				this.model.attributes.project.projectFlow.executeFakeIOS("upload-file");
			}						
			return this;
		},
		onClickButtonVideo: function (event) {		
			var respData; 
			respData = {
				idField: this.model.get("name"),
				type:"video"					
			};			
			if(navigator.userAgent == "formslider-android"){
				JsInterface.getVideo(JSON.stringify(respData));				
			}			
			if(navigator.userAgent == "formslider-ios"){
				this.model.attributes.project.setMemoryStack({"data":respData});
				this.model.attributes.project.projectFlow.executeFakeIOS("upload-file");
			}						
			return this;
		},		
		validate: function(e, file) {
			var validated = true;
			//extensions = this.model.get("extensions"),
			//maxSize = this.model.get("size");
			
			//Check the extension of the file
			/*if(extensions.indexOf("*") < 0) {
				type = file.extra.extension.toLowerCase().trim();
				if (extensions.indexOf(type) < 0) {
					alert("The extension of the file is not supported for the field...");
					validated = false;
				}
			}*/
			
			// check file size
			/*if ((parseInt(file.size / 1024) > parseInt(maxSize * 1024)) && validated === true) {
				alert("File \""+file.name+"\" is too big. \n Max allowed size is "+ maxSize +" MB.");
				validated = false;
			}*/

			return validated;			
		},
		removeItem: function (event) {
			var items = this.model.get("items"),
			index = $(event.target).data("index");

			items.splice(index, 1);
			this.model.set("items", items);
			this.render();
			return this;
		},		
		onClosePreview: function () {

			return this;
		},
		onPreviewItem: function (event) {
			var file,
			reader,
			shadow = document.createElement("div"),
			background = document.createElement("div"),
			preview = document.createElement("img"),
			index = $(event.target).data("index"),
			closeItem = document.createElement("span");
			closeItem.className = "glyphicon glyphicon-remove";
			closeItem.title = "close";
			$(closeItem).tooltip().click(function(e) {
                $(this).tooltip('toggle');
            });
			heightContainer = document.documentElement.clientHeight;

			shadow.className = "pmdynaform-file-shadow";
			shadow.style.height = heightContainer + "px";
			background.className = "pmdynaform-file-preview-image"
			background.style.height = heightContainer + "px";
			background.appendChild(closeItem);
			$(background).on('click', function (event){
				document.body.removeChild(shadow);
				document.body.removeChild(background);
			});
			file = this.model.get("items")[index].file;
			reader  = new FileReader();
			reader.onloadend = function () {
				preview.src = reader.result;
			}
			
			if (file) {
				reader.readAsDataURL(file);
			} else {
				preview.src = "";
			}
			background.appendChild(preview);
			document.body.appendChild(shadow);
			document.body.appendChild(background);
			return this;
		},
		renderFiles: function () {
			var i,
			that = this,
			items = this.model.get("items");

			for (i=0; i< items.length; i+=1) {
				if (that.model.get("preview")) {
					that.createBox(i, items[i].event,items[i].file);
				} else {
					that.createListBox(i, items[i].event, items[i].file);
				}	
			}
			
			return this;
		},
		createButtonsHTML: function (index) {
			var that = this,
			buttonGroups = document.createElement("div"),
	    	buttonGroupUp = document.createElement("div"),
	    	buttonGroupCancel = document.createElement("div"),
	    	buttonGroupRemove = document.createElement("div"),
	    	buttonGroupView = document.createElement("div");
	    	buttonUp = document.createElement("button"),
	    	buttonCancel = document.createElement("button"),
	    	buttonRemove = document.createElement("button"),
	    	buttonView = document.createElement("button");

	    	/*
	    	 * Adding Options buttons to file
	    	 **/
	    	buttonGroups.className = "btn-group btn-group-justified";

	    	buttonGroupUp.className = "pmdynaform-file-buttonup btn-group";
	    	buttonGroupCancel.className = "pmdynaform-file-buttoncancel btn-group";
	    	buttonGroupCancel.style.display = "none";
	    	buttonGroupRemove.className = "pmdynaform-file-buttonremove btn-group";
	    	buttonGroupView.className = "pmdynaform-file-buttonview btn-group";

	    	buttonUp.className = "glyphicon glyphicon-upload btn btn-success btn-sm";
	    	buttonCancel.className = "glyphicon glyphicon-remove btn btn-danger btn-sm";
	    	buttonRemove.className = "glyphicon glyphicon-trash btn btn-danger btn-sm";
	    	buttonView.className = "glyphicon glyphicon-zoom-in btn btn-primary btn-sm";

	    	$(buttonUp).data("index", index);
	    	$(buttonCancel).data("index", index);
	    	$(buttonRemove).data("index", index);
	    	$(buttonView).data("index", index);
	    	
	    	$(buttonUp).on("click", function(event) {
				that.onToggleButtonUpload(event, "up");
				that.onUploadItem(event);
				event.stopPropagation();
				event.preventDefault();
			});
			$(buttonCancel).on("click", function(event) {
				that.onToggleButtonUpload(event, "cancel");
				that.onCancelUploadItem(event);
				event.stopPropagation();
				event.preventDefault();
			});
	    	$(buttonRemove).on("click", function(event) {
				that.removeItem(event);
				event.stopPropagation();
				event.preventDefault();
			});			
			$(buttonView).on("click", function(event) {
				that.onPreviewItem(event);
				event.stopPropagation();
				event.preventDefault();
			});

	    	buttonGroupUp.appendChild(buttonUp);
	    	buttonGroupCancel.appendChild(buttonCancel);
			buttonGroupRemove.appendChild(buttonRemove);
			buttonGroupView.appendChild(buttonView);

			buttonGroups.appendChild(buttonGroupUp);
			buttonGroups.appendChild(buttonGroupCancel);
			buttonGroups.appendChild(buttonGroupView);
			buttonGroups.appendChild(buttonGroupRemove);

			return buttonGroups;
		},
		createBox: function (index, e, file) {
			var buttonGroups,
			rand = Math.floor((Math.random()*100000)+3),
	    	imgName = file.name,
	    	src = e.target.result,
	    	template = document.createElement("div"),
	    	resizeImage = document.createElement("div"),
	    	preview = document.createElement("span"),
	    	progress = document.createElement("div"),
	    	imgPreview = document.createElement("img"),
	    	spanOverlay = document.createElement("span"),
	    	spanUpDone = document.createElement("span"),
	    	typeClasses = {
	    		video: {
	    			class: "pmdynaform-file-boxpreview-video",
	    			icon: "glyphicon glyphicon-facetime-video"
	    		},
	    		audio: {
	    			class: "pmdynaform-file-boxpreview-audio",
	    			icon: "glyphicon glyphicon-music"
	    		},
	    		file: {
	    			class: "pmdynaform-file-boxpreview-file",
	    			icon: "glyphicon glyphicon-book"
	    		}
	    	},
	    	fileName = file.name.split(/\./)[1].toUpperCase();

	    	template.id = rand;
	    	template.className = "pmdynaform-file-containerimage";

			resizeImage.className = "pmdynaform-file-resizeimage";
			if (file.type.match(/image.*/)) {
				imgPreview.src = src;
				resizeImage.appendChild(imgPreview);
			} else if(file.type.match(/audio.*/)) {
				resizeImage.innerHTML = '<div class="'+ typeClasses['audio'].class +' thumbnail ' + typeClasses['audio'].icon+'"><div>'+ fileName +'</div></div>'; 
			} else if(file.type.match(/video.*/)) {
				resizeImage.innerHTML = '<div class="'+ typeClasses['video'].class +' thumbnail ' + typeClasses['video'].icon+'"><div>'+ fileName +'</div></div>'; 
			} else {
				resizeImage.innerHTML = '<div class="'+ typeClasses['file'].class +' thumbnail ' + typeClasses['file'].icon+'"><div>'+ fileName +'</div></div>'; 
			}
			spanOverlay.className = "pmdynaform-file-overlay";
			spanUpDone.className = "pmdynaform-file-updone";
			spanOverlay.appendChild(spanUpDone);
			resizeImage.appendChild(spanOverlay);

	    	preview.id = rand;
	    	preview.className = "pmdynaform-file-preview";
			preview.appendChild(resizeImage);	    	

	    	progress.id = rand;
	    	progress.className = "pmdynaform-file-progress";
	    	progress.innerHTML = "<span></span>";

	    	template.appendChild(preview);
	    	buttonGroups = this.createButtonsHTML(index);
	    	template.appendChild(buttonGroups);
	    	template.appendChild(progress);
	    	this.$el.find(".pmdynaform-file-droparea").append(template);
	    	
	    	return this;
			
		},
		createListBox: function (index, e, file) {
			var buttonGroups,
			rand = Math.floor((Math.random()*100000)+3),
			listItem = document.createElement("div"),
			label = document.createElement("div");

			listItem.className = "pmdynaform-file-listitem";

			buttonGroups = this.createButtonsHTML(index);
			label.className = "pmdynaform-label-nowrap";
			label.innerHTML = file.name;
			listItem.appendChild(label)
			listItem.appendChild(buttonGroups);
			

			this.$el.find(".pmdynaform-file-list").append(listItem);

			return this;
		},
		toggleButtonsAll: function () {
			//Select the name="button-all" for show the buttons

			return this;
		},
		render: function () {
			var that = this,
			oprand;
			this.createBoxPlus();
			this.$el.html( this.template(this.model.toJSON()) );
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			this.renderFiles();
			this.toggleButtonsAll();
			oprand = {
				dragClass : "pmdynaform-file-active",
				dnd: this.model.get("dnd"),
			    on: {
			        load: function (e, file) {
			        	that.addNewItem(e, file);
			        }
			    }
			};

			var fileContainer = this.$el.find(".pmdynaform-file-droparea-ext")[0];
			this.$el.find(".pmdynaform-file-droparea-ext").append(this.boxPlus);
			var fileControl = this.$el.find("input")[0];
			if (this.model.get("dnd") || this.model.get("preview")) {
				//PMDynaform.core.FileStream.setupDrop(fileContainer, oprand);
			}
			//PMDynaform.core.FileStream.setupInput(fileControl, oprand); 			
			return this;
		},
		renderFile : function (){
			var model;
			model = this.model;			
			switch (model.get("subType")) {
			    case "image":
			        this.renderImage();
			        break; 
			    case "audio":
			        this.renderAudio();
			        break;
			    case "video":
			        this.renderVideo();
			        break;
			    default: 
			        this.renderDefault();
			}
		},
		renderImage : function (){

		},
		createBoxImage : function(file){
			var src,
				newsrc,
				rand = Math.floor((Math.random()*100000)+3);
	    	//imgName = file.name, 
	    	// not used, Irand just in case if user wanrand to print it.
	    	if(file.filePath){
	    		src = file.filePath;
	    		newsrc = src;
	    	}
	    	if(file["base64"]){
	    		src = file["base64"];
	    		newsrc = this.model.makeBase64Image(src); 
	    	}	    	
	    	//src = file["thumbnails"];	    	
	    	
	    	template = document.createElement("div"),
	    	resizeImage = document.createElement("div"),
	    	preview = document.createElement("span"),
	    	progress = document.createElement("div");

	    	template.id = rand;
	    	template.className = "pmdynaform-file-containerimage";

			resizeImage.className = "pmdynaform-file-resizeimage";
			resizeImage.innerHTML = '<img class="pmdynaform-image-ext" src="'+newsrc+'"><span class="pmdynaform-file-overlay"><span class="pmdynaform-file-updone"></span></span>';	    		    	
	    	preview.id = rand;
	    	preview.className = "pmdynaform-file-preview";
			preview.appendChild(resizeImage);
	    	progress.id = rand;
	    	progress.className = "pmdynaform-file-progress";
	    	progress.innerHTML = "<span></span>";
	    	template.appendChild(preview);	    	
	    	template.setAttribute("data-toggle","modal");
	    	template.setAttribute("data-target","#myModal");	
	    	this.viewsFiles.push({
	    		"id":file.id,				
				"data":template	    		
	    	});
	    	this.$el.find(".pmdynaform-file-droparea-ext").prepend(template);	    	
	    	return this;
		},
		renderAudio : function (){

		},
		createBoxAudio : function(file){
			var model,
				tplContainerAudio,
				tplContainer,				
				tplMediaAudio,
				mediaElement;
			model= {
				id: Math.floor((Math.random()*100000)+3),
				src : file.filePath?file.filePath:file,
				extension:file.extension?file.extension:null,
				name: file.name
			};

			tplMediaAudio = this.templateMediaAudio(model);
			mediaElement = new PMDynaform.core.MediaElement({
				el: $(tplMediaAudio),
				type : "audio"
			});

			tplContainerAudio = $(this.templateAudio(model)); 			
			tplContainerAudio.find(".pmdynaform-file-resizevideo").append(mediaElement.$el);			
			this.$el.find(".pmdynaform-file-droparea-ext").prepend(tplContainerAudio);
			
			this.viewsFiles.push({
	    		"id":file.id,				
				"data":tplContainerAudio	    		
	    	});
	    	return this;
		},
		renderVideo : function (){

		},
		/**
		 * [createBoxVideo Create a html of a video]
		 * @param  {[type]} file [description]
		 * @return {[type]}      [description]
		 */
		createBoxVideo : function(file){
			var model,
				tplContainerVideo,
				tplContainer,				
				tplMediaVideo,
				mediaElement,
				urlVideo;
			// The url for streaming consume a endpoint of a project			
			model= {
				id: Math.floor((Math.random()*100000)+3),
				src : file.filePath?file.filePath:file,
				name: file.name
			};

			tplMediaVideo = this.templateMediaVideo(model);
			mediaElement = new PMDynaform.core.MediaElement({
				el:$(tplMediaVideo),
				type:"video"
			});

			tplContainerVideo = $(this.templateVideo(model)); 			
			tplContainerVideo.find(".pmdynaform-file-resizevideo").append(mediaElement.$el);			
			this.$el.find(".pmdynaform-file-droparea-ext").prepend(tplContainerVideo);
			
			this.viewsFiles.push({
	    		"id":file.id,				
				"data":tplContainerVideo	    		
	    	});
	    	return this;
		},
		renderDefault : function (){

		},

		setFilesFromNetwork : function (arrayFiles){
            var array,model,item;
			model = this.model;			
			switch (model.get("subType")) {
			    case "image":
			    		this.loadMixingSourceImages(arrayFiles);			        
			        break; 
			    case "audio":
			    		this.loadMixingSourceMedia(arrayFiles);			        
			        break;
			    case "video":
			        	this.loadMixingSourceMedia(arrayFiles);
			        break;
			    default: 
			        //this.renderDefault();
			}
        },

        loadMixingSourceImages : function (arrayFiles){        	
            var arrayRemoteData=[],
            	arrayFilePath=[],
            	array,
            	sw=false;            
            for (var i=0 ;i< arrayFiles.length ;i++){
            	item = arrayFiles[i];
            	if(item.filePath){            		
            		this.validateFiles(item);
            		this.model.attributes.files.push(item);
            	}else{
            		arrayRemoteData.push(item);
            	}            	
            }

            if(arrayRemoteData.length != 0){
	            array = this.model.remoteProxyData(arrayRemoteData);
		        if(array){
		            for (var i=0 ;i< array.length ;i++){            	
		            	this.validateFiles(array[i]);
		            	this.model.attributes.files.push(array[i]);
		            }
	        	}
        	}
        },

        loadMixingSourceMedia : function (arrayFiles){        	
            var arrayRemoteData=[],
            	arrayFilePath=[],
            	array,
            	itemMedia,
            	sw=false;            
            for (var i=0 ;i< arrayFiles.length ;i++){
            	item = arrayFiles[i];
            	if(typeof item == "string"){
            		itemMedia = this.model.urlFileStreaming(item);
            		this.validateFiles(itemMedia);
            		this.model.attributes.files.push(itemMedia);			        	
            	}
            	if(item.filePath){
            		this.validateFiles(item);
            		this.model.attributes.files.push(item);			        	
            	}            	
            }
        },
        /**
         * [setFiles Function for set files images, video and audio from a interface to mobile]
         * @param {[type]} arrayFiles [description]
         */
        setFiles : function (arrayFiles){        	
            var array;            
            for (var i=0 ;i< arrayFiles.length ;i++){            	
            	this.validateFiles(arrayFiles[i]);
            	this.model.attributes.files.push(arrayFiles[i]);
            }
        },
        validateFiles: function(file) {
        	this.item
        	if (this.model.get("preview")) {
				this.createBoxFile(file);
			} else {
				this.createListBox(file);
			}			
			return this;
		},
		createBoxFile : function (file){
			var model,
				response;
			model = this.model;			
			switch (model.get("subType")) {
			    case "image":
			    	this.createBoxImage(file);			    	
			        break; 
			    case "audio":
			        this.createBoxAudio(file);
			        break;
			    case "video":
			        this.createBoxVideo(file);
			        break;
			    default: 
			        //this.renderDefault();
			}
		},
		createBoxPlus: function () {
			var model;
			model = this.model;			
			switch (model.get("subType")) {
			    case "image":
			        this.boxPlus=$(this.templatePlusImage());
			        break; 
			    case "audio":
			        this.boxPlus=$(this.templatePlusAudio());
			        break;
			    case "video":
			        this.boxPlus=$(this.templatePlusVideo());
			        break;
			    default: 
			        //this.renderDefault();
			}			
			return this;
		},
		changeID : function (arrayNew){        	
            var array = this.model.attributes.files,
            	itemNew,
            	itemOld;           
            for (var i=0 ;i< arrayNew.length ;i++){            	
            	itemNew = arrayNew[i];
            	for (var j=0 ;j< array.length ;j++){
            		itemOld = array[j];
            		if(typeof itemOld === "string"){
            			if(itemNew["idOld"] === itemOld){
            				itemOld = itemNew["idNew"];
            			}            				
            		}
            		if(typeof itemOld === "object"){
            			if(itemNew["idOld"] === itemOld["id"]){
            				itemOld["id"] = itemNew["idNew"];
            			}	
            		}
            	}
            }
        }            
	});

	PMDynaform.extendNamespace("PMDynaform.view.FileMobile",FileMobile);
}());
