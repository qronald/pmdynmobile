(function(){
    var PanelMobile = PMDynaform.view.Panel.extend({
        tagName: "form",
        content : null,    
        template: null,
        items: new PMDynaform.util.ArrayList(),
        views:[],
        templateRow:_.template($('#tpl-row').html()),
        colSpanLabel: 3,
        colSpanControl: 9,
        project: null,
        events: {
            'submit': 'onSubmit'
        },
        initialize: function(options) {
            var defaults = {
            factory : {
                products: {
                    "text": {
                        model: PMDynaform.model.Text,
                        view: PMDynaform.view.Text
                    },
                    "textarea": {
                        model: PMDynaform.model.TextArea,
                        view: PMDynaform.view.TextArea
                    },
                    "checkbox": {
                        model: PMDynaform.model.Checkbox,
                        view: PMDynaform.view.Checkbox
                    },
                    "radio": {
                        model: PMDynaform.model.Radio,
                        view: PMDynaform.view.Radio
                    },
                    "dropdown": {
                        model: PMDynaform.model.DropdownMobile,
                        view: PMDynaform.view.DropdownMobile
                    },
                    "button": {
                        model: PMDynaform.model.Button,
                        view: PMDynaform.view.Button
                    },
                    "submit": {
                        model: PMDynaform.model.Submit,
                        view: PMDynaform.view.Submit
                    },
                    "datetime": {
                        model: PMDynaform.model.Datetime,
                        view: PMDynaform.view.Datetime
                    },
                    "fieldset": {
                        model: PMDynaform.model.Fieldset,
                        view: PMDynaform.view.Fieldset
                    },                    
                    "suggest": {
                        model: PMDynaform.model.SuggestMobile,
                        view: PMDynaform.view.SuggestMobile
                    },                                        
                    "link": {
                        model: PMDynaform.model.Link,
                        view: PMDynaform.view.Link
                    },                                        
                    "hidden": {
                        model: PMDynaform.model.Hidden,
                        view: PMDynaform.view.Hidden
                    },
                    "title": {
                        model: PMDynaform.model.Title,
                        view: PMDynaform.view.Title
                    },
                    "subtitle": {
                        model: PMDynaform.model.Title,
                        view: PMDynaform.view.Title
                    },
                    "label": {
                        model: PMDynaform.model.Label,
                        view: PMDynaform.view.Label
                    },
                    "empty": {
                        model: PMDynaform.model.Empty,
                        view: PMDynaform.view.Empty
                    },
                    "file": {
                        model: PMDynaform.model.FileMobile,
                        view: PMDynaform.view.FileMobile
                    },
                    "image": {
                        model: PMDynaform.model.Image,
                        view: PMDynaform.view.Image
                    },
                    "grid": {
                        model: PMDynaform.model.GridPanel,
                        view: PMDynaform.view.GridPanel
                    },
                    "location": {
                        model: PMDynaform.model.Geo,
                        view: PMDynaform.view.Geo
                    },
                    "scannercode": {
                        model: PMDynaform.model.ScannerCode,
                        view: PMDynaform.view.ScannerCode
                    },
                    "signature": {
                        model: PMDynaform.model.Signature,
                        view: PMDynaform.view.Signature
                    }
                },
                defaultProduct: "empty"
            }       
            };
            if(options.project) {
                this.project = options.project;
            }
            this.setFactory(defaults.factory);
            this.makeItem();
        },
        setAction: function() {
            this.$el.attr("action", this.model.get("action"));
            return this;
        },
        setMethod: function() {
            this.$el.attr("method", this.model.get("method"));
            return this;
        },
        setFactory: function (factory) {
            this.factory = factory;
            return this;
        },
        getData: function() {
            return this.model.getData();
        },
        discardViewField: function () {
            var enabled = true,
            
            disabled = [
                "button",
                "submit"
            ];
            //if (this.model.get("mode") === "view") {}
            

            return enabled;
        },
        makeItem: function() {
            var factory = this.factory, 
            product, 
            productBuilt, 
            rowView,
            jsonFixed,            
            fields =  this.model.get("items");
            this.viewsBuilt = [];
            this.items.clear();

            for(var i=0; i<fields.length; i+=1){
                rowView = [];
                for(var j=0; j<fields[i].length; j+=1){
                    if ((fields[i][j] !== null) &&
                        (this.discardViewField(fields[i][j].type) === true)) {
                        if (fields[i][j].type) {

                            jsonFixed  = new PMDynaform.core.TransformJSON({
                                parentMode: this.model.get("mode"),
                                field: fields[i][j]
                            });
                            product =   factory.products[jsonFixed.getJSON().type.toLowerCase()] ? 
                                factory.products[jsonFixed.getJSON().type.toLowerCase()] : factory.products[factory.defaultProduct];
                            /*product =   factory.products[fields[i][j].type.toLowerCase()] ? 
                                factory.products[fields[i][j].type.toLowerCase()] : factory.products[factory.defaultProduct];*/
                        } else {
                            product = factory.products[factory.defaultProduct];
                        }
                        
                        //The number 12 is related to 12 columns from Bootstrap framework
                        var fieldModel = {
                            colSpanLabel: fields[i].length*2,
                            colSpanControl: 12 - fields[i].length*2,
                            project: this.project,
                            parentMode: this.model.get("mode"),
                            namespace: this.model.get("namespace")
                        };
                        $.extend(true, fieldModel, jsonFixed.getJSON());

                        var productModel = new product.model(fieldModel);                        
                        var productBuilt = new product.view({
                            model: productModel,
                            project:this.project
                        });
                        
                        productBuilt.parent = this;
                        productBuilt.project = this.project;
                        this.project.addViewFields(productBuilt);
                        rowView.push(productBuilt);
                        this.items.insert(productBuilt);
                    }       
                    
                }
                this.viewsBuilt.push(rowView);   
            }
            return this;
        },
        getItems: function () {
            return (this.items.getSize() > 0)? this.items.asArray(): [];
        },
        beforeRender : function (){

        },
        render : function (){
            var i,j, $rowView;
            for(i=0; i<this.viewsBuilt.length; i+=1){
                $rowView = $(this.templateRow());
                for(j=0; j<this.viewsBuilt[i].length; j+=1){
                    $rowView.append(this.viewsBuilt[i][j].render().el);
                }
                this.$el.attr("role","form");
                this.$el.addClass("form-horizontal pmdynaform-form");
                /*this.$el.css("height",
                    parseInt(document.documentElement.clientHeight - 
                    document.documentElement.clientHeight*0.05, 10) + "px");*/
                this.setAction();
                this.setMethod();
                if (this.model.get("target")) {
                    this.$el.attr("target", this.model.get("target"));
                }
                
                
                this.$el.append($rowView);
            }
            this.disableContextMenu();
          return this;
        },
        disableContextMenu: function() {
            this.$el.on("contextmenu", function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
            
            return this;
        },
        afterRender : function (){
            var i, j;
            for(i=0; i<this.viewsBuilt.length; i+=1){
                for(j=0; j<this.viewsBuilt[i].length; j+=1){
                    if (this.viewsBuilt[i][j].afterRender) {
                        this.viewsBuilt[i][j].afterRender();
                    }
                }
            }
            return this;
        },
        onSubmit: function(event) {
            var booResponse, i, restData, restClient;

            if (!this.isValid(true)) {
                booResponse =  false;
            } else {
                for (i=0; i<this.items.length; i+=1) {
                    if(this.items[i].applyStyleSuccess) {
                        this.items[i].applyStyleSuccess();
                    }
                }
                booResponse =  true;
            }            
            if(booResponse){
                if(this.project.submitRest){
                    event.preventDefault();
                    restData= this.project.getData();                  
                    this.project.executeSubmit(restData);
                }
            }
            return booResponse;
        },
        isValid: function(submit) {
            var i, formValid = true,
            itemsField = this.items.asArray();

            if (itemsField.length > 0) {
                for (i = 0; i < itemsField.length; i+=1) {
                    if(itemsField[i].validate) {
                        if(submit){
                            itemsField[i].model.attributes.isSubmit = true;
                        }
                        itemsField[i].validate();
                        if (!itemsField[i].model.get("valid")) {
                            formValid = itemsField[i].model.get("valid");    
                        }
                    }
                }
            }
            return formValid;
        }
    });

    PMDynaform.extendNamespace("PMDynaform.view.PanelMobile", PanelMobile);
    
}());
