(function(){
    var Qrcode_mobile = PMDynaform.view.Field.extend({
        item: null, 
        template: _.template( $("#tpl-ext-scannercode").html()),
        templatePlus: _.template( $("#tpl-extfile-plus").html()),
        boxPlus: null,
        boxModal:null,
        boxBackground:null,
        viewsImages: [],
        imageOffLine : "geoMap.jpg",        
        events: {
            "click button": "onClickButton"         
        },
        initialize: function () {
            //this.setOnChangeFiles();
            //this.initDropArea();
        },              
        onClickButton: function (event) {           
            var respData;
            respData ={
                idField:this.model.get("name")
            };          
            if(navigator.userAgent == "formslider-android"){
                JsInterface.getScannerCode(JSON.stringify(respData));               
            }
            if(navigator.userAgent == "formslider-ios"){
                this.model.attributes.project.setMemoryStack({"data":respData});
                this.model.attributes.project.projectFlow.executeFakeIOS("scannercode");
            }
            event.preventDefault();
            event.stopPropagation();            
            return this;
        },                      
        hideButton : function (){
            var button;
            button = this.$el.find("button");
            button.hide();          
        },
        showLabel : function (scannercode){
            var label,
                newValue,
                html,
                container;
            
            container = this.$el.find("scanner").find(".pmdynaform-label-options");         
            html = '<span>'+scannercode+'</span>';
            container.append(html);                     
        },
        render: function () {
            var that = this,
                fileContainer,
                fileControl;            
            this.$el.html( this.template(this.model.toJSON()));         
            
            return this;
        },
        setScannerCode: function (scannercode) {
            var model,
                obj={},
                response;
            model= this.model;
            model.addCode(scannercode.data);
            //model.set("value",scannercode.data);
            //this.hideButton();
            this.showLabel(scannercode.data);           
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.Qrcode_mobile", Qrcode_mobile);
}());