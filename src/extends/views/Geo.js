(function(){
	var GeoMobile = PMDynaform.view.Field.extend({
		item: null,	
		template: _.template( $("#tpl-extgeo").html()),
		templatePlus: _.template( $("#tpl-extfile-plus").html()),
		boxPlus: null,
		boxModal:null,
		boxBackground:null,
		viewsImages: [],
		imageOffLine : "geoMap.jpg",		
		events: {
	        "click button": "onClickButton"	        
	    },
		initialize: function () {			
		},		
		onClickButton: function (event) {			
			var respData;
			this.model.set("interactive",true);			
			respData = {
					idField: this.model.get("name"),
					interactive:true
				};			
			if(navigator.userAgent == "formslider-android"){
				JsInterface.getGeoTag(JSON.stringify(respData));				
			}
			if(navigator.userAgent == "formslider-ios"){
				this.model.attributes.project.setMemoryStack({"data":respData,"source":"IOS"});
				this.model.attributes.project.projectFlow.executeFakeIOS("show-map");
			}
			event.preventDefault();
			event.stopPropagation();			
			return this;
		},
		makeBase64Image : function (base64){
            return "data:image/png;base64,"+base64;
        },		
		createBox: function (data) {
			var rand,
				newsrc,
				template,
				resizeImage,
				preview,
				progress;

			if(data.base64){
				newsrc = this.makeBase64Image(data.base64);				 				
			}			
			if(data.filePath){
				newsrc = data.filePath;
			}

			rand = Math.floor((Math.random()*100000)+3);	    	
	    	template = document.createElement("div"),
	    	resizeImage = document.createElement("div"),
	    	preview = document.createElement("span"),
	    	progress = document.createElement("div");

	    	template.id = rand;
	    	template.className = "pmdynaform-file-containergeo";

			resizeImage.className = "pmdynaform-file-resizeimage";
			resizeImage.innerHTML = '<img src="'+newsrc+'">';	    	
	    	preview.id = rand;
	    	preview.className = "pmdynaform-file-preview";
			preview.appendChild(resizeImage);
	    	template.appendChild(preview);	    	
	    	this.$el.find(".pmdynaform-ext-geo").prepend(template);	    	
	    	this.hideButton();
	    	return this;
		},		
		hideButton : function (){
			var button;
			button = this.$el.find("button");
			button.hide();	    	
		},	
		render: function () {
			var that = this,
				fileContainer,
				fileControl;			
			this.$el.html( this.template(this.model.toJSON()));			
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			fileContainer = this.$el.find(".pmdynaform-file-droparea-ext")[0];			
			fileControl = this.$el.find("input")[0];			
			return this;
		},
		setLocation: function (location){
			var model,obj={},
				response;
			model= this.model;
			model.set("geoData",location);
			if(this.model.get("interactive")&& location !=null){				
				if(location.id == "" || location.id == null){
					obj={
						location: {
							altitude : location.altitude,
					        latitude : location.latitude,
					        longitude : location.longitude
						}
					};
					response=model.remoteGenerateLocation(obj);
					if(response){
						if(response.success == true){
							obj["imageId"]=response.imageId;
							response=model.remoteProxyData(response);
							obj["data"]=response.data;							
							this.createBox(response);
							model.set("geoData",obj);
						}
					}
					else{
						this.createImageOffLine(location);
					}									
				}else{
					if(location.base64){
						this.createBox(location);		
					}else{
						response=model.remoteProxyData(location.id);								
						response.altitude = location.altitude;
						response.latitude = location.latitude;
						response.longitude = location.longitude;
						model.set("geoData",response);						
						this.createBox(response);
					}					
				}
			}
		},

		setLocationRFC: function (location){
			var model,
				obj={},
				response;
			model= this.model;
			model.set("geoData",location);

			//location.data is a string Base64 from device mobile
			if(location.data){
				this.createBox(response);	//ok	
			}else{
				response=model.getImagesNetwork(location);								
				this.createBox(response);		
			}
		},		

		setLocationRFC: function (location){
			var model,
				obj={},
				response;
			model= this.model;
			model.set("geoData",location);

			//location.data is a string Base64 from device mobile
			if(location.data){
				this.createBox(response);	//ok	
			}else{
				response=model.getImagesNetwork(location);								
				this.createBox(response);		
			}
		},	

		createImageOffLine: function (location){
			location["filePath"]=this.imageOffLine;
			this.createBox({
				filePath:this.imageOffLine
			});
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.GeoMobile",GeoMobile);
}());