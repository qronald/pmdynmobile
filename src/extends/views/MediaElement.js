(function(){
    var MediaElement = function (settings) {
        this.el = settings.el;
        this.$el = settings.el;
        this.streaming = settings.streaming? settings.streaming: null;
        this.type = settings.type;
        if(this.type == "video"){            
            MediaElement.prototype.initVideo.call(this, this.el);
        }
        if(this.type== "audio"){
            MediaElement.prototype.initAudio.call(this, this.el);
        }        
    };

    MediaElement.prototype.initVideo = function (element) {
            var video = element.find("video");
            var control = element.find(".pmdynaform-media-control");
            //remove default control when JS loaded
            video[0].removeAttribute("controls");
            element.find('.pmdynaform-media-control').fadeIn(500);
            element.find('.pmdynaform-media-caption').fadeIn(500);
         
            //before everything get started
            video.on('loadedmetadata', function() {
                    
                //set video properties
                element.find('.current').text(timeFormat(0));
                element.find('.duration').text(timeFormat(video[0].duration));
                updateVolume(0, 0.7);
                    
                //start to get video buffering data 
                setTimeout(startBuffer, 150);
                    
                //bind video events
                element
                /*.hover(function() {
                    $('.control').stop().fadeIn();
                    $('.caption').stop().fadeIn();
                }, function() {
                    if(!volumeDrag && !timeDrag){
                        $('.control').stop().fadeOut();
                        $('.caption').stop().fadeOut();
                    }
                })*/
                .on('click', function() {
                    element.find('.btnPlay').find('.glyphicon.glyphicon-play').addClass('glyphicon glyphicon-pause').removeClass('glyphicon glyphicon-play');
                    $(this).unbind('click');
                    video[0].play();
                });
            });
            
            //display video buffering bar
            var startBuffer = function() {
                var that= this;
                var currentBuffer = video[0].buffered.end(0);
                var maxduration = video[0].duration;
                var perc = 100 * currentBuffer / maxduration;
                element.find('.pmdynaform-media-bufferBar').css('width',perc+'%');
                    
                if(currentBuffer < maxduration) {
                    setTimeout(startBuffer, 500);
                }
            };  
            
            //display current video play time
            video.on('timeupdate', function() {
                var currentPos = video[0].currentTime;
                var maxduration = video[0].duration;
                var perc = 100 * currentPos / maxduration;
                element.find('.pmdynaform-media-timeBar').css('width',perc+'%');    
                element.find('.current').text(timeFormat(currentPos)); 
            });
            
            //CONTROLS EVENTS
            //video screen and play button clicked
            video.on('click', function() { playpause(); } );
            element.find('.btnPlay').on('click', function() { playpause(); } );
            var playpause = function() {
                if(kitKatMode != null){
                    JsInterface.startVideo(video[0].src,"video/mp4");                                    
                }else{
                    if(video[0].paused || video[0].ended) {
                        element.find('.btnPlay').addClass('paused');
                        element.find('.btnPlay').find('.glyphicon.glyphicon-play').addClass('glyphicon glyphicon-pause').removeClass('glyphicon glyphicon-play');
                        video[0].play();
                    }
                    else {
                        element.find('.btnPlay').removeClass('paused');
                        element.find('.btnPlay').find('.glyphicon.glyphicon-pause').removeClass('glyphicon glyphicon-pause').addClass('glyphicon glyphicon-play');
                        video[0].pause();
                    }
                }
            };

            
            //fullscreen button clicked
            element.find('.btnFS').on('click', function() {
                if($.isFunction(video[0].webkitEnterFullscreen)) {
                    video[0].webkitEnterFullscreen();
                }   
                else if ($.isFunction(video[0].mozRequestFullScreen)) {
                    video[0].mozRequestFullScreen();
                }
                else {
                    alert('Your browsers doesn\'t support fullscreen');
                }
            });
            
            //sound button clicked
            element.find('.sound').click(function() {
                video[0].muted = !video[0].muted;
                $(this).toggleClass('muted');
                if(video[0].muted) {
                    element.find('.pmdynaform-media-volumeBar').css('width',0);
                }
                else{
                    element.find('.pmdynaform-media-volumeBar').css('width', video[0].volume*100+'%');
                }
            });
            
            //VIDEO EVENTS
            //video canplay event
            video.on('canplay', function() {
                element.find('.loading').fadeOut(100);
            });
            
            //video canplaythrough event
            //solve Chrome cache issue
            var completeloaded = false;
            video.on('canplaythrough', function() {
                completeloaded = true;
            });
            
            //video ended event
            video.on('ended', function() {
                element.find('.btnPlay').removeClass('paused');
                video[0].pause();
            });

            //video seeking event
            video.on('seeking', function() {
                //if video fully loaded, ignore loading screen
                if(!completeloaded) { 
                    element.find('.loading').fadeIn(200);
                }   
            });
            
            //video seeked event
            video.on('seeked', function() { });
            
            //video waiting for more data event
            video.on('waiting', function() {
                element.find('.loading').fadeIn(200);
            });
            
            //VIDEO PROGRESS BAR
            //when video timebar clicked
            var timeDrag = false;   /* check for drag event */
            element.find('.pmdynaform-media-progress').on('mousedown', function(e) {
                timeDrag = true;
                updatebar(e.pageX);
            });
            $(document).on('mouseup', function(e) {
                if(timeDrag) {
                    timeDrag = false;
                    updatebar(e.pageX);
                }
            });
            $(document).on('mousemove', function(e) {
                if(timeDrag) {
                    updatebar(e.pageX);
                }
            });
            var updatebar = function(x) {
                var progress = element.find('.pmdynaform-media-progress');
                
                //calculate drag position
                //and update video currenttime
                //as well as progress bar
                var maxduration = video[0].duration;
                var position = x - progress.offset().left;
                var percentage = 100 * position / progress.width();
                if(percentage > 100) {
                    percentage = 100;
                }
                if(percentage < 0) {
                    percentage = 0;
                }
                element.find('.pmdynaform-media-timeBar').css('width',percentage+'%');  
                video[0].currentTime = maxduration * percentage / 100;
            };

            //VOLUME BAR
            //volume bar event
            var volumeDrag = false;
            element.find('.pmdynaform-media-volume').on('mousedown', function(e) {
                volumeDrag = true;
                video[0].muted = false;
                element.find('.sound').removeClass('muted');
                updateVolume(e.pageX);
            });
            $(document).on('mouseup', function(e) {
                if(volumeDrag) {
                    volumeDrag = false;
                    updateVolume(e.pageX);
                }
            });
            $(document).on('mousemove', function(e) {
                if(volumeDrag) {
                    updateVolume(e.pageX);
                }
            });
            var updateVolume = function(x, vol) {
                var volume = element.find('.pmdynaform-media-volume');
                var percentage;
                //if only volume have specificed
                //then direct update volume
                if(vol) {
                    percentage = vol * 100;
                }
                else {
                    var position = x - volume.offset().left;
                    percentage = 100 * position / volume.width();
                }
                
                if(percentage > 100) {
                    percentage = 100;
                }
                if(percentage < 0) {
                    percentage = 0;
                }
                
                //update volume bar and video volume
                element.find('.pmdynaform-media-volumeBar').css('width',percentage+'%');    
                video[0].volume = percentage / 100;
                
                //change sound icon based on volume
                if(video[0].volume == 0){
                    element.find('.sound').removeClass('sound2').addClass('muted');
                }
                else if(video[0].volume > 0.5){
                    element.find('.sound').removeClass('muted').addClass('sound2');
                }
                else{
                    element.find('.sound').removeClass('muted').removeClass('sound2');
                }
                
            };

            //Time format converter - 00:00
            var timeFormat = function(seconds){
                var m = Math.floor(seconds/60)<10 ? "0"+Math.floor(seconds/60) : Math.floor(seconds/60);
                var s = Math.floor(seconds-(m*60))<10 ? "0"+Math.floor(seconds-(m*60)) : Math.floor(seconds-(m*60));
                return m+":"+s;
            };
            this.$el=element;    
    };


    MediaElement.prototype.initAudio = function (element) {
            var video = element.find("audio");
            var control = element.find(".pmdynaform-media-control");
            //remove default control when JS loaded
            video[0].removeAttribute("controls");
            element.find('.pmdynaform-media-control').fadeIn(500);
            element.find('.pmdynaform-media-caption').fadeIn(500);
         
            //before everything get started
            video.on('loadedmetadata', function() {
                    
                //set video properties
                element.find('.current').text(timeFormat(0));
                element.find('.duration').text(timeFormat(video[0].duration));
                updateVolume(0, 0.7);
                    
                //start to get video buffering data 
                setTimeout(startBuffer, 150);
                    
                //bind video events
                element
                /*.hover(function() {
                    $('.control').stop().fadeIn();
                    $('.caption').stop().fadeIn();
                }, function() {
                    if(!volumeDrag && !timeDrag){
                        $('.control').stop().fadeOut();
                        $('.caption').stop().fadeOut();
                    }
                })*/
                .on('click', function() {
                    element.find('.btnPlay').find('.glyphicon.glyphicon-play').addClass('glyphicon glyphicon-pause').removeClass('glyphicon glyphicon-play');
                    $(this).unbind('click');
                    video[0].play();
                });
            });
            
            //display video buffering bar
            var startBuffer = function() {
                var that= this;
                var currentBuffer = video[0].buffered.end(0);
                var maxduration = video[0].duration;
                var perc = 100 * currentBuffer / maxduration;
                element.find('.pmdynaform-media-bufferBar').css('width',perc+'%');
                    
                if(currentBuffer < maxduration) {
                    setTimeout(startBuffer, 500);
                }
            };  
            
            //display current video play time
            video.on('timeupdate', function() {
                var currentPos = video[0].currentTime;
                var maxduration = video[0].duration;
                var perc = 100 * currentPos / maxduration;
                element.find('.pmdynaform-media-timeBar').css('width',perc+'%');    
                element.find('.current').text(timeFormat(currentPos)); 
            });
            
            //CONTROLS EVENTS
            //video screen and play button clicked
            video.on('click', function() { playpause(); } );
            element.find('.btnPlay').on('click', function() { playpause(); } );
            var playpause = function() {
                if(video[0].paused || video[0].ended) {
                    element.find('.btnPlay').addClass('paused');
                    element.find('.btnPlay').find('.glyphicon.glyphicon-play').addClass('glyphicon-pause').removeClass('glyphicon-play');
                    video[0].play();
                }
                else {
                    element.find('.btnPlay').removeClass('paused');
                    element.find('.btnPlay').find('.glyphicon.glyphicon-pause').removeClass('glyphicon-pause').addClass('glyphicon-play');
                    video[0].pause();
                }
            };

            
            //fullscreen button clicked
            element.find('.btnFS').on('click', function() {
                if($.isFunction(video[0].webkitEnterFullscreen)) {
                    video[0].webkitEnterFullscreen();
                }   
                else if ($.isFunction(video[0].mozRequestFullScreen)) {
                    video[0].mozRequestFullScreen();
                }
                else {
                    alert('Your browsers doesn\'t support fullscreen');
                }
            });
            
            //sound button clicked
            element.find('.sound').click(function() {
                video[0].muted = !video[0].muted;
                $(this).toggleClass('muted');
                if(video[0].muted) {
                    element.find('.pmdynaform-media-volumeBar').css('width',0);
                }
                else{
                    element.find('.pmdynaform-media-volumeBar').css('width', video[0].volume*100+'%');
                }
            });
            
            //VIDEO EVENTS
            //video canplay event
            video.on('canplay', function() {
                element.find('.loading').fadeOut(100);
            });
            
            //video canplaythrough event
            //solve Chrome cache issue
            var completeloaded = false;
            video.on('canplaythrough', function() {
                completeloaded = true;
            });
            
            //video ended event
            video.on('ended', function() {
                element.find('.btnPlay').removeClass('paused');
                video[0].pause();
            });

            //video seeking event
            video.on('seeking', function() {
                //if video fully loaded, ignore loading screen
                if(!completeloaded) { 
                    element.find('.loading').fadeIn(200);
                }   
            });
            
            //video seeked event
            video.on('seeked', function() { });
            
            //video waiting for more data event
            video.on('waiting', function() {
                element.find('.loading').fadeIn(200);
            });
            
            //VIDEO PROGRESS BAR
            //when video timebar clicked
            var timeDrag = false;   /* check for drag event */
            element.find('.pmdynaform-media-progress').on('mousedown', function(e) {
                timeDrag = true;
                updatebar(e.pageX);
            });
            $(document).on('mouseup', function(e) {
                if(timeDrag) {
                    timeDrag = false;
                    updatebar(e.pageX);
                }
            });
            $(document).on('mousemove', function(e) {
                if(timeDrag) {
                    updatebar(e.pageX);
                }
            });
            var updatebar = function(x) {
                var progress = element.find('.pmdynaform-media-progress');
                
                //calculate drag position
                //and update video currenttime
                //as well as progress bar
                var maxduration = video[0].duration;
                var position = x - progress.offset().left;
                var percentage = 100 * position / progress.width();
                if(percentage > 100) {
                    percentage = 100;
                }
                if(percentage < 0) {
                    percentage = 0;
                }
                element.find('.pmdynaform-media-timeBar').css('width',percentage+'%');  
                video[0].currentTime = maxduration * percentage / 100;
            };

            //VOLUME BAR
            //volume bar event
            var volumeDrag = false;
            element.find('.pmdynaform-media-volume').on('mousedown', function(e) {
                volumeDrag = true;
                video[0].muted = false;
                element.find('.sound').removeClass('muted');
                updateVolume(e.pageX);
            });
            $(document).on('mouseup', function(e) {
                if(volumeDrag) {
                    volumeDrag = false;
                    updateVolume(e.pageX);
                }
            });
            $(document).on('mousemove', function(e) {
                if(volumeDrag) {
                    updateVolume(e.pageX);
                }
            });
            var updateVolume = function(x, vol) {
                var volume = element.find('.pmdynaform-media-volume');
                var percentage;
                //if only volume have specificed
                //then direct update volume
                if(vol) {
                    percentage = vol * 100;
                }
                else {
                    var position = x - volume.offset().left;
                    percentage = 100 * position / volume.width();
                }
                
                if(percentage > 100) {
                    percentage = 100;
                }
                if(percentage < 0) {
                    percentage = 0;
                }
                
                //update volume bar and video volume
                element.find('.pmdynaform-media-volumeBar').css('width',percentage+'%');    
                video[0].volume = percentage / 100;
                
                //change sound icon based on volume
                if(video[0].volume == 0){
                    element.find('.sound').removeClass('sound2').addClass('muted');
                }
                else if(video[0].volume > 0.5){
                    element.find('.sound').removeClass('muted').addClass('sound2');
                }
                else{
                    element.find('.sound').removeClass('muted').removeClass('sound2');
                }
                
            };

            //Time format converter - 00:00
            var timeFormat = function(seconds){
                var m = Math.floor(seconds/60)<10 ? "0"+Math.floor(seconds/60) : Math.floor(seconds/60);
                var s = Math.floor(seconds-(m*60))<10 ? "0"+Math.floor(seconds-(m*60)) : Math.floor(seconds-(m*60));
                return m+":"+s;
            };
            this.$el=element;    
    };
    PMDynaform.extendNamespace("PMDynaform.core.MediaElement", MediaElement);

}());

