require 'rubygems'
require 'find'

task :required do
    #puts "Checking GEMs required..."
    isOk = true
    begin
        require 'json'
    rescue LoadError
        puts "JSON gem not found.\nInstall it by running 'gem install json'"
        isOk = false
    end
    begin
        require 'closure-compiler'
    rescue LoadError
        puts "closure-compiler gem not found.\nInstall it by running 'gem install closure-compiler'"
        isOk = false
    end
    begin
        require 'compass'
    rescue LoadError
        puts "compass gem not found.\nInstall it by running 'gem install compass'"
        isOk = false
    end
    begin
        require 'zip/zip'
        require 'zip/zipfilesystem'
    rescue LoadError
        puts "Zip Tools not found.\nInstall it by running 'gem install rubyzip -v 0.9.9'"
        isOk = false
    end
    if !isOk
        abort
    end
    #puts "DONE"
end

task :dir do
    system "rm -R build/"
    Dir.mkdir('build') unless File.exists?('build')
    Dir.mkdir('build/js') unless File.exists?('build/js')
    Dir.mkdir('build/css') unless File.exists?('build/css')
    Dir.mkdir('build/img') unless File.exists?('build/img')
end

desc "Build full project"
task :fullBuild => [:required] do
    system "rm -R packaged"
    Dir.mkdir('packaged') unless File.exists?('packaged')
    Dir.mkdir('packaged/libs') unless File.exists?('packaged/libs')
    Rake::Task['build'].execute
    system "cp -R build/ packaged/"
    system "cp -R libs/ packaged/libs"

    files = ["packaged/form.html", "packaged/index.html"]    
    files.each do |file|
        html = File.read file 
        while html['../'] do
            html['../'] = ""
        end
        File.open(file, 'w+') do |f|
            f.write html
        end    
    end
end

desc "Generates the project .js file"
task :js => [:required, :dir] do
    Rake::Task['required'].execute
    buildConfig = File.read 'config/build.json'
    buildFiles = JSON.parse buildConfig
    i = 0

    buildFiles.each do |bFile|
        extension = bFile['extension']
        name = bFile['name']
        write_path = bFile['target_path']
        buffer_file = ""
        sourceFiles = bFile['files']
        
        sourceFiles.each do |source|
            buffer_file += File.read source
            buffer_file += "\n"
        end

        fileName = write_path + name + '.' + extension
        File.open(fileName, 'w+') do |write_file|
            write_file.write buffer_file
        end

        puts "#{name}.#{extension} file has been generated correctly."
        if (bFile['minify'])    
            write_path += 'min/'
            Dir.mkdir(write_path) unless File.exists?(write_path)
            fileName = write_path + name + '.min.' + extension
            File.open(fileName, 'w+') do |write_file|
                write_file.write Closure::Compiler.new.compress(buffer_file)
            end
            puts "#{name}.min.#{extension} file generated correctly."
        end
    end
    #puts "DONE"
end

desc "Run Sass"
task :sass => [:required] do    
    Dir.mkdir('src/stylesheets/css') unless File.exists?('src/stylesheets/css')
    pathScss = "src/stylesheets/sass/"
    pathCss = "src/stylesheets/css/"

    files = Dir.entries(pathScss)
    files.each do |item|
        if File.directory?(pathScss+item)
            #puts "directory"
        else
            sourceTarg = pathScss + item
            sourceDest = pathCss + item.split(".").first + ".css"
            #File.open(sourceDest, 'w') {|f| f.write("") }
            system "sass ./#{sourceTarg} #{sourceDest}"
        end
    end
    
    #For compass
    #preferred_syntax = :sass
    #http_path = '/'
    #css_dir = 'src/stylesheets/css'
    #sass_dir = 'src/stylesheets/sass'
    #images_dir = 'src/stylesheets/img'
    #javascripts_dir = './src'
    #http_generated_images_path = "./build/css"
    #relative_assets = true
    #line_comments = true  
end

desc "Generates the index.html"
task :buildApp => [:required] do
    Rake::Task['required'].execute
    templatesConfig = File.read 'config/templates.json'
    templatesFiles = JSON.parse templatesConfig
    i = 0
    templateHTML = ""
    templatesFiles.each do |bFile|
        extension = bFile['extension']
        name = bFile['name']
        write_path = bFile['target_path']
        buffer_file = ""
        sourceFiles = bFile['files']
        sourceFiles.each do |source|
            buffer_file += File.read source
            buffer_file += "\n"
        end
        templateHTML += buffer_file
    end

    system "cp app/index.html build/"
    system "cp app/form.html build/"
    system "cp app/formRest.html build/"    
    system "cp app/server.php build/"
    system "cp app/appBuild.js build/"
    system "cp -R data build/"
    system "cp -R fonts build/css/"
    
    doc_target = "build/form.html"    
    html = File.read doc_target 
    while html['###TEMPLATES##'] do
        html['###TEMPLATES###'] = templateHTML
    end
    File.open(doc_target, 'w+') do |file|
        file.write html
    end 
    doc_target = "build/formRest.html"    
    html = File.read doc_target 
    while html['###TEMPLATES##'] do
        html['###TEMPLATES###'] = templateHTML
    end
    File.open(doc_target, 'w+') do |file|
        file.write html
    end   
end

desc "Build PMDynaform library"
task :build => [:required, :dir] do
    Rake::Task['dir'].execute
    Rake::Task['sass'].execute
    Rake::Task['js'].execute
    Rake::Task['buildApp'].execute
    puts "Build generated successfully..."
end

desc "Default Task - Build Library"
task :default do
  Rake::Task['build'].execute
end

task :version do
    puts getVersion
end

def getVersion
    if File.exists?'VERSION.txt'
        version = File.read 'VERSION.txt'
    else
        hash = `git rev-parse HEAD`[0..6]
        branch = `git branch`
        branch = branch[2..(branch.length-2)]
        version = branch+"."+hash
    end

    return version
    exit
end

class String
    def red;            "\033[31m#{self}\033[0m" end
    def green;          "\033[32m#{self}\033[0m" end
    def bold;           "\033[1m#{self}\033[22m" end
end

